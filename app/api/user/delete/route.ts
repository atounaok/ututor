import prisma from '@/app/libs/prismadb'
import { NextResponse } from 'next/server'

export const dynamic = 'force-dynamic';

export async function DELETE(
    request: Request
){
    try {
        // Extraire les données de la requête
        const body = await request.json();

        const { userId } = body;

        // Vérifier si userId est fourni dans la requête
        if (!userId) {
            return new NextResponse('User ID is required', { status: 400 });
        }

        // On supprime l'utilisateur
        await prisma.user.delete({
            where: {
                id: userId,
            },
        });
    

        // Retourner une réponse réussie
        return new NextResponse('User deleted successfully', { status: 200 });
    } catch (error: any) {
        console.log(error, 'APPLICATION_ERROR');
        return new NextResponse('Internal Error', { status: 500 });
    }
}