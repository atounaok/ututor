import prisma from '@/app/libs/prismadb'
import { NextResponse } from 'next/server'
import bcrypt from 'bcrypt'

export const dynamic = 'force-dynamic';

export async function PUT(
    request: Request
){
    try {
        // Extraire les données de la requête
        const body = await request.json();

        const { userId, password, newPassword, cNewPassword } = body;


        // Vérifier si userId, password, newPassword et cNewPassword sont fournis dans la requête
        if (!userId || !password || !newPassword || !cNewPassword) {
            return new NextResponse('User ID, password, newPassword et cNewPassword sont nécessaires', { status: 400 });
        }

        console.log(body);

        // Vérifier si userId est fourni dans la requête
        if (!userId) {
            return new NextResponse('User ID is required', { status: 400 });
        }

        // Récupérer l'utilisateur avec l'ID fourni
        const user = await prisma.user.findUnique({
            where: {
                id: userId,
            },
        });

        // Vérifier si l'utilisateur existe
        if (!user) {
            return new NextResponse('User non trouvé', { status: 404 });
        }

        if(newPassword.length < 8){
            return new NextResponse('Le mot de passe doit contenir au moins 8 caractères!', { status: 400 });
        }

        // Vérifier si le nouveau mot de passe et sa confirmation correspondent
        if (newPassword !== cNewPassword) {
            return new NextResponse('Les mots de passe ne correspondent pas!', { status: 400 });
        }

        // Hacher le nouveau mot de passe avec bcrypt
        const newHashedPassword = await bcrypt.hash(newPassword, 12);

        // On modifie le statut de l'utilisateur
        const updatedUser = await prisma.user.update({
            where: {
                id: userId,
            },
            data: {
                hashedPassword: newHashedPassword
            }
        });
    

        return new NextResponse(JSON.stringify(updatedUser), {
            headers: {
                'Content-Type': 'application/json',
            },
            status: 200,
        });
    } catch (error: any) {
        console.log(error, 'APPLICATION_ERROR');
        return new NextResponse('Internal Error', { status: 500 });
    }
}