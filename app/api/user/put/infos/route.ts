import prisma from '@/app/libs/prismadb'
import { NextResponse } from 'next/server'

export const dynamic = 'force-dynamic';

export async function PUT(
    request: Request
){
    try {
        // Extraire les données de la requête
        const body = await request.json();

        const { userId, name, email, tel } = body;

        // Vérifier si userId est fourni dans la requête
        if (!userId) {
            return new NextResponse('User ID is required', { status: 400 });
        }

        // Vérifier si toutes les données requises sont fournies
        if (!name || !email || !tel) {
            throw new Error('Veuillez remplir tous les champs!');
        }

        // On modifie le statut de l'utilisateur
        const updatedUser = await prisma.user.update({
            where: {
                id: userId,
            },
            data: {
                name: name,
                email: email,
                telephone: tel,
            }
        });
    

        return new NextResponse(JSON.stringify(updatedUser), {
            headers: {
                'Content-Type': 'application/json',
            },
            status: 200,
        });
    } catch (error: any) {
        console.log(error, 'APPLICATION_ERROR');
        return new NextResponse('Internal Error', { status: 500 });
    }
}