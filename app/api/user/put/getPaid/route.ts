import prisma from '@/app/libs/prismadb'
import { NextResponse } from 'next/server'
import nodemailer from 'nodemailer';

export const dynamic = 'force-dynamic';

export async function PUT(
    request: Request
){
    try {
        // Extraire les données de la requête
        const body = await request.json();

        const { userId } = body;

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            port: 465,
            secure: true,
            auth: {
                user: 'atounaok@gmail.com',
                pass: "kymo fxhy sitn gnpj",}
         })

        // Vérifier si userId est fourni dans la requête
        if (!userId) {
            return new NextResponse('User ID is required', { status: 400 });
        }

        // On selectionne l'utilisateur
        const tuteur = await prisma.user.findUnique({
            where: {
                id: userId
            }
        });

        if(!tuteur){
            return new NextResponse('Utilisateur non trouvé', { status: 404 });
        }

        if(tuteur.soldeRecompenses < 1){
            return new NextResponse('Vous n\'avez pas assez de récompenses.', { status: 401 });
        }

        const montantRecompenses = tuteur.soldeRecompenses;
        const montantEnDollars = montantRecompenses * 15;

        // On modifie le statut de l'utilisateur
        const updatedUser = await prisma.user.update({
            where: {
                id: userId,
            },
            data: {
                soldeRecompenses: 0
            }
        });

        const mailOptions = {
            from: 'Équipe UTutor',
            to: `${tuteur.email}`,
            subject: 'Réclamation autorisé - UTutor',
            html: `
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Devoir corrigé - UTutor</title>
                </head>
                <body style="font-family: 'Arial', sans-serif;">
                
                    <header style="background-color: #4CAF50; color: white; text-align: center; padding: .5rem;">
                        <h1>Bonjour, ${tuteur.name}</h1>
                    </header>
                
                    <section style="padding: 1em;">
                        <p>${tuteur?.name} nous avons reçu votre réclamation</p>
                        <p>Vous avez échangé l'équivalent de ${montantRecompenses} récompenses.</p>
                        <p>Vous recevrez un paiement de ${montantEnDollars} $ CAD dans les 5 jours ouvrables!</p>
                        <p>Merci pour votre confiance.</p>
                    </section>

                    <section style="padding: 1em;">
                        <p>Sachez qu'une récompense équivaut à 15$ CAD en date du ${new Date()}.</p>
                    </section>

                    <section style="padding: 1em;">
                        <p>Merci de nous faire confiance.</p>
                    </section>
                
                    <footer style="background-color: #4CAF50; color: white; text-align: center; padding: .5rem;">
                        <p>Cordialement,</p>
                        <p>L'équipe de UTutor</p>
                    </footer>
                
                </body>
                </html>
                `,
            }


        await transporter.sendMail(mailOptions)
    

        return new NextResponse(JSON.stringify(updatedUser), {
            headers: {
                'Content-Type': 'application/json',
            },
            status: 200,
        });
    } catch (error: any) {
        console.log(error, 'APPLICATION_ERROR');
        return new NextResponse('Internal Error', { status: 500 });
    }
}