import prisma from '@/app/libs/prismadb';
import { NextResponse } from 'next/server';
import bcrypt from 'bcrypt';

export const dynamic = 'force-dynamic';

export async function POST(request: Request) {
    try {
        // Extraire les données de la requête
        const body = await request.json();

        const { userId, password } = body;

        // Vérifier si userId et le mot de passe sont fournis dans la requête
        if (!userId || !password) {
            return new NextResponse('User ID et password son nécessaire', { status: 400 });
        }

        // Récupérer l'utilisateur avec l'ID fourni
        const user = await prisma.user.findUnique({
            where: {
                id: userId,
            },
        });

        // Vérifier si l'utilisateur existe
        if (!user) {
            return new NextResponse('User non trouvé', { status: 404 });
        }

        if (!user.hashedPassword) {
            return new NextResponse('User non trouvé', { status: 404 });
        }

        // Vérifier si le mot de passe hashé existe et si le mot de passe fourni correspond à celui hashé
        if (user.hashedPassword && await bcrypt.compare(password, user.hashedPassword)) {
            // Retourner que le mot de passe est valide
            return new NextResponse(JSON.stringify({ isValid: true }), {
                headers: {
                    'Content-Type': 'application/json',
                },
                status: 200,
            });
        } else {
            // Retourner que le mot de passe n'est pas valide
            return new NextResponse(JSON.stringify({ isValid: false }), {
                headers: {
                    'Content-Type': 'application/json',
                },
                status: 200,
            });
        }
    } catch (error: any) {
        console.log(error, 'APPLICATION_ERROR');
        return new NextResponse('Internal Error', { status: 500 });
    }
}
