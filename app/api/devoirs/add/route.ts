import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";

export const dynamic = 'force-dynamic';
export async function POST(request: Request) {
	const body = await request.json();
	const { titre, description, matiere, dateLimite, file, userId } = body;

	try {
		console.log({ titre, description, matiere, dateLimite, file, userId });

		if (!titre || !description || !matiere || !dateLimite) {
			return new NextResponse(`Informations manquantes ou invalide : \n`, {
				status: 400,
			});
		}

		let date = new Date(dateLimite);
		let formattedDate = date.toISOString();

		const nouveauDevoir = await prisma.devoir.create({
			data: {
				titre: titre,
				description: description,
				dateLimite: formattedDate,
				matiere: matiere,
				user: {
					connect: {
						id: userId,
					},
				},
				fichier: file,
			},
		});

		return NextResponse.json(200);
	} catch (error) {
		console.log(error, "APPLICATION_ERROR");
		return new NextResponse("Internal Error", { status: 500 });
	}
}
