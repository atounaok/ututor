import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";

export const dynamic = "force-dynamic";
export async function POST(request: Request) {
	const body = await request.json();
	const { devoirId } = body;

	try {
		if (!devoirId) {
			return new NextResponse(`Informations manquantes ou invalide : \n`, {
				status: 400,
			});
		}

		const deletedDevoir = await prisma.devoir.delete({
			where: {
				id: devoirId,
			},
		});

		return NextResponse.json(200);
	} catch (error) {
		console.log(error, "APPLICATION_ERROR");
		return new NextResponse("Internal Error", { status: 500 });
	}
}
