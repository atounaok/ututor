import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";

export const dynamic = 'force-dynamic';
export async function POST(request: Request) {
	const body = await request.json();

	try {
		return NextResponse.json(200);
	} catch (error) {
		console.log(error, "APPLICATION_ERROR");
		return new NextResponse("Internal Error", { status: 500 });
	}
}
