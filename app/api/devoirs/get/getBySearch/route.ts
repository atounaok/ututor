// Importez vos modèles Prisma ici

import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";

export const dynamic = 'force-dynamic';
export async function GET(request: Request) {
    try {
        // Récupérez le nom recherché à partir des paramètres de la requête
        const searchQuery = new URL(request.url).searchParams.get("search") ?? "";

        // Utilisez Prisma pour obtenir tous les devoirs avec les informations de l'utilisateur associé
        const devoirs = await prisma.devoir.findMany({
            include: {
                user: true, // Inclure les informations de l'utilisateur associé
            },
            where: {
                // Filtrez les devoirs par titre, matière et nom d'utilisateur
                OR: [
                    { titre: { contains: searchQuery.toLocaleLowerCase(), mode: "insensitive" } },
                    { matiere: { contains: searchQuery.toLocaleLowerCase(), mode: "insensitive" } },
                    { user: { name: { contains: searchQuery.toLocaleLowerCase(), mode: "insensitive"  } } },
                ],
            },
        });

        // Retournez les devoirs en tant que réponse JSON
        return new NextResponse(JSON.stringify(devoirs), {
            headers: {
                "Content-Type": "application/json",
            },
            status: 200,
        });
    } catch (error) {
        console.error(error, "APPLICATION_ERROR");
        return new NextResponse("Internal Error", { status: 500 });
    }
}
