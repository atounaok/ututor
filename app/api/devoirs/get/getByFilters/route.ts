// Importez vos modèles Prisma ici
import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";
import getCurrentUser from "@/app/actions/getCurrentUser";
import { ObjectId } from "mongodb";

export const dynamic = "force-dynamic";

export async function GET(request: Request) {
	try {
		let whereCondition: any = {};
		const currentUser = await getCurrentUser();
		const ordres = [
			"De A à Z",
			"De Z à A",
			"Date limite croissante",
			"Date limite décroissante",
			"Par niveau",
			"Par matière",
			"Par utilisateur",
		];

		const matiere = request.url.includes("?matiere=")
			? request.url.split("?matiere=")[1].split("&")[0]
			: "";

		const dateLimite = request.url.includes("&dateLimite=")
			? request.url.split("&dateLimite=")[1].split("&")[0]
			: "";

		let ordre = request.url.includes("&ordre=")
			? request.url.split("&ordre=")[1]
			: "";

		// Si une matière est spécifiée, ajoutez-la à la condition de filtrage
		if (matiere && matiere !== "choix0") {
			whereCondition.matiere = matiere;
		}
		let userId;
		if (currentUser) {
			userId = request.url.includes("&user=") ? currentUser.id : null;
		}

		// Si une date limite est spécifiée, ajoutez-la à la condition de filtrage
		if (dateLimite) {
			// Convertir la date limite en objet Date
			const dateLimiteObj = new Date(dateLimite);
			console.log("Date limite :", dateLimiteObj);
			// Ajouter la condition de date limite
			whereCondition.dateLimite = {
				lt: dateLimiteObj,
			};
		} else {
			// Ajouter une condition pour exclure les devoirs dont la date limite est dépassée
			whereCondition.dateLimite = {
				gt: new Date(), // Date actuelle
			};
		}

		if (userId) {
			whereCondition.userId = userId;
		}

		// Décoder l'ordre s'il est encodé
		ordre = decodeURIComponent(ordre).toLocaleLowerCase();

		// Si un ordre est spécifié, appliquez-le
		let orderByOption: any = {};
		switch (ordre) {
			case "de a à z":
				orderByOption = { titre: "asc" };
				break;
			case "de z à a":
				orderByOption = { titre: "desc" };
				break;
			case "date limite croissante":
				orderByOption = { dateLimite: "asc" };
				break;
			case "date limite décroissante":
				orderByOption = { dateLimite: "desc" };
				break;
			case "par matière":
				orderByOption = { matiere: "asc" };
				break;
			case "par utilisateur":
				orderByOption = { user: { name: "asc" } };
				break;
			default:
				break;
		}

		// Utilisez Prisma pour obtenir les devoirs avec les informations de l'utilisateur associé
		const devoirs = await prisma.devoir.findMany({
			include: {
				user: true, // Inclure les informations de l'utilisateur associé
			},
			where: whereCondition,
			orderBy: orderByOption,
		});

		console.log(userId);
		console.log(devoirs);

		// Retourner les devoirs en tant que réponse JSON
		return new NextResponse(JSON.stringify(devoirs), {
			headers: {
				"Content-Type": "application/json",
			},
			status: 200,
		});
	} catch (error) {
		console.error(error, "APPLICATION_ERROR");
		return new NextResponse("Internal Error", { status: 500 });
	}
}
