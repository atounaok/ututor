// Importez vos modèles Prisma ici
import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";

export const dynamic = 'force-dynamic';

export async function GET(request: Request) {
    try {
        
        const devoirs = await prisma.devoir.findMany({
            include: {
                user: true, 
            },
        });

        return new NextResponse(JSON.stringify(devoirs), {
            headers: {
                "Content-Type": "application/json",
            },
            status: 200,
        });
    } catch (error) {
        console.error(error, "APPLICATION_ERROR");
        return new NextResponse("Internal Error", { status: 500 });
    }
}
