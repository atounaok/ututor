import getCurrentUser from "@/app/actions/getCurrentUser";
import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";
import nodemailer from "nodemailer";

export const dynamic = "force-dynamic";
export async function PUT(request: Request) {
	try {
		const currentUser = await getCurrentUser();
		const body = await request.json();
		const { devoirId, tuteurId, statut } = body;

		const transporter = nodemailer.createTransport({
			service: "gmail",
			port: 465,
			secure: true,
			auth: {
				user: "atounaok@gmail.com",
				pass: "kymo fxhy sitn gnpj",
			},
		});

		if (!currentUser || !currentUser.isTutor) {
			return new NextResponse("Vous ne pouvez pas traiter un devoir.", {
				status: 401,
			});
		}

		if (!devoirId || !tuteurId || !statut) {
			return new NextResponse("Informations insuffisantes", { status: 400 });
		}

		// Récupération du devoir
		const devoir = await prisma.devoir.findUnique({
			include: {
				user: true,
			},
			where: {
				id: devoirId,
			},
		});

		if (!devoir) {
			return new NextResponse("Devoir introuvable", { status: 404 });
		}

		const tutor = await prisma.user.findUnique({
			where: {
				id: tuteurId,
			},
		});

		if (!tutor) {
			return new NextResponse("Tuteur introuvable", { status: 404 });
		}

		// On crée une relation entre le devoir et le tuteur
		const devoirTuteurRelation = await prisma.devoirTuteurRelation.create({
			data: {
				devoirId: devoirId,
				statut: statut,
				tutorId: tuteurId,
				devoirUserId: devoir.userId,
			},
		});

		// On modifie le statut de l'utilisateur
		const updatedDevoir = await prisma.devoir.update({
			where: {
				id: devoirId,
			},
			data: {
				statut: devoirTuteurRelation.statut,
			},
		});

		const mailOptions = {
			from: "Équipe UTutor",
			to: `${devoir?.user?.email}`,
			subject: "Votre devoir est en traitement - UTutor",
			html: `
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Devoir en traitement - UTutor</title>
                </head>
                <body style="font-family: 'Arial', sans-serif;">
                
                    <header style="background-color: #4CAF50; color: white; text-align: center; padding: 1rem;">
                        <h1>Bonjour, ${devoir?.user?.name}</h1>
                    </header>
                
                    <section style="padding: 1em;">
                        <p>${tutor?.name} a commencé a traiter votre devoir.</p>
                        <p>Vous recevrez une notification une fois le traitement terminé.</p>
                        <p>Gardez un oeil sur votre boîte de messagerie pour avoir le résultat de votre devoir traité.</p>
                        <p>Merci pour votre confiance.</p>
                    </section>

                    <section style="padding: 1em;">
                        <p>En attendant, vous pouvez contacter votre tuteur à l'adresse suivante: ${tutor?.email}</p>
                    </section>

                    <section style="padding: 1em;">
                        <p>Vous pouvez aussi consulter le statut de votre devoir sur votre espace personnel.</p>
                        <a href="https://ututor.ca/profil">Voir mon profil</a>
                    </section>
                
                    <footer style="background-color: #4CAF50; color: white; text-align: center; padding: 1rem;">
                        <p>Cordialement,</p>
                        <p>L'équipe de UTutor</p>
                    </footer>
                
                </body>
                </html>
                `,
		};

		await transporter.sendMail(mailOptions);

		return new NextResponse(JSON.stringify(updatedDevoir), {
			headers: {
				"Content-Type": "application/json",
			},
			status: 200,
		});
	} catch (error: any) {
		console.log(error, "APPLICATION_ERROR");
		return new NextResponse("Internal Error", { status: 500 });
	}
}
