import prisma from '@/app/libs/prismadb'
import { NextResponse } from 'next/server'

export async function POST(
    request: Request
){
    try {

        const body = await request.json();
        const {
            name,
            email,
            tel,
            da,
            niveau,
            domaine,
            matiere,
            dispo,
            cvFile,
            lettreFile,
            userId
        } = body;

        console.log("cvFile: " + cvFile);

        if (!email || !tel || !name || !da || !niveau || !domaine || !matiere || !dispo){
            return new NextResponse('Informations insuffisantes', { status: 400 });
        }

        // Création de l'instance InfosCandidature
        const infosCandidature = await prisma.infosCandidature.create({
            data: {
                name: name,
                email: email,
                telephone: tel,
                da: da,
                dispo: new Date(dispo).toISOString(),
                niveau: niveau,
                matieres: {
                    set: [matiere],
                },
                cv: cvFile,
                lettre_motivation: lettreFile,
            },
        });

        // Utilisation de l'instance InfosCandidature pour créer la Candidature
        const nouvelleCandidature = await prisma.candidature.create({
            data: {
                infosCandidature: {
                    connect: {
                        id: infosCandidature.id,
                    },
                },
                user: {
                    connect: {
                      id: userId,
                    },
                  },
            },
        });

        await prisma.user.update({
            where: {
                id: userId,
            },
            data: {
                hasApplied: true,
            },
        });
    

        return NextResponse.json(200);
    } catch (error: any) {
        console.log(error, 'APPLICATION_ERROR');
        return new NextResponse('Internal Error', { status: 500 });
    }
}