
import getCurrentUser from "@/app/actions/getCurrentUser";
import prisma from "@/app/libs/prismadb";
import { notFound } from "next/navigation";
import { NextResponse, NextRequest } from "next/server";

export const dynamic = 'force-dynamic';

export async function GET(request: NextRequest) {
    try {
        const { searchParams } = new URL(request.url);
        const devoirId = searchParams.get("devoirId");
        const currentUser =  await getCurrentUser();

        if (!currentUser || !currentUser.isTutor || !currentUser.isAdmin) {
            return new NextResponse("Vous n'avez pas les droits pour cette page.", { status: 401 });
        }

        if (!devoirId || devoirId.length < 24) {
            return new NextResponse("Informations du devoir incorrectes.", { status: 400 });
        }

        const devoirTuteurRelation = await prisma.devoirTuteurRelation.findUnique({
            where: {
                devoirId: devoirId
            }
        });

        if (!devoirTuteurRelation) {
            return new NextResponse("Aucune relation trouvée", { status: 404 });
        }

        return new NextResponse(JSON.stringify(devoirTuteurRelation), { status: 200});
    } catch (error) {
        console.error(error, "APPLICATION_ERROR");
        return new NextResponse("Internal Error", { status: 500 });
    }
}
