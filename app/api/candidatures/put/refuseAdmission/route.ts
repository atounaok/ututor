import prisma from '@/app/libs/prismadb'
import { NextResponse } from 'next/server'
import nodemailer from 'nodemailer';

export const dynamic = 'force-dynamic';
export async function PUT(
    request: Request
){
    try {

        const body = await request.json();
        const {
            candidatureId,
            candidatEmail,
        } = body;

        if (!candidatureId){
            return new NextResponse('Informations insuffisantes', { status: 400 });
        }

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            port: 465,
            secure: true,
            auth: {
                user: 'atounaok@gmail.com',
                pass: "kymo fxhy sitn gnpj",}
         })

         const mailOptions = {
            from: 'atounaok@gmail.com',
            to: `${candidatEmail}`,
            subject: 'Candidature refusée - UTutor',
            html: `
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Candidature Refusée - UTutor</title>
                </head>
                <body style="font-family: 'Arial', sans-serif;">
                
                    <header style="background-color: #4CAF50; color: white; text-align: center; padding: 1em;">
                        <h1>Bonjour,</h1>
                    </header>
                
                    <section style="padding: 1em;">
                        <p>Nous avons le regret de vous informer que votre candidature a été refusée.</p>
                        <p>N'hésitez pas à postuler à nouveau si vous le souhaitez.</p>
                    </section>
                
                    <footer style="background-color: #4CAF50; color: white; text-align: center; padding: 1em;">
                        <p>Cordialement,</p>
                        <p>L'équipe de UTutor</p>
                    </footer>
                
                </body>
                </html>
                `,
         }

        // Récupération de l'ID de l'utilisateur
        const candidature = await prisma.candidature.findUnique({
            where: {
                id: candidatureId,
            },
        });

        if (!candidature){
            return new NextResponse('Candidature introuvable', { status: 404 });
        }

        // On modifie le statut de la candidature
        const updatedCandidature = await prisma.candidature.update({
            where: {
                id: candidatureId,
            },
            data: {
                isTreated: true,
                dateTreated: new Date(),
            },
        });

        await transporter.sendMail(mailOptions)
    

        return new NextResponse(JSON.stringify(updatedCandidature), {
            headers: {
                'Content-Type': 'application/json',
            },
            status: 200,
        });
    } catch (error: any) {
        console.log(error, 'APPLICATION_ERROR');
        return new NextResponse('Internal Error', { status: 500 });
    }
}