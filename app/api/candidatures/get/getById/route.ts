
import prisma from "@/app/libs/prismadb";
import { NextResponse, NextRequest } from "next/server";

export const dynamic = 'force-dynamic';
export async function GET(request: NextRequest) {
    try {
        const { searchParams } = new URL(request.url);
        const candidatureId = searchParams.get("candidatureId")

        console.log('Candidature Id: ' + candidatureId);

        if (!candidatureId) {
            return new NextResponse("CandidatureId is required", { status: 400 });
        }

        const Candidatures = await prisma.candidature.findUnique({
            include: {
                user: true,
                infosCandidature: true,
            },
            where: {
                id: candidatureId
            },
        });


        return new NextResponse(JSON.stringify(Candidatures), {
            headers: {
                "Content-Type": "application/json",
            },
            status: 200,
        });
    } catch (error) {
        console.error(error, "APPLICATION_ERROR");
        return new NextResponse("Internal Error", { status: 500 });
    }
}
