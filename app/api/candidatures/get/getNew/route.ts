import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";

export const dynamic = 'force-dynamic';
export async function GET(request: Request) {
    try {

        const nouvellesCandidatures = await prisma.candidature.findMany({
            include: {
                user: true,
                infosCandidature: true,
            },
            where: {
                isTreated: false
            },
            orderBy: {
                createdAt: 'desc',
            },
        });


        return new NextResponse(JSON.stringify(nouvellesCandidatures), {
            headers: {
                "Content-Type": "application/json",
            },
            status: 200,
        });
    } catch (error) {
        console.error(error, "APPLICATION_ERROR");
        return new NextResponse("Internal Error", { status: 500 });
    }
}
