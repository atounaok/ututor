
import prisma from "@/app/libs/prismadb";
import { NextResponse, NextRequest } from "next/server";

export const dynamic = 'force-dynamic';

export async function GET(request: NextRequest) {
    try {

        const { searchParams } = new URL(request.url);
        const userId = searchParams.get("userId")

        if (!userId) {
            return new NextResponse("userId is required", { status: 400 });
        }

        const Candidature = await prisma.candidature.findUnique({
            include: {
                user: true,
                infosCandidature: true,
            },
            where: {
                userId: userId
            },
        });


        return new NextResponse(JSON.stringify(Candidature), {
            headers: {
                "Content-Type": "application/json",
            },
            status: 200,
        });
    } catch (error) {
        console.error(error, "APPLICATION_ERROR");
        return new NextResponse("Internal Error", { status: 500 });
    }
}
