import getCurrentUser from "@/app/actions/getCurrentUser";
import { NextResponse } from "next/server";
import prisma from "@/app/libs/prismadb";
import nodemailer from 'nodemailer';

export async function PUT(
    request: Request,
) {
    try {
        const currentUser =  await getCurrentUser();
        const body = await request.json();
        const { conversationId } = body;

        if(currentUser && !currentUser?.isAdmin){
            return new NextResponse('Vous n\'avez pas la permission.', { status: 401 });
        }

        const conve = await prisma.conversation.update({
            where: {
                id: conversationId
            },
            data: {
                statut: "Accepte_Admin"
            }
        })

        const devoir = await prisma.devoir.update({
            where: {
                id: conve.devoirId
            },
            data: {
                statut: "Finie"
            }
        })

        // On accepte de donner la récompense
        const conversationRecompense = await prisma.donnerRecompense.update({
            where: {
                conversationId: conve.id
            },
            data: {
                statut: "Accepte"
            },
            include: {
                tutor: true,
            }
        });

        if(!conversationRecompense){
            return new NextResponse('Conversation non trouvée', { status: 404 });
        }

        // On octroie une récompense au tuteur
        const recompenseTuteur = await prisma.user.update({
            where: {
                id: conversationRecompense.tutorId
            },
            data: {
                soldeRecompenses: {
                    increment: 1
                }
            }
        });

        // On cherche le nom de l'élève
        const eleve = await prisma.user.findUnique({
            where: {
                id: devoir.userId
            },
        });
        
        if(!eleve){
            return new NextResponse('Élève non trouvé', { status: 404 });
        }

        // On envoie une notification au tuteur
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            port: 465,
            secure: true,
            auth: {
                user: 'atounaok@gmail.com',
                pass: "kymo fxhy sitn gnpj",}
         })

         const mailOptions = {
            from: 'Équipe UTutor',
            to: 'abdelbrahim76@gmail.com',
            subject: 'Vous avez obtenu une récompense - UTutor',
            html: `
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Récompense octroyée - UTutor</title>
                </head>
                <body style="font-family: 'Arial', sans-serif;">
                
                    <header style="background-color: #4CAF50; color: white; text-align: center; padding: .5rem;">
                        <h1>Bonjour, ${conversationRecompense?.tutor.name}</h1>
                    </header>
                
                    <section style="padding: 1em;">
                        <p>Un administrateur a analyser votre conversation avec ${eleve?.name}.</p>
                        <p>Vous pouvez consulter votre sole de récompenses dans votre profil.</p>
                        <p>Sachez que vous pouvez convertir vos récompenses en argent réel en faisant une demande de réclamation.</p>
                        <p>Merci pour votre confiance.</p>
                    </section>

                    <section style="padding: 1em;">
                        <p>Consultez votre solde ici: <a href="www.ututor.ca/profil/${conversationRecompense?.tutorId}">Voir mon solde</a></p>
                    </section>
                
                    <footer style="background-color: #4CAF50; color: white; text-align: center; padding: .5rem;">
                        <p>Cordialement,</p>
                        <p>L'équipe de UTutor</p>
                    </footer>
                
                </body>
                </html>
                `,
            }


        await transporter.sendMail(mailOptions)
    
        return NextResponse.json(conversationRecompense);
    } catch (error) {
        return new NextResponse('Internal Error', { status: 500 });
    }
}