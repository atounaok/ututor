import getCurrentUser from "@/app/actions/getCurrentUser";
import { NextResponse } from "next/server";
import prisma from "@/app/libs/prismadb";
import { pusherServer } from "@/app/libs/pusher";

export async function PUT(
    request: Request,
) {
    try {
        const currentUser =  await getCurrentUser();
        const body = await request.json();
        const { conversationId } = body;

        if(!currentUser?.id || !currentUser?.email){
            return new NextResponse('Vous n\'avez pas la permission.', { status: 401 });
        }

        if(!conversationId){
            return new NextResponse('Il faut un Id pour la conversation', { status: 400 });
        }

        // On cherche une conversation existante
        const conversation = await prisma.conversation.findUnique({
            where: {
                id: conversationId
            }
        });

        if(!conversation){
            return new NextResponse('Conversation non trouvée', { status: 404 });
        }

        if(!conversation.devoirId){
            return new NextResponse('Conversation sans devoir', { status: 400 });
        }

        const devoir = await prisma.devoir.findUnique({
            where: {
                id: conversation.devoirId
            }
        });

        if(!devoir){
            return new NextResponse('Devoir non trouvé', { status: 404 });
        }

        const devoirTuteurRelation = await prisma.devoirTuteurRelation.findUnique({
            where: {
                devoirId: conversation.devoirId,
                devoirUserId: currentUser.id,
                NOT:{
                    tutorId: currentUser.id
                }
            }
        });

        if(!devoirTuteurRelation){
            return new NextResponse('Vous n\'avez pas la permission.', { status: 401 });
        }

        const updatedDevoir = await prisma.devoir.update({
            where: {
                id: conversation.devoirId
            },
            data: {
                statut: 'Traite'
            }
        });

        const updatedDevoirTuteurRelation = await prisma.devoirTuteurRelation.update({
            where: {
                devoirId: conversation.devoirId,
                devoirUserId: currentUser.id,
                NOT:{
                    tutorId: currentUser.id
                }
            },
            data: {
                statut: 'Traite'
            }
        });

        const updatedConversation = await prisma.conversation.update({
            where: {
                id: conversationId
            },
            include: {
                users: true
            },
            data: {
                isActive: false,
                statut: 'A_Examiner_Admin'
            }
        });

        updatedConversation.users.map((user) => {
            if(user.email) {
                pusherServer.trigger(user.email, 'conversation:new', updatedConversation);
            }
        })

        return NextResponse.json(updatedConversation);
    } catch (error) {
        return new NextResponse('Internal Error', { status: 500 });
    }
}