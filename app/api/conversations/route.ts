import getCurrentUser from "@/app/actions/getCurrentUser";
import { NextResponse } from "next/server";
import prisma from "@/app/libs/prismadb";
import { pusherServer } from "@/app/libs/pusher";

export async function POST(
    request: Request,
) {
    try {
        const currentUser =  await getCurrentUser();
        const body = await request.json();
        const { userId, devoirId, file } = body;
        let devoirUserId = null;

        if(devoirId) {
            const devoir = await prisma.devoir.findUnique({
                where: {
                    id: devoirId
                }
            });

            if(!devoir){
                return new NextResponse('Devoir non trouvé', { status: 404 });
            }

            devoirUserId = devoir.userId;
        }

        if(!currentUser?.id || !currentUser?.email){
            return new NextResponse('Vous n\'avez pas la permission.', { status: 401 });
        }

        const newConversation = await prisma.conversation.create({
            data: {
                users: {
                    connect: [
                        {
                            id: currentUser.id
                        },
                        {
                            id: userId ? userId : devoirUserId
                        }
                    ]
                },
                devoirId: devoirId,
                devoirCorrige: file,
                tutorId: currentUser.id
            },
            include: {
                users: true
            }
        })

        newConversation.users.map((user) => {
            if(user.email) {
                pusherServer.trigger(user.email, 'conversation:new', newConversation);
            }
        })

        return NextResponse.json(newConversation);
    } catch (error) {
        return new NextResponse('Internal Error', { status: 500 });
    }
}