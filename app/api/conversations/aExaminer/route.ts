import getCurrentUser from "@/app/actions/getCurrentUser";
import { NextResponse } from "next/server";
import prisma from "@/app/libs/prismadb";
import { pusherServer } from "@/app/libs/pusher";

export async function GET(
    request: Request,
) {
    try {
        const currentUser =  await getCurrentUser();

        if(!currentUser?.id || !currentUser?.email || !currentUser.isAdmin){
            return new NextResponse('Vous n\'avez pas la permission.', { status: 401 });
        }

        
        const conversations = await prisma.conversation.findMany({
            where: {
                NOT: {
                    statut: 'En_Revue'
                }
            },
        })

        return NextResponse.json(conversations);
    } catch (error) {
        return new NextResponse('Internal Error', { status: 500 });
    }
}