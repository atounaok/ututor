import getCurrentUser from "@/app/actions/getCurrentUser";
import { NextResponse } from "next/server";
import prisma from "@/app/libs/prismadb";
import { pusherServer } from "@/app/libs/pusher";

export async function POST(
    request: Request,
) {
    try {
        const currentUser =  await getCurrentUser();
        const body = await request.json();
        const { conversationId } = body;

        if(!conversationId){
            return new NextResponse('Il faut un Id pour la conversation', { status: 400 });
        }

        if(!currentUser?.id || !currentUser?.email){
            return new NextResponse('Vous n\'avez pas la permission.', { status: 401 });
        }

        // On cherche une conversation existante
        const existingConversations = await prisma.conversation.findUnique({
            where: {
                id: conversationId,
            },
            include:{
                users: true
            }
        });

        if(!existingConversations){
            return new NextResponse('Conversation non trouvée', { status: 404 });
        }

        existingConversations.users.map((user) => {
            if(user.email) {
                pusherServer.trigger(user.email, 'conversation:new', existingConversations);
            }
        })

        return NextResponse.json(existingConversations);
    } catch (error) {
        return new NextResponse('Internal Error', { status: 500 });
    }
}