import bcrypt from 'bcrypt'

import prisma from '@/app/libs/prismadb'
import { NextResponse } from 'next/server'

export async function POST(
    request: Request
){
    try {
        const body = await request.json();
        const {
            email,
            name,
            password,
            cPassword
        } = body;
    
        if (!email || !name || !password || !cPassword){
            return new NextResponse('Missing info', { status: 400 });
        }

        if(name.length < 3){
            return new NextResponse('Le nom doit contenir au moins 3 caractères!', { status: 400 });
        }

        if(!email.includes('.') || !email.includes('@')){
            return new NextResponse('Email invalide!', { status: 400 });
        }

        if(password.length < 8){
            return new NextResponse('Le mot de passe doit contenir au moins 8 caractères!', { status: 400 });
        }

        let hashedPassword = null;

        if(password === cPassword){
            hashedPassword = await bcrypt.hash(password, 12);
        }else{
            return new NextResponse('Les mots de passe ne correspondent pas!', { status: 400 });
        }

        const existingUser = await prisma.user.findUnique({
            where: {
                email
            }
        });

        if(existingUser){
            return new NextResponse('Un utilisateur existe déjà!', { status: 400 });
        }

        let user = null;

        if(hashedPassword){
            user = await prisma.user.create({
                data: {
                    email,
                    name,
                    hashedPassword
                }
            });
        } else {
            return new NextResponse('Internal Error', { status: 500 });
        }

        return NextResponse.json(user);
    } catch (error: any) {
        console.log(error, 'REGISTRATION_ERROR');
        return new NextResponse('Internal Error', { status: 500 });
    }
}