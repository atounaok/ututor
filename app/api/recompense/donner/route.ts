import getCurrentUser from "@/app/actions/getCurrentUser";
import { NextResponse } from "next/server";
import prisma from "@/app/libs/prismadb";
import { pusherServer } from "@/app/libs/pusher";

export async function POST(
    request: Request,
) {
    try {
        const currentUser =  await getCurrentUser();
        const body = await request.json();
        const { statut, conversationId } = body;

        if(!currentUser?.isAdmin){
            return new NextResponse('Vous n\'êtes pas autorisé à faire cette action', { status: 403 });
        }

        if(!statut || !conversationId){
            return new NextResponse('Mauvaise requête pour informations manquantes', { status: 400 });
        }

        // On cherche la conversation
        const conversation = await prisma.conversation.findUnique({
            where:{
                id: conversationId
            },
        })

        if(!conversation){
            return new NextResponse('Conversation introuvable', { status: 404 });
        }

        if(conversation.isActive){
            return new NextResponse('Conversation non terminée', { status: 400 });
        }

        // On cherche la relation tuteur et devoir
        const tutorDevoir = await prisma.devoirTuteurRelation.findUnique({
            where:{
                devoirId: conversation.devoirId,
            },
            include: {
                tutor: true,
            }
        })

        if(!tutorDevoir){
            return new NextResponse('Relation entre devoir et tuteur introuvable', { status: 404 });
        }

        if(tutorDevoir.statut !== 'Traite'){
            return new NextResponse('Devoir non traité', { status: 400 });
        }

        if(tutorDevoir.tutor.id !== currentUser?.id){
            return new NextResponse('Vous ne pouvez pas traité votre propre situation.', { status: 403 });
        }

        // Si tout est bon on crée la demande de recompense
        const donnerRecompense = await prisma.donnerRecompense.create({
            data:{
                statut: statut,
                conversationId: conversationId,
                tutorId: tutorDevoir.tutor.id,
            }
        })

        return NextResponse.json(donnerRecompense, { status: 201 });
    } catch (error) {
        return new NextResponse('Internal Error', { status: 500 });
    }
}