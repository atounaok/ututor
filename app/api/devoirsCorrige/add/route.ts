import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";
import nodemailer from 'nodemailer';
import getCurrentUser from '@/app/actions/getCurrentUser';

export const dynamic = 'force-dynamic';

export async function PUT(request: Request) {

	const body = await request.json();
	const { file, userId, devoirId } = body;

    const currentUser = await getCurrentUser();

    const transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 465,
        secure: true,
        auth: {
            user: 'atounaok@gmail.com',
            pass: "kymo fxhy sitn gnpj",}
     })

     if(!currentUser || !currentUser.isTutor){
        return new NextResponse('Vous ne pouvez pas faire cette action.', { status: 401 });
     }


	try {

		if (!file || !userId || !devoirId) {
			return new NextResponse(`Informations manquantes ou invalide : \n`, {
				status: 400,
			});
		}

        // Récupération du devoir
        const devoir = await prisma.devoir.findUnique({
            include: {
                user: true,
            },
            where: {
                id: devoirId,
            },
        });

        if (!devoir){
            return new NextResponse('Devoir introuvable', { status: 404 });
        }

        const tutor = await prisma.user.findUnique({
            where: {
                id: userId,
            },
        });

        if (!tutor){
            return new NextResponse('Tuteur introuvable', { status: 404 });
        }

        if (!tutor.isTutor || tutor.id !== currentUser.id){
            return new NextResponse('Vous ne pouvez pas effectuer cette action.', { status: 401 });
        }

		const relation = await prisma.devoirTuteurRelation.update({
            where: {
                devoirId: devoirId,
                tutorId: userId,
            },
            data: {
                devoirCorriges: file,
                statut: "En_Revue",
                lastModified: new Date(),
            },
		});

        if(!relation){
            return new NextResponse('La relation entre le devoir et le tuteur est introuvable', { status: 404 });
        }

        // On modifie le statut de l'utilisateur
        const updatedDevoir = await prisma.devoir.update({
            where: {
                id: devoirId,
            },
            data: {
                statut: relation.statut,
            }
        });

        if(!updatedDevoir){
            return new NextResponse('Le devoir est introuvable', { status: 404 });
        }


        const mailOptions = {
            from: 'Équipe UTutor',
            to: `${devoir?.user?.email}`,
            subject: 'Votre devoir est corrigé - UTutor',
            html: `
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Devoir corrigé - UTutor</title>
                </head>
                <body style="font-family: 'Arial', sans-serif;">
                
                    <header style="background-color: #4CAF50; color: white; text-align: center; padding: .5rem;">
                        <h1>Bonjour, ${devoir?.user?.name}</h1>
                    </header>
                
                    <section style="padding: 1em;">
                        <p>${tutor?.name} a corrigé votre devoir.</p>
                        <p>Vous pouvez consulter la correction dans votre messagerie avec ${tutor?.name}.</p>
                        <p>Il attend votre retour pour finaliser la correction et qu'il se fasse rémunérer.</p>
                        <p>Merci pour votre confiance.</p>
                    </section>

                    <section style="padding: 1em;">
                        <p>Vous pouvez aussi contacter votre tuteur à l'adresse courrielle suivante: ${tutor?.email}</p>
                    </section>

                    <section style="padding: 1em;">
                        <p>Consultez le devoir corrigé ici: <a href="${relation.devoirCorriges}">${relation.devoirCorriges}</a></p>
                        <p>Posez vos question à ${tutor.name}: <a href={"https://ututor.ca/conversations/" + }>Messagerie</a></p>
                    </section>
                
                    <footer style="background-color: #4CAF50; color: white; text-align: center; padding: .5rem;">
                        <p>Cordialement,</p>
                        <p>L'équipe de UTutor</p>
                    </footer>
                
                </body>
                </html>
                `,
            }


        await transporter.sendMail(mailOptions)

		return NextResponse.json(200);
	} catch (error) {
		console.log(error, "APPLICATION_ERROR");
		return new NextResponse("Internal Error", { status: 500 });
	}
}
