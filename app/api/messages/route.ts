import getCurrentUser from "@/app/actions/getCurrentUser";
import { NextResponse } from "next/server";
import prisma from "@/app/libs/prismadb";
import { pusherServer } from "@/app/libs/pusher";


export async function POST(
    request: Request
) {
    try {
        const currentUser = await getCurrentUser();
        const body = await request.json();
        const {message, image, conversationId, file} = body;

        if (!currentUser || !currentUser?.id || !currentUser?.email) {
            return new NextResponse('Vous n\'avez pas les autorisations', { status: 401 })
        }

        let messageBody = message;

        if (file) {
            messageBody += ` Voici le lien du devoir corrigé: ${file}`;
        }

        

        
        // Si on a l'id de la conversation, on ajoute le message à la conversation
        if(!conversationId || conversationId.length < 24){
            return new NextResponse('Conversation non trouvée', { status: 400 })
        }

        const newMessage = await prisma.message.create({
            data: {
                body: message,
                image: image ? image : null,
                conversation: {
                    connect: {
                        id: conversationId
                    }
                },
                sender: {
                    connect:{
                        id: currentUser.id
                    }
                },
                seen: {
                    connect: {
                        id: currentUser.id
                    }
                }
            },
            include: {
                seen: true,
                sender: true
            }
        });

        // On ajoute le nouveau message à la conversation
        const updatedConversation = await prisma.conversation.update({
            where: {
                id: conversationId
            },
            data: {
                lastMessageAt: new Date(),
                messages: {
                    connect: {
                        id: newMessage.id
                    }
                }
            },
            include: {
                users: true,
                messages: {
                    include: {
                        seen: true
                    }
                }
            }
        });

        // Realtime update
        await pusherServer.trigger(conversationId, 'messages:new', newMessage);

        const lastMessage = updatedConversation.messages[updatedConversation.messages.length - 1];

        updatedConversation.users.map((user) => {
            pusherServer.trigger(user.email!, 'conversation:update', {
                id: conversationId,
                messages: [lastMessage]
            })
        })

        return NextResponse.json(newMessage);
    } catch (error) {
        console.error(error);
        return new NextResponse('Internal Server Error', { status: 500 })
    }
}