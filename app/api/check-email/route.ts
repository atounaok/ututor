import prisma from "@/app/libs/prismadb";
import { NextResponse } from "next/server";

export async function POST(req: Request) {
    const body = await req.json();
    const { email } = body;

    try {
      const user = await prisma.user.findUnique({
        where: {
          email: email,
        },
      });

      if (user) {
        return NextResponse.json({ exists: true }); 
      } else {
        return NextResponse.json({ exists: false }); 
      }
    } catch (error) {
      console.error('Erreur lors de la vérification de l\'e-mail dans la base de données :', error);
      return new NextResponse('Erreur lors de la vérification de l\'e-mail dans la base de données');
    }
}
