// import qs from 'query-string';
// import { useParams } from 'next/navigation';
// import { useInfiniteQuery } from '@tanstack/react-query';

// import { useSocket } from '@/components/providers/socket-provider';

// interface ConversationQueryParams {
//     queryKey: string;
//     apiUrl: string;
//     paramKey: "conversationId";
//     paramValue: string;
// }

// export const useConversationQuery = ({ queryKey, apiUrl, paramKey, paramValue }: ConversationQueryParams) => {
//     const { isConnected } = useSocket();
//     const params = useParams();

//     const fetchMessages = async ({ pageParam = undefined }) => {
//         const url = qs.stringifyUrl({
//             url: apiUrl,
//             query: {
//                 [paramKey]: paramValue,
//                 cursor: pageParam
//             }
//         }, { skipNull: true });

//         const res = await fetch(url);
//         return res.json();
//     }

//     const {
//         data,
//         fetchNextPage,
//         hasNextPage,
//         isFetchingNextPage,
//         status
//     } = useInfiniteQuery({
//         queryKey: [queryKey],
//         queryFn: fetchMessages,
//         getNextPageParam: (lastPage) => lastPage?.nextCursor,
//         initialPageParam: 1,
//         refetchInterval: isConnected ? false : 1000,
//     });
// };
