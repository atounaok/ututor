import { useCallback, useMemo } from "react";
import { usePathname, useRouter } from "next/navigation";
import { HiChat } from "react-icons/hi";
import { HiArrowLeftOnRectangle, HiUsers } from "react-icons/hi2";
import { signOut } from "next-auth/react";

import  useConversation  from "./useConversation";
import toast from "react-hot-toast";

const useRoutes = () => {
    const router = useRouter();
    const pathname = usePathname();
    const { conversationId } = useConversation();

    const onSignOut = useCallback(() => {
        signOut();
        toast.success('Vous êtes déconnecté.');
        router.push('/auth');
      }, [router]);

    const routes = useMemo(() => [
        {
            label: 'Chat',
            href: '/conversations',
            icon: HiChat,
            active: pathname === '/conversations' && !!conversationId,
        },
        // {
        //     label: 'Users',
        //     href: '/messagerie',
        //     icon: HiUsers,
        //     active: pathname === '/messagerie',
        // },
        {
            label: 'Sign Out',
            href: "/auth",
            onClick: () => onSignOut(),
            icon: HiArrowLeftOnRectangle,
            active: false,
        }
    ], [pathname, conversationId, onSignOut]);

    return routes;
}

export default useRoutes;