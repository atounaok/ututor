import type { Metadata } from "next";

import "./globals.css";
import ToasterContext from "./context/ToasterContext";
import AuthContext from "./context/AuthContext";
import getCurrentUser from "./actions/getCurrentUser";

import Navbar from "@/components/Navbar";
import { Analytics } from "@vercel/analytics/react";
import { SpeedInsights } from "@vercel/speed-insights/next";

import AddHomeWorkTrigger from "@/components/AddHomeWorkTrigger";
import ActiveStatus from "@/components/ActiveStatus";

export const metadata: Metadata = {
  title: "UTutor",
  description: "Plateforme de tutorat en ligne",
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const currentUser = await getCurrentUser();

  return (
    <html lang="fr" className="" data-theme="light">
      <head>
        <link
          rel="stylesheet"
          href="https://fonts.googleapiscomcss2?family=Roboto:wght@400;700&display=swap"
        />
      </head>
      <body>
        <AuthContext>
          <Navbar currentUser={currentUser!} />
          <ToasterContext />
          <ActiveStatus />
          {children}
          <AddHomeWorkTrigger currentUser={currentUser!} />
          <Analytics />
          <SpeedInsights />
        </AuthContext>
      </body>
    </html>
  );
}
