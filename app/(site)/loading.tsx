import Loading from '@/components/Loading';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';

const loading = () => {
  return (
    // <Box sx={{ width: '100%' }}>
    //   <LinearProgress color='secondary'/>
    // </Box>
    <Loading isLoading={true} />
  )
}

export default loading
