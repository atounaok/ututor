"use client"

import CarouselInputAccueil from "./components/accueil/CarouselInputAccueil";


export default function Home() {

  return (
    <main className="flex items-center justify-center h-[calc(100vh-64px)] w-[100vw] fixed -z-10">
      <CarouselInputAccueil />
    </main>
  );
}
