import React, { useRef } from "react";
import Autoplay from "embla-carousel-autoplay";

import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
} from "@/components/ui/carousel";

const CarouselInputAccueil = () => {
  const plugin = useRef(Autoplay({ delay: 10000, stopOnInteraction: true }));
  return (
    <Carousel
      className="w-full h-[calc(100vh-64px)] overflow-hidden "
      plugins={[plugin.current]}
    >
      <CarouselContent className="w-full flex absolute -z-10">
        <CarouselItem
          className="flex flex-col justify-center items-center h-[calc(100vh-64px)] gap-6 bg-cover bg-center w-full"
          style={{ backgroundImage: "url('/images/unsplash/study-2.jpg')" }}
        >
          <div className=" h-full w-full bg-black bg-opacity-25 flex items-end">
            <div className=" mx-2 sm:ms-10 mb-32">
              <h1 className="text-white text-4xl text-center sm:text-7xl font-semibold mb-4">
                Exploration Éducative
              </h1>
              <p className="text-gray-300 text-lg sm:text-xl ms-8 text-center">
                Découvrez un monde d&#39;apprentissage dynamique.
              </p>
            </div>
          </div>
        </CarouselItem>
        <CarouselItem
          className="flex flex-col justify-center items-center h-[calc(100vh-64px)] gap-6 bg-cover bg-center w-full"
          style={{ backgroundImage: "url('/images/unsplash/study-3.jpg')" }}
        >
          <div className=" h-full w-full bg-black bg-opacity-25 flex items-start sm:justify-end justify-center">
            <div className=" sm:me-10 mt-14 backdrop-blur-sm">
              <h1 className="text-white text-4xl sm:text-7xl font-semibold mb-4">
                Échanges Enrichissants
              </h1>
              <p className="text-gray-300 text-xl text-center">
                Partagez des idées, créez des opportunités.
              </p>
            </div>
          </div>
        </CarouselItem>
        <CarouselItem
          className="flex flex-col justify-center items-center h-[calc(100vh-64px)] gap-6 bg-cover bg-center w-full"
          style={{ backgroundImage: "url('/images/unsplash/study-7.jpg')" }}
        >
          <div className=" h-full w-full bg-black bg-opacity-25 flex items-start justify-center">
            <div className=" mt-60">
              <h1 className="text-white text-4xl sm:text-7xl font-semibold mb-4 text-center">
                Votre Réussite, Notre Priorité!
              </h1>
              <p className="text-gray-300 text-sm sm:text-xl text-center">
                Faites confiance à notre engagement envers votre succès
                académique.
              </p>
            </div>
          </div>
        </CarouselItem>
        <CarouselItem
          className="flex flex-col justify-center items-center h-[calc(100vh-64px)] gap-6 bg-cover bg-center w-full"
          style={{ backgroundImage: "url('/images/unsplash/study-6.jpg')" }}
        >
          <div className=" h-full w-full bg-black bg-opacity-25 flex items-start sm:mt-0 mt-60 sm:items-center justify-center">
            <div className=" backdrop-blur-sm rounded-lg  px-6 py-4">
              <h1 className="text-white text-3xl sm:text-7xl font-semibold mb-4 text-center">
                Votre réussite commence ici, maintenant.
              </h1>
              <p className="text-gray-300 text-sm sm:text-xl text-center">
                Cultivez votre succès avec UTutor - Où l&#39;apprentissage
                devient une victoire.
              </p>
            </div>
          </div>
        </CarouselItem>
      </CarouselContent>

      <CarouselPrevious className="absolute z-50 left-8 active:scale-110 border-none" />
      <CarouselNext className="absolute z-50 right-8 active:scale-110 border-none" />
    </Carousel>
  );
};

export default CarouselInputAccueil;
