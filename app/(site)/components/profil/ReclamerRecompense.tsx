import Image from "next/image";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import React from "react";
import { User } from "@prisma/client";
import { useForm, FieldValues, set } from "react-hook-form";
import Input from "@/components/inputs/inputTutor";
import toast from "react-hot-toast";
import axios from "axios";
import { signOut, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import Link from "next/link";
import Loading from "@/components/Loading";

interface ReclamerRecompenseProps {
  currentUser: User;
}

const ReclamerRecompense: React.FC<ReclamerRecompenseProps> = ({
  currentUser,
}) => {
  const router = useRouter();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      userId: currentUser ? currentUser.id : "",
      password: "",
    },
  });

  const aRecompenses = () => {
    if(currentUser.soldeRecompenses < 1){
      console.log("Vous n'avez pas de récompenses.");
      toast.error("Vous n'avez pas de récompenses.");
    }
  }

  const ReclamerRecompense = async (data: any) => {
    setIsLoading(true)
    try {
      if(currentUser?.hashedPassword){
        if (!data.password || !data.userId)
          throw new Error("Veuillez remplir tous les champs!");
  
        // Vérifier si le mot de passe actuel correspond au mot de passe de l'utilisateur
        const mdpResponse = await axios.post("/api/user/check-password", data);
  
        if (!mdpResponse.data.isValid) {
          throw new Error("Le mot de passe est incorrect");
        } else {
          toast.success("Mot de passe valide!");
        }
      }

      const res = await axios.put("/api/user/put/getPaid", {
        userId: currentUser.id, // Envoyer l'ID de l'utilisateur à supprimer
      })
      .then(() => {
        toast.success("Réclamation autorisée!");
        window.location.reload()
      })
      .catch((error) => {
        toast.error(error);
      })
      .finally(() => {
        setIsLoading(false)
      })

    } catch (error: any) {
      toast.error(error.message);
    }
  };

  return (
    <Dialog>
      {/* Bouton Trigger */}
      <DialogTrigger className="w-full" disabled={currentUser?.soldeRecompenses < 1} onClick={() => aRecompenses()}>
        <span  className={currentUser?.soldeRecompenses < 1 ? "disabled w-full btn border-none flex items-center justify-center p-2 bg-green-500 hover:bg-green-300 text-white rounded-md mb-2 gap-2" : "w-full btn border-none flex items-center justify-center p-2 bg-green-500 hover:bg-green-300 text-white rounded-md mb-2 gap-2"}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M2.25 18.75a60.07 60.07 0 0 1 15.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 0 1 3 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 0 0-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 0 1-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 0 0 3 15h-.75M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm3 0h.008v.008H18V10.5Zm-12 0h.008v.008H6V10.5Z"
            />
          </svg>

          <p>Réclamer récompenses</p>
        </span>
      </DialogTrigger>

      {/* Contenu */}
      <DialogContent className="max-h-[70vh] overflow-auto">
          {
            isLoading ? 
            (
            <div className="flex justify-center items-center min-h-[30vh]">
                <span className="loading loading-ball text-purple-400 loading-lg"></span>
              </div>
            ) : ('')
          }
        <form
          className={!isLoading ? "space-y-6 flex flex-col gap-6 max-h-full overflow-auto px-6 sm:px-10 py-8" : "hidden"}
          onSubmit={handleSubmit(ReclamerRecompense)}
        >
          <section>
            <h2 className="text-3xl font-thin text-center">
              Confirmation de réclamation
            </h2>

            <div className="mt-6 flex flex-col gap-4 mb-6">
              {
                currentUser.hashedPassword ? (
                  <p className="text-center text-md text-gray-500 font-thin">
                    Inscrivez votre mot de passe pour confirmer la réclamation. Toutes vos récompenses seront transférées sur votre compte bancaire.
                  </p>
                ) : (
                  <p className="text-center text-md text-gray-500 font-thin">
                    Toutes vos récompenses seront transférées sur votre compte bancaire.
                  </p>
                )
              }
            </div>

              {
                currentUser.hashedPassword ? (
                  <Input
                  id="password"
                  label="Entrez votre mot de passe"
                  type="password"
                  register={register}
                  errors={errors}
                  disabled={isLoading}
                />
                ) : ('')
              }
          </section>
          <section className="flex justify-center">
            <button className="btn w-full bg-green-500 border-none text-white hover:bg-green-400 flex gap-2 rounded-md">
            <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M2.25 18.75a60.07 60.07 0 0 1 15.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 0 1 3 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 0 0-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 0 1-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 0 0 3 15h-.75M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm3 0h.008v.008H18V10.5Zm-12 0h.008v.008H6V10.5Z"
            />
          </svg>
              Confirmer
            </button>
          </section>
        </form>
      </DialogContent>
    </Dialog>
  );
};

export default ReclamerRecompense;
