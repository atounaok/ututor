import Image from "next/image";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import React from "react";
import { User } from "@prisma/client";
import { useForm, FieldValues, set } from "react-hook-form";
import Input from "@/components/inputs/inputTutor";
import toast from "react-hot-toast";
import axios from "axios";
import { useRouter } from "next/navigation";

import {
  GlowingStarsBackgroundCard,
  GlowingStarsDescription,
  GlowingStarsTitle,
} from "@/components/ui/glowing-stars";

interface UpdateProfileFormProps {
  currentUser: User;
}

const InfosPerso: React.FC<UpdateProfileFormProps> = ({ currentUser }) => {
  const router = useRouter();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      userId: currentUser ? currentUser.id : "",
      name: currentUser ? currentUser.name || "" : "",
      email: currentUser ? currentUser.email || "" : "",
      tel: currentUser ? currentUser.telephone || "" : "",
    },
  });

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      if (!data.name || !data.email || !data.tel)
        throw new Error("Veuillez remplir tous les champs!");

      console.log(data);

      const response = await axios.put("/api/user/put/infos", data);

      // Vérifier si la requête a réussi
      if (response.status === 200) {
        toast.success("Informations mises à jour avec succès!");
        window.location.reload();
      } else {
        throw new Error("Échec de la mise à jour des informations");
      }
    } catch (error: any) {
      toast.error(
        "Une erreur est survenue lors de la mise à jour de vos informations: " +
          error.message
      );
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Dialog>
      {/* Bouton Trigger */}
      <DialogTrigger className="antialiased border-none h-full w-full p-0 m-0">
        {/* <div className="flex flex-col justify-between items-center ">
          <div className="">
            <h3 className="text-center font-thin text-2xl">
              Informations personnelles
            </h3>
          </div>
          <div className="">
            <Image
              alt="mail"
              height={150}
              width={150}
              src={"/images/utils/edit-profile.png"}
            />
          </div>
          <div className="flex items-center justify-center gap-2">
            <p className="text-sm">Mettre à jour les Informations</p>
            <Image
              alt="mail"
              height={10}
              width={10}
              src={"/images/utils/right-arrow.png"}
            />
          </div>
        </div> */}

        <GlowingStarsBackgroundCard className="border-none w-full h-full">
          <GlowingStarsTitle className="text-start flex gap-2 items-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M17.982 18.725A7.488 7.488 0 0 0 12 15.75a7.488 7.488 0 0 0-5.982 2.975m11.963 0a9 9 0 1 0-11.963 0m11.963 0A8.966 8.966 0 0 1 12 21a8.966 8.966 0 0 1-5.982-2.275M15 9.75a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"
              />
            </svg>
            <p>Modifer mes infos</p>
          </GlowingStarsTitle>
          <div className="flex justify-between items-end">
            <GlowingStarsDescription className="text-start">
              Changer vos informations personnelles
            </GlowingStarsDescription>
            <div className="h-8 w-8 rounded-full bg-[hsla(0,0%,100%,.1)] flex items-center justify-center">
              <Icon />
            </div>
          </div>
        </GlowingStarsBackgroundCard>
      </DialogTrigger>

      {/* Contenu */}
      <DialogContent className="card max-h-[70vh] max-w-[90vw] lg:max-w-[25vw] overflow-auto">
      {
            isLoading ? 
            (
            <div className="flex justify-center items-center min-h-[30vh]">
                <span className="loading loading-ball text-purple-400 loading-lg"></span>
              </div>
            ) : ('')
          }
        <form
          className={!isLoading ? "space-y-6 flex flex-col gap-6 max-h-full overflow-auto px-6 sm:px-10 py-8" : "hidden"}
          onSubmit={handleSubmit(onSubmit)}
        >
          <section>
            <h2 className="text-2xl font-thin text-center">
              Informations personnelles
            </h2>

            <div className="mt-6 flex flex-col gap-4">
              <Input
                id="name"
                label="Nom complet"
                register={register}
                errors={errors}
                disabled={isLoading}
                value={currentUser ? currentUser.name! : ""}
              />

              <Input
                id="email"
                label="Email"
                type="email"
                register={register}
                errors={errors}
                disabled={isLoading}
                value={currentUser ? currentUser.email! : ""}
              />

              <Input
                id="tel"
                label="Téléphone"
                type="tel"
                register={register}
                errors={errors}
                disabled={isLoading}
                value={currentUser ? currentUser.telephone! : ""}
              />
            </div>
          </section>
          <section className="flex justify-center">
            <button className="btn border-none w-full bg-green-500 py-2 px-6 rounded-md text-white hover:bg-green-400">
              Mettre à jour
            </button>
          </section>
        </form>
      </DialogContent>
    </Dialog>
  );
};

const Icon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth="1.5"
      stroke="currentColor"
      className="h-4 w-4 text-white stroke-2"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"
      />
    </svg>
  );
};

export default InfosPerso;
