"use client";

import Image from "next/image";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import React, { useState } from "react";
import { User } from "@prisma/client";
import { useForm, FieldValues, set } from "react-hook-form";
import Input from "@/components/inputs/inputTutor";
import toast from "react-hot-toast";
import axios from "axios";
import { useRouter } from "next/navigation";
import Link from "next/link";

import {
  GlowingStarsBackgroundCard,
  GlowingStarsDescription,
  GlowingStarsTitle,
} from "@/components/ui/glowing-stars";

interface CandidatureFormProps {
  currentUser: User;
}

const Candidature: React.FC<CandidatureFormProps> = ({ currentUser }) => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [candidature, setCandidature] = useState<any>(null);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      userId: currentUser ? currentUser.id : "",
      password: "",
      newPassword: "",
      cNewPassword: "",
    },
  });

  const getCandidature = async () => {
    try {
      const response = await axios.get("/api/candidatures/get/getByUserId", {
        params: {
          userId: currentUser.id,
        },
      });

      if (response.status === 200) {
        const Candidature = response.data;
        console.log("Candidature: " + Candidature);
        setCandidature(Candidature);
      } else {
        throw new Error(
          "Échec de la récupération des informations de candidature!"
        );
      }
    } catch (error: any) {
      toast.error(error.message);
    }
  };

  React.useEffect(() => {
    if (currentUser && currentUser.hasApplied) {
      getCandidature();
    }
  });
  return (
    <div className="">
      {currentUser && currentUser.hasApplied && candidature ? (
        <Dialog>
          {/* Bouton Trigger */}
          <DialogTrigger className="antialiased border-none h-full w-full p-0 m-0">
            {/* <div className="flex flex-col justify-between items-center h-full w-full">
                            <div className="">
                                <h3 className="text-center font-thin text-2xl">Informations de ma Candidature</h3>
                            </div>
                            <div className="">
                                <Image
                                    alt="mail"
                                    height={130}
                                    width={130}
                                    src={"/images/utils/writing.png"}/>
                            </div>
                            <div className="flex items-center justify-center gap-2">
                                <p className='text-sm'>Voir ma candidature</p>
                                <Image
                                    alt="mail"
                                    height={10}
                                    width={10}
                                    src={"/images/utils/right-arrow.png"}/>
                            </div>
                        </div> */}

            <GlowingStarsBackgroundCard className="border-none w-full h-full">
              <GlowingStarsTitle className="text-start flex gap-2 items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M4.26 10.147a60.438 60.438 0 0 0-.491 6.347A48.62 48.62 0 0 1 12 20.904a48.62 48.62 0 0 1 8.232-4.41 60.46 60.46 0 0 0-.491-6.347m-15.482 0a50.636 50.636 0 0 0-2.658-.813A59.906 59.906 0 0 1 12 3.493a59.903 59.903 0 0 1 10.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.717 50.717 0 0 1 12 13.489a50.702 50.702 0 0 1 7.74-3.342M6.75 15a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Zm0 0v-3.675A55.378 55.378 0 0 1 12 8.443m-7.007 11.55A5.981 5.981 0 0 0 6.75 15.75v-1.5"
                  />
                </svg>

                <p>Ma candidature</p>
              </GlowingStarsTitle>
              <div className="flex justify-between items-end">
                <GlowingStarsDescription className="text-start">
                  Voir ma candidature
                </GlowingStarsDescription>
                <div className="h-8 w-8 rounded-full bg-[hsla(0,0%,100%,.1)] flex items-center justify-center">
                  <Icon />
                </div>
              </div>
            </GlowingStarsBackgroundCard>
          </DialogTrigger>

          {/* Contenu */}
          <DialogContent className="card max-h-[90vh] max-w-[90vw] lg:max-w-[25vw] overflow-auto">
            <div className="flex flex-col items-center justify-center gap-4">
              <h3 className="text-2xl font-thin text-center">
                Informations de ma Candidature
              </h3>
              <div className="flex flex-col gap-4 w-full">
                <div className="flex flex-col gap-2 w-full mb-4">
                  <h2 className="mb-2">Candidat</h2>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Nom: </span>
                    <span>{candidature.user.name}</span>
                  </p>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Email: </span>
                    <span>{candidature.user.email}</span>
                  </p>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Téléphone: </span>
                    <span>{candidature.user.telephone}</span>
                  </p>
                </div>
                <div className="flex flex-col gap-2 w-full mb-4">
                  <h2 className="mb-2">Candidature</h2>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Numéro DA: </span>
                    <span>{candidature.infosCandidature.da}</span>
                  </p>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Matière: </span>
                    <span>{candidature.infosCandidature.matieres}</span>
                  </p>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Niveau scolaire: </span>
                    <span>{candidature.infosCandidature.niveau}</span>
                  </p>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Disponibilité: </span>
                    <span>
                      {new Date(
                        candidature.infosCandidature.dispo
                      ).toLocaleDateString()}
                    </span>
                  </p>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Date de candidature: </span>
                    <span>
                      {new Date(candidature.createdAt).toLocaleDateString()}
                    </span>
                  </p>
                </div>
                <div className="flex flex-col gap-2 w-full mb-4">
                  <h2 className="mb-2">Documents</h2>
                  <p className="font-thin text-lg flex justify-between">
                    <span>CV: </span>
                    <span>
                      <a
                        className="text-blue-500 hover:text-blue-300 underline"
                        href={candidature.infosCandidature.cv}
                        target="_blank"
                      >
                        Télécharger
                      </a>
                    </span>
                  </p>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Lettre de motivation: </span>
                    <span>
                      <a
                        className="text-blue-500 hover:text-blue-300 underline"
                        href={candidature.infosCandidature.lettreMotivation}
                        target="_blank"
                      >
                        Télécharger
                      </a>
                    </span>
                  </p>
                </div>
                <div className="flex flex-col gap-2 w-full">
                  <h2 className="mb-2">Statut</h2>
                  <p className="font-thin text-lg flex justify-between">
                    <span>État: </span>
                    <span
                      className={
                        candidature.isAccepted
                          ? `text-green-500`
                          : "text-red-500"
                      }
                    >
                      {candidature.isTreated
                        ? "Traitement terminé!"
                        : "En traitement"}
                    </span>
                  </p>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Admission: </span>
                    <span
                      className={
                        candidature.isAccepted
                          ? `text-green-500`
                          : "text-red-500"
                      }
                    >
                      {candidature.isAccepted
                        ? "Vous êtes admis!"
                        : "Votre candidature est refusée"}
                    </span>
                  </p>
                  <p className="font-thin text-lg flex justify-between">
                    <span>Date de traitement: </span>
                    <span>
                      {new Date(candidature.dateTreated).toLocaleDateString()}
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </DialogContent>
        </Dialog>
      ) : (
        <Dialog>
          {/* Bouton Trigger */}
          <DialogTrigger className="antialiased border-none h-full w-full p-0 m-0">
            {/* <div className="btn hover:bg-white bg-white shadow-sm hover:shadow-lg p-3 flex flex-col justify-between items-center h-[100%] w-[100%]">
                            <div className="">
                                <h3 className="text-center font-thin text-2xl">Je veux devenir tuteur</h3>
                            </div>
                            <div className="">
                                <Image
                                    alt="mail"
                                    height={100}
                                    width={100}
                                    src={"/images/utils/writing.png"}/>
                            </div>
                            <div className="flex items-center justify-center gap-2">
                                <p className='text-sm'>Devenir tuteur</p>
                                <Image
                                    alt="mail"
                                    height={10}
                                    width={10}
                                    src={"/images/utils/right-arrow.png"}/>
                            </div>
                        </div> */}

            <GlowingStarsBackgroundCard className="border-none w-full h-full">
              <GlowingStarsTitle className="text-start flex gap-2 items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M4.26 10.147a60.438 60.438 0 0 0-.491 6.347A48.62 48.62 0 0 1 12 20.904a48.62 48.62 0 0 1 8.232-4.41 60.46 60.46 0 0 0-.491-6.347m-15.482 0a50.636 50.636 0 0 0-2.658-.813A59.906 59.906 0 0 1 12 3.493a59.903 59.903 0 0 1 10.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.717 50.717 0 0 1 12 13.489a50.702 50.702 0 0 1 7.74-3.342M6.75 15a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Zm0 0v-3.675A55.378 55.378 0 0 1 12 8.443m-7.007 11.55A5.981 5.981 0 0 0 6.75 15.75v-1.5"
                  />
                </svg>

                <p>Devenir tuteur</p>
              </GlowingStarsTitle>
              <div className="flex justify-between items-end">
                <GlowingStarsDescription className="text-start">
                  Je veux devenir tuteur
                </GlowingStarsDescription>
                <div className="h-8 w-8 rounded-full bg-[hsla(0,0%,100%,.1)] flex items-center justify-center">
                  <Icon />
                </div>
              </div>
            </GlowingStarsBackgroundCard>
          </DialogTrigger>

          {/* Contenu */}
          <DialogContent className="card max-h-[70vh] overflow-auto flex items-center justify-center flex-col">
            <p className="px-6 text-center">
              Vous n&#39;avez pas encore postulé pour devenir tuteur.
            </p>
            <Link
              href={"/tuteur"}
              className="btn border-none w-full bg-green-500 hover:bg-green-300 px-8 py-2 text-center text-white"
            >
              Postuler
            </Link>
          </DialogContent>
        </Dialog>
      )}
    </div>
  );
};

const Icon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth="1.5"
      stroke="currentColor"
      className="h-4 w-4 text-white stroke-2"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"
      />
    </svg>
  );
};

export default Candidature;
