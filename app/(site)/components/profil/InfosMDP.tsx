import Image from "next/image";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import React from "react";
import { User } from "@prisma/client";
import { useForm, FieldValues, set } from "react-hook-form";
import Input from "@/components/inputs/inputTutor";
import toast from "react-hot-toast";
import axios from "axios";
import { useRouter } from "next/navigation";

import {
  GlowingStarsBackgroundCard,
  GlowingStarsDescription,
  GlowingStarsTitle,
} from "@/components/ui/glowing-stars";

interface UpdateProfileFormProps {
  currentUser: User;
}

const InfosMDP: React.FC<UpdateProfileFormProps> = ({ currentUser }) => {
  const router = useRouter();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      userId: currentUser ? currentUser.id : "",
      password: "",
      newPassword: "",
      cNewPassword: "",
    },
  });

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      if (!data.password || !data.newPassword || !data.cNewPassword)
        throw new Error("Veuillez remplir tous les champs!");

      // Vérifier si le mot de passe actuel correspond au mot de passe de l'utilisateur
      const mdpResponse = await axios.post("/api/user/check-password", data);

      if (!mdpResponse.data.isValid) {
        throw new Error("Le mot de passe actuel est incorrect");
      } else {
        toast.success("Mot de passe actuel valide!");
      }

      if (data.newPassword.length < 8)
        throw new Error("Le mot de passe doit contenir au moins 8 caractères!");

      if (data.newPassword !== data.cNewPassword)
        throw new Error("Les mots de passe ne correspondent pas!");

      if (data.newPassword === data.password)
        throw new Error(
          "Le nouveau mot de passe doit être différent du mot de passe actuel!"
        );

      const response = await axios.put("/api/user/put/password", data);

      // Vérifier si la requête a réussi
      if (response.status === 200) {
        toast.success("Mot de passe mises à jour avec succès!");
        window.location.reload();
      } else {
        throw new Error("Échec de la mise à jour des informations");
      }
    } catch (error: any) {
      toast.error(error.message);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <div className="">
      {currentUser && currentUser.hashedPassword ? (
        <Dialog>
          {/* Bouton Trigger */}
          <DialogTrigger className="antialiased border-none h-full w-full p-0 m-0">
            {/* <div className="flex flex-col justify-between items-center">
              <div className="">
                <h3 className="text-center font-thin text-2xl">Mot de passe</h3>
              </div>
              <div className="">
                <Image
                  alt="mail"
                  height={130}
                  width={130}
                  src={"/images/utils/padlock.png"}
                />
              </div>
              <div className="flex items-center justify-center gap-2">
                <p className="text-sm">Mettre à jour les Informations</p>
                <Image
                  alt="mail"
                  height={10}
                  width={10}
                  src={"/images/utils/right-arrow.png"}
                />
              </div>
            </div> */}

            <GlowingStarsBackgroundCard className="border-none w-full h-full">
              <GlowingStarsTitle className="text-start flex gap-2 items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M16.5 10.5V6.75a4.5 4.5 0 1 0-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 0 0 2.25-2.25v-6.75a2.25 2.25 0 0 0-2.25-2.25H6.75a2.25 2.25 0 0 0-2.25 2.25v6.75a2.25 2.25 0 0 0 2.25 2.25Z"
                  />
                </svg>

                <p>Modifier mot de passe</p>
              </GlowingStarsTitle>
              <div className="flex justify-between items-end">
                <GlowingStarsDescription className="text-start">
                  Changer votre mot de passe
                </GlowingStarsDescription>
                <div className="h-8 w-8 rounded-full bg-[hsla(0,0%,100%,.1)] flex items-center justify-center">
                  <Icon />
                </div>
              </div>
            </GlowingStarsBackgroundCard>
          </DialogTrigger>

          {/* Contenu */}
          <DialogContent className="card max-h-[70vh] max-w-[90vw] lg:max-w-[25vw] overflow-auto rounded-xl">
            {
              isLoading ? 
              (
              <div className="flex justify-center items-center min-h-[30vh]">
                  <span className="loading loading-ball text-purple-400 loading-lg"></span>
                </div>
              ) : ('')
            }
            <form
              className={!isLoading ? "space-y-6 flex flex-col gap-6 max-h-full overflow-auto px-6 sm:px-10 py-8" : "hidden"}
              onSubmit={handleSubmit(onSubmit)}
            >
              <section>
                <h2 className="text-2xl font-thin text-center">
                  Changer le mot de passe
                </h2>

                <div className="mt-6 flex flex-col gap-4">
                  <Input
                    id="password"
                    label="Entrez votre mot de passe actuel"
                    type="password"
                    register={register}
                    errors={errors}
                    disabled={isLoading}
                  />
                  <Input
                    id="newPassword"
                    label="Entrez votre nouveau mot de passe"
                    type="password"
                    register={register}
                    errors={errors}
                    disabled={isLoading}
                  />
                  <Input
                    id="cNewPassword"
                    label="Confirmez le nouveau mot de passe actuel"
                    type="password"
                    register={register}
                    errors={errors}
                    disabled={isLoading}
                  />
                </div>
              </section>
              <section className="flex justify-center">
                <button className="btn w-full border-none bg-green-500 rounded-md py-2 px-6 text-white hover:bg-green-400">
                  Mettre à jour
                </button>
              </section>
            </form>
          </DialogContent>
        </Dialog>
      ) : (
        <Dialog>
          {/* Bouton Trigger */}
          <DialogTrigger className="antialiased border-none h-full w-full p-0 m-0">
            {/* <div className="flex flex-col justify-between items-center">
              <div className="">
                <h3 className="text-center font-thin text-2xl">Mot de passe</h3>
              </div>
              <div className="">
                <Image
                  alt="mail"
                  height={130}
                  width={130}
                  src={"/images/utils/padlock.png"}
                />
              </div>
              <div className="flex items-center justify-center gap-2">
                <p className="text-sm">Mettre à jour les Informations</p>
                <Image
                  alt="mail"
                  height={10}
                  width={10}
                  src={"/images/utils/right-arrow.png"}
                />
              </div>
            </div> */}

            <GlowingStarsBackgroundCard className="border-none w-full h-full">
              <GlowingStarsTitle className="text-start flex gap-2 items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M16.5 10.5V6.75a4.5 4.5 0 1 0-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 0 0 2.25-2.25v-6.75a2.25 2.25 0 0 0-2.25-2.25H6.75a2.25 2.25 0 0 0-2.25 2.25v6.75a2.25 2.25 0 0 0 2.25 2.25Z"
                  />
                </svg>

                <p>Modifier mot de passe</p>
              </GlowingStarsTitle>
              <div className="flex justify-between items-end">
                <GlowingStarsDescription className="text-start">
                  Changer votre mot de passe
                </GlowingStarsDescription>
                <div className="h-8 w-8 rounded-full bg-[hsla(0,0%,100%,.1)] flex items-center justify-center">
                  <Icon />
                </div>
              </div>
            </GlowingStarsBackgroundCard>
          </DialogTrigger>

          {/* Contenu */}
          <DialogContent className="card max-h-[70vh] max-w-[90vw] lg:max-w-[25vw] overflow-auto rounded-xl">
            <div className="p-6 ">
              <h2 className="text-xl font-semibold mb-4 text-center">
                Impossible de changer le mot de passe!
              </h2>
              <p className="text-gray-700 mb-4 font-thin text-justify">
                Nous sommes ravis de vous voir sur notre plateforme.{" "}
                <span className="text-red-500">
                  Vous êtes connecté avec un compte externe.
                </span>{" "}
                Malheureusement, cela signifie que vous ne pouvez pas changer
                votre mot de passe directement depuis notre site. Cependant, si
                vous avez besoin d&#39;assistance ou si vous rencontrez des
                problèmes, n&#39;hésitez pas à nous contacter via notre service
                d&#39;assistance.
              </p>
              <p className="text-gray-700 mb-4 font-thin text-justify">
                Vous pouvez également explorer notre site et profiter de toutes
                les fonctionnalités que nous proposons. Si vous avez des
                questions, notre équipe est là pour vous aider.
              </p>
              <p className="text-gray-700 font-thin text-center">
                Merci de faire partie de notre communauté!{" "}
                <span className="text-purple-500">- UTutor</span>
              </p>
            </div>
          </DialogContent>
        </Dialog>
      )}
    </div>
  );
};

const Icon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth="1.5"
      stroke="currentColor"
      className="h-4 w-4 text-white stroke-2"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"
      />
    </svg>
  );
};

export default InfosMDP;
