import Image from "next/image";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import React from "react";
import { User } from "@prisma/client";
import { useForm, FieldValues, set } from "react-hook-form";
import Input from "@/components/inputs/inputTutor";
import toast from "react-hot-toast";
import axios from "axios";
import { signOut, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import Link from "next/link";

interface UpdateProfileFormProps {
  currentUser: User;
}

const InfosPerso: React.FC<UpdateProfileFormProps> = ({ currentUser }) => {
  const router = useRouter();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      userId: currentUser ? currentUser.id : "",
      password: "",
    },
  });

  const deleteAccount = async (data: any) => {
    try {
      if (!data.password || !data.userId)
        throw new Error("Veuillez remplir tous les champs!");

      // Vérifier si le mot de passe actuel correspond au mot de passe de l'utilisateur
      const mdpResponse = await axios.post("/api/user/check-password", data);

      if (!mdpResponse.data.isValid) {
        throw new Error("Le mot de passe est incorrect");
      } else {
        toast.success("Mot de passe valide!");
      }

      // Envoyer une demande pour supprimer le compte (vous devez implémenter cette fonctionnalité dans votre API)
      const response = await axios.delete("/api/user/delete", {
        data: { userId: currentUser.id }, // Envoyer l'ID de l'utilisateur à supprimer
      });

      if (response.status === 200) {
        // Si la suppression du compte est réussie, déconnectez l'utilisateur
        signOut();
        router.push("/auth");
        toast.success("Votre compte a été supprimé avec succès!");
      } else {
        // Sinon, afficher un message d'erreur
        toast.error("Erreur lors de la suppression du compte!");
      }
    } catch (error: any) {
      toast.error(error.message);
    }
  };

  return (
    <Dialog>
      {/* Bouton Trigger */}
      <DialogTrigger className="w-full">
        <span className="w-full btn border-none flex items-center justify-center p-2 bg-red-500 hover:bg-red-300 text-white rounded-md mb-2 gap-2">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"
            />
          </svg>

          <p>Supprimer le compte</p>
        </span>
      </DialogTrigger>

      {/* Contenu */}
      <DialogContent className="max-h-[70vh] overflow-auto">
        <form
          className="space-y-6 flex flex-col gap-6 max-h-full overflow-auto px-6 sm:px-10 py-8"
          onSubmit={handleSubmit(deleteAccount)}
        >
          <section>
            <h2 className="text-3xl font-thin text-center">
              Confirmation de suppression
            </h2>

            <div className="mt-6 flex flex-col gap-4 mb-6">
              <p className="text-center text-md text-gray-500 font-thin">
                Voulez-vous vraiment supprimer votre compte? <br />
                Cette action est non réversible.
              </p>
            </div>

            <Input
              id="password"
              label="Entrez votre mot de passe"
              type="password"
              register={register}
              errors={errors}
              disabled={isLoading}
            />
          </section>
          <section className="flex justify-center">
            <button className="btn w-full bg-red-500 border-none text-white hover:bg-red-400 flex gap-2 rounded-md">
              <Image
                alt="mail"
                height={20}
                width={20}
                src={"/images/utils/delete(1).png"}
              />
              Supprimer le compte
            </button>
          </section>
        </form>
      </DialogContent>
    </Dialog>
  );
};

export default InfosPerso;
