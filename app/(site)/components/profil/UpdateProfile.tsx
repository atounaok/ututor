"use client";

import Image from "next/image";
import { User } from "@prisma/client";
import React, { useEffect, useState } from "react";
import { signOut, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import toast from "react-hot-toast";
import InfosPerso from "./InfosPerso";
import InfosMDP from "./InfosMDP";
import DeleteUser from "./DeleteUser";
import Candidature from "./Candidature";
import { Avatar } from "@material-tailwind/react";
import { Spotlight } from "@/components/ui/Spotlight";
import MesDevoirs from "./MesDevoirs";
import ReclamerRecompense from "./ReclamerRecompense";

interface UpdateProfileFormProps {
  currentUser: User;
}

const UpdateProfile: React.FC<UpdateProfileFormProps> = ({ currentUser }) => {
  const router = useRouter();
  const [disabled, setDisabled] = useState<boolean>(true);

  return (
    <div className="h-[80vh] flex justify-center items-center">
      <div className="h-full w-full grid lg:grid-cols-6 justify-center items-center gap-6">
        {/* Côté gauche */}
        <div className="card hover:bg-white bg-white border h-full lg:col-span-2 min-h-[70vh]  p-4 flex flex-col justify-between">
          <div className="w-full flex justify-center items-center flex-col mb-4">
            <div className="avatar">
              <div
                className={
                  currentUser?.isTutor || currentUser?.isAdmin
                    ? "rounded-full shadow-md mb-4 ring ring-primary ring-purple-500 ring-offset-base-100 ring-offset-2"
                    : "rounded-full shadow-md mb-4 ring ring-primary ring-purple-100 ring-offset-base-100 ring-offset-2"
                }
              >
                <Image
                  alt="PFP"
                  className="rounded-full"
                  height={100}
                  width={100}
                  src={
                    currentUser?.image
                      ? currentUser?.image
                      : `https://i.pravatar.cc/150?u=${currentUser?.id}`
                  }
                />
              </div>
            </div>
            <h2 className="font-thin text-4xl text-center">
              {currentUser ? currentUser.name : ""}
            </h2>
          </div>
          <div className="text-center font-thin h-[50%]">
            <p>
              {currentUser && !currentUser?.isTutor && !currentUser?.isAdmin
                ? "Vous n'etes pas tuteur"
                : ""}
            </p>
            <p>
              {currentUser && currentUser?.isTutor && !currentUser?.isAdmin
                ? "Tuteur"
                : ""}
            </p>
            <p>{currentUser && currentUser?.isAdmin ? "Administrateur" : ""}</p>

            <p className="flex justify-center items-center gap-2 mt-4">
              <Image
                alt="mail"
                height={20}
                width={20}
                src={"/images/utils/telephone.png"}
              />
              {currentUser ? currentUser.telephone : ""}
            </p>

            <p className="flex justify-center items-center gap-2 mt-4">
              <Image
                alt="mail"
                height={20}
                width={20}
                src={"/images/utils/mail.png"}
              />
              {currentUser ? currentUser.email : ""}
            </p>

            {
              currentUser?.isTutor ? (            
                <p className="flex justify-center items-center gap-2 mt-4">
                  Solde de récompenses: { currentUser.soldeRecompenses }
                </p>) : ('')
            }
          </div>

          <div className="flex flex-col gap-2">
            <button
              className="btn border-none w-full flex items-center justify-center p-2 hover:bg-gray-200 rounded-md mb-2"
              onClick={() => signOut()}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M8.25 9V5.25A2.25 2.25 0 0 1 10.5 3h6a2.25 2.25 0 0 1 2.25 2.25v13.5A2.25 2.25 0 0 1 16.5 21h-6a2.25 2.25 0 0 1-2.25-2.25V15m-3 0-3-3m0 0 3-3m-3 3H15"
                />
              </svg>

              <p>Se déconnecter</p>
            </button>

            {
              currentUser?.isTutor ? (<ReclamerRecompense currentUser={currentUser!} />) : ('')
            }
            
            <DeleteUser currentUser={currentUser!} />
          </div>
        </div>

        {/* Côté droit */}
        <div className="h-full grid lg:grid-cols-2 lg:grid-rows-2 lg:col-span-4 gap-6 lg:pb-0 pb-20">
          <InfosPerso currentUser={currentUser!} />
          <InfosMDP currentUser={currentUser!} />
          <MesDevoirs currentUser={currentUser!} />
          {
            currentUser?.isAdmin ? ('') : <Candidature currentUser={currentUser!} />
          }
        </div>
      </div>
    </div>
  );
};

export default UpdateProfile;
