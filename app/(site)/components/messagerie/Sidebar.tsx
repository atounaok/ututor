import React from 'react'
import DesktopSidebar from './DesktopSidebar';
import MobileFooter from './MobileFooter';
import getCurrentUser from '@/app/actions/getCurrentUser';

const Sidebar = async ({ children } : { children: React.ReactNode }) => {
    const currentUser = await getCurrentUser();
    
  return (
    <div className='max-h-[calc(100vh-64px)]'>
        <DesktopSidebar currentUser={currentUser!}/>
        <MobileFooter />
        <main className='lg:pl-20 h-full'>
            {children}
        </main>
    </div>
  )
}

export default Sidebar
