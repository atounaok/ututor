import React from 'react'

const EmptyState = () => {
  return (
    <div 
        className='
            px-4 py-8 h-[calc(100vh-64px)] top-[64px]
            sm:px-6 lg:px-8
            flex justify-center items-center
            bg-gray-100
        '
    >
      
      <div className="
        text-center
        items-center
        flex flex-col
      ">
        <h3 className='
            mt-2
            text-2xl
            font-semibold
            text-gray-400
        '>
            Sélectionnez une conversation
        </h3>
      </div>
    </div>
  )
}

export default EmptyState
