'use client'

import { User } from '@prisma/client';
import React, { useEffect, useState } from 'react'

interface AnciennesCandidaturesFormProps {
    currentUser: User;
}

const AnciennesCandidatures: React.FC<AnciennesCandidaturesFormProps> = ({
    currentUser
}) => {
    const [loading, setLoading] = useState(false);
    const [anciennesCandidatures, setAnciennesCandidatures] = useState<any>([]);

    useEffect(() => {
        // Déclarez une fonction asynchrone pour récupérer les devoirs
        const fetchAnciennesCandidatures = async () => {
            try {
                // Appelez votre API pour récupérer tous les devoirs
                const response = await fetch('/api/candidatures/get/getOld'); // Assurez-vous que l'URL est correcte
                const data = await response.json();

                // Mettez à jour l'état avec les devoirs récupérés
                setAnciennesCandidatures(data);
            } catch (error) {
                console.error('Erreur lors de la récupération des devoirs', error);
            }
        };

        // Appelez la fonction pour récupérer les devoirs au moment du rendu initial
        fetchAnciennesCandidatures();
    }, []);

    // Fonction pour formater la date
    const formatDate = (dateString: string) => {
        const options: Intl.DateTimeFormatOptions = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            hour12: false, // Use 24-hour format
        };
        const date = new Date(dateString);
        const formattedDateTime = date.toLocaleDateString('fr-CA', options);

        return formattedDateTime.replace(' ', ' | ');
    };

  return (
    <ul className='m-4 p-2 border h-[30vh] max:h-[30vh] overflow-auto rounded-lg'>
        <div className="grid lg:grid-cols-5 grid-cols-2 p-2 border-b font-thin">
            <span>Nom du candidat</span>
            <span className='hidden lg:block'>Courriel</span>
            <span className='hidden lg:block'>Numéro DA</span>
            <span className='hidden lg:block'>Date de traitement</span>
            <span>Admission</span>
        </div>
        {loading ? (
                <div className="flex items-center justify-center h-auto">
                    <span className="loading loading-dots loading-lg"></span>
                </div>
            ) : anciennesCandidatures && anciennesCandidatures.length > 0 ? (
                anciennesCandidatures.map((candidature: any) => (
                    <a key={candidature.id} href={'/admin/candidatures/' + candidature.id} className='grid grid-cols-2 lg:grid-cols-5 p-2 border-b hover:bg-slate-100'>
                        <span className='truncate'>{candidature.user.name}</span>
                        <span className='hidden lg:block'>{candidature.user.email}</span>
                        <span className='hidden lg:block'>{candidature.infosCandidature.da}</span>
                        <span className='hidden lg:block'>{formatDate(candidature.dateTreated)}</span>
                        <span className={candidature.isAccepted ? 'font-thin text-green-500' : 'font-thin text-red-500'}>{candidature.isAccepted ? 'Autorisé' : 'Refusé'}</span>
                    </a>
                ))
            ) : (
                <div className="flex items-center justify-center h-auto">
                   <p>Pas de candidature traitée pour le moment</p>
                </div>
            )}
    </ul>
  )
}

export default AnciennesCandidatures
