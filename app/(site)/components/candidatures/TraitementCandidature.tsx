'use client'

import { User } from '@prisma/client';
import axios from 'axios';
import Link from 'next/link';
import React, { useEffect, useState } from 'react'
import Image from 'next/image';
import toast from 'react-hot-toast';
import { useRouter } from 'next/navigation';
import { useSession } from 'next-auth/react';

interface InfosCandidaturesFormProps {
    currentUser: User;
    candidatureId: string;
}

const TraitementCandidature: React.FC<InfosCandidaturesFormProps> = ({
    currentUser,
    candidatureId
}) => {
    const [loading, setLoading] = useState(false);
    const [candidature, setCandidature] = useState<any>([]);

    const router = useRouter();

    useEffect(() => {
        // Déclarez une fonction asynchrone pour récupérer les devoirs
        setLoading(true);
        const fetchCandidatures = async () => {
            try {
                // Appelez votre API pour récupérer tous les devoirs
                const response = await axios.get('/api/candidatures/get/getById', {
                    params: {
                        candidatureId: candidatureId,
                    },
                });

                // Mettez à jour l'état avec les devoirs récupérés
                setCandidature(response.data);
            } catch (error) {
                console.error('Erreur lors de la récupération des devoirs', error);
            }
            finally{
                setLoading(false);
            }
        };

        // Appelez la fonction pour récupérer les devoirs au moment du rendu initial
        fetchCandidatures();
    }, [candidatureId]);

    const onRefuseAdmission = async () => {
        try {
            setLoading(true);
            // Appelez votre API pour refuser l'admission
            const response = await axios.put('/api/candidatures/put/refuseAdmission', {
                candidatureId: candidatureId,
                userId: currentUser.id,
                candidatMail: candidature?.infosCandidature?.email,
            });

            if(response.status === 200) {
                setCandidature(response.data);
    
                // Affichez un message de succès
                toast.success('Admission refusée avec succès');
            }

        } catch (error) {
            console.error('Erreur lors du refus de l\'admission', error);
            // Affichez un message d'erreur
            toast.error('Erreur lors du refus de l\'admission');
        } finally  {
            setLoading(false);
        }
    }

    const onAutoriseAdmission = async () => {
        try {
            // Appelez votre API pour autoriser l'admission
            const response = await axios.put('/api/candidatures/put/autoriseAdmission', {
                candidatureId: candidatureId,
                userId: currentUser.id,
                candidatMail: candidature?.infosCandidature?.email,
            });

            if(response.status === 200){
                setCandidature(response.data);

                // Affichez un message de succès
                toast.success('Admission autorisée avec succès');
            }
        } catch (error) {
            console.error('Erreur lors de l\'autorisation de l\'admission', error);
            // Affichez un message d'erreur
            toast.error('Erreur lors de l\'autorisation de l\'admission');
        }
    }

  return (
    <div className='py-4 min-h-[80vh] w-full flex flex-col justify-between'>
        <div className="h-2/6 flex flex-col gap-4 p-4 rounded-lg">
            <h2 className='text-2xl font-thin'>Informations de contact</h2>
            <div className="flex flex-col gap-4">
                <div className="flex justify-between border-b py-1">
                    <p>Nom</p>
                    <p className='font-thin'>{candidature?.infosCandidature?.name}</p>
                </div>
                <div className="flex justify-between border-b py-1">
                    <p>Email</p>
                    <p className='font-thin'>{candidature?.infosCandidature?.email}</p>
                </div>
                <div className="flex justify-between border-b py-1">
                    <p>Téléphone</p>
                    <Link href="tel: +14500000000" className='font-thin hover:border-b'>{candidature?.infosCandidature?.telephone}</Link>
                </div>
                <div className="flex justify-between border-b py-1">
                    <p>Date de disponibilité</p>
                    <p className='font-thin'>{candidature?.infosCandidature?.dispo}</p>
                </div>
            </div>
        </div>
        <div className="flex flex-col justify-center gap-4 h-3/6 p-4">
        <h2 className='text-2xl font-thin'>Qualifications</h2>
            <div className="flex flex-col gap-4">
                <div className="flex justify-between border-b py-1">
                    <p>Numéro DA</p>
                    <p className='font-thin'>{candidature?.infosCandidature?.da}</p>
                </div>
                <div className="flex justify-between border-b py-1">
                    <p>Niveau scolaire</p>
                    <p className='font-thin'>{candidature?.infosCandidature?.niveau}</p>
                </div>
                <div className="flex justify-between border-b py-1">
                    <p>Matière voulue</p>
                    <p className='font-thin'>{candidature?.infosCandidature?.matieres}</p>
                </div>
            </div>
            <div className="flex justify-center mt-4 gap-4">
                <Link href={candidature?.infosCandidature?.cv ? candidature?.infosCandidature?.cv : ''} target='_blank'
                className="
                    border btn-link
                    flex py-4 px-4
                    items-center 
                    justify-center 
                    gap-2 rounded-xl 
                    bg-cover shadow-sm hover:shadow-lg
                " 
                >
                    <Image
                        alt="Logo"
                        height="48"
                        width="48"
                        className="mx-auto w-auto"
                        src="/images/utils/cv.png"/>
                    <p>CV: Curriculum vitae</p>
                </Link>
                <Link href={candidature?.infosCandidature?.lettre_motivation ? candidature?.infosCandidature?.lettre_motivation : ''} target='_blank'
                className="
                border btn-link
                flex py-4 px-4
                items-center 
                justify-center 
                gap-2 rounded-xl 
                bg-cover shadow-sm hover:shadow-lg
                " 
                >
                    <Image
                        alt="Logo"
                        height="48"
                        width="48"
                        className="mx-auto w-auto"
                        src="/images/utils/writing.png"/>
                    <p>Lettre de motivation</p>
                </Link>
            </div>
        </div>
        <div className="h-1/6">
            {
                candidature?.isTreated ? 
                <p className='text-green-500 font-thin text-center'>Cette candidature a déjà été traitée!</p> : (
                    <div className="flex gap-4 justify-center">
                        <button className="btn bg-red-500 hover:bg-red-400 text-white border-none" onClick={() => {onRefuseAdmission()}}>Refuser l&#39;admission</button>
                        <button className="btn bg-green-500 hover:bg-green-400 text-white border-none" onClick={() => {onAutoriseAdmission()}}>Autoriser l&#39;admission</button>
                    </div>
                )
            }
        </div>
    </div>
  )
}

export default TraitementCandidature
