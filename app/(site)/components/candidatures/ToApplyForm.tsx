"use client";

import Button from "@/components/AuthButton";
import Input from "@/components/inputs/inputTutor";
import InputSelectTutor from "@/components/inputs/InputSelectTutor";
import { useEffect, useRef, useState } from "react";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import Image from "next/image";
import axios from "axios";
import toast from "react-hot-toast";
import { User } from "@prisma/client";
import { useRouter } from "next/navigation";
import Autoplay from "embla-carousel-autoplay";
import { useSession } from "next-auth/react";

import { twMerge } from "tailwind-merge";
import { TracingBeam } from "@/components/ui/tracing-beam";

import * as React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import Fab from "@mui/material/Fab";
import CheckIcon from "@mui/icons-material/Check";
import { green } from "@mui/material/colors";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";

import type { PutBlobResult } from "@vercel/blob";
import CarouselApplyForm from "./CarouselApplyForm";

interface ApplyFormProps {
  currentUser: User;
}

const ToApplyForm: React.FC<ApplyFormProps> = ({ currentUser }) => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);

  const inputCVFileRef = useRef<HTMLInputElement>(null);
  const inputLettreFileRef = useRef<HTMLInputElement>(null);
  const [cvBlob, setCVBlob] = useState<PutBlobResult | null>(null);
  const [lettreBlob, setLettreBlob] = useState<PutBlobResult | null>(null);

  //const inputFileRef = useRef<HTMLInputElement>(null);
  const [blob, setBlob] = useState<PutBlobResult | null>(null);

  const timer = React.useRef<number>();

  const buttonSx = {
    ...(success && {
      bgcolor: green[500],
      "&:hover": {
        bgcolor: green[700],
      },
    }),
  };

  React.useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, []);

  useEffect(() => {
    if (currentUser?.hasApplied === true) {
      toast.error("Vous avez déjà envoyé une candidature!");
      router.push("/");
    }
  }, [currentUser, router]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      name: "",
      email: "",
      tel: "",
      da: "",
      niveau: "",
      domaine: "",
      matiere: "",
      dispo: "",
      cvFile: "",
      lettreFile: "",
    },
  });

  const onSubmit: SubmitHandler<FieldValues> = async (data) => {
    setSuccess(false);
    setIsLoading(true);

    console.log("Je suis dans le onSubmit");

    try {
      if (!currentUser) {
        toast.error("Impossible de récupérer l'ID de l'utilisateur.");
        return;
      }

      if (!inputCVFileRef.current?.files) {
        throw new Error("Pas de fichier sélectionné pour le cv");
      }

      const cvFile = inputCVFileRef.current.files[0];

      const cvResponse = await fetch(
        `/api/file/upload?filename=${cvFile.name}`,
        {
          method: "POST",
          body: cvFile,
        }
      );

      const newCvBlob = (await cvResponse.json()) as PutBlobResult;

      setCVBlob(newCvBlob);

      if (!inputLettreFileRef.current?.files) {
        throw new Error(
          "Pas de fichier sélectionné pour la lettre de motivation"
        );
      }

      const lettreFile = inputCVFileRef.current.files[0];

      const lettreResponse = await fetch(
        `/api/file/upload?filename=${lettreFile.name}`,
        {
          method: "POST",
          body: lettreFile,
        }
      );

      const newLettreBlob = (await lettreResponse.json()) as PutBlobResult;

      setLettreBlob(newCvBlob);

      const userId = currentUser.id;

      axios
        .post("/api/apply", {
          ...data,
          userId,
          cvFile: newCvBlob.url,
          lettreFile: newLettreBlob.url,
        })
        .then((response) => {
          if (response.status === 200) {
            setSuccess(true);
            toast.success(
              "Merci, votre candidature a été soumise avec succès!"
            );
            setIsLoading(false);
            router.push("/");
            // timer.current = window.setTimeout(() => {

            // }, 1000);
          } else {
            toast.error(
              "1: Une erreur s'est produite lors de la soumission de la candidature."
            );
            setIsLoading(false);
          }
        })
        .catch((error) => {
          // La requête a échoué, affichez un message d'erreur
          console.error(error);
          timer.current = window.setTimeout(() => {
            toast.error(
              "2: Une erreur s'est produite lors de la soumission de la candidature."
            );
            setIsLoading(false);
          }, 1000);
        });
    } catch (error) {
      toast.error("Il y'a une erreur: " + error);
    } finally {
      setIsLoading(false);
    }
  };

  const niveau = ["Primaire", "Secondaire", "Collegial", "Universitaire"];
  const matieres = [
    "Mathematiques",
    "Informatique",
    "Francais",
    "Anglais",
    "Philosophie",
    "Geographie",
  ];

  return (
    <div
      className="
        flex 
        sm:mx-auto max-h-[calc(100vh-64px)]
        w-full overflow-hidden
    "
    >
      {/* Loading */}
      <div
        className={
          isLoading
            ? "fixed h-[calc(100vh-64px)] w-screen flex items-center justify-center backdrop-blur-sm z-50 p-10 rounded-md"
            : "hidden"
        }
      >
        <div className=" p-16 rounded-lg drop-shadow-lg backdrop-blur-lg">
          <Box sx={{ m: 1, position: "relative" }}>
            <Fab
              aria-label="save"
              color="primary"
              sx={{ backgroundColor: "transparent", ...buttonSx }}
            >
              {success ? <CheckIcon /> : ""}
            </Fab>
            {isLoading && !success && (
              <CircularProgress
                size={68}
                sx={{
                  color: green[500],
                  position: "absolute",
                  top: -6,
                  left: -6,
                  zIndex: 1,
                }}
              />
            )}
          </Box>
        </div>
      </div>

      {/* Carousel */}
      <CarouselApplyForm />

      {/* Formulaire */}
      <div
        className="
            bg-white
            lg:w-1/3
            sm:w-1/2
            w-full
        "
      >
        <form
          className="space-y-6 flex flex-col gap-6 max-h-full overflow-auto px-6 sm:px-10 py-8 overflow-y-auto"
          onSubmit={handleSubmit(onSubmit)}
        >
          <h2 className="text-2xl font-bold text-center">
            Formulaire de candidature
          </h2>

          {/* Infos perso */}
          <section>
            <h2 className="text-2xl font-thin">Informations personnelles</h2>

            <div className="mt-6 flex flex-col gap-4">
              <Input
                id="name"
                label="Nom complet"
                register={register}
                errors={errors}
                disabled={isLoading}
              />

              <Input
                id="email"
                label="Email"
                type="email"
                register={register}
                errors={errors}
                disabled={isLoading}
              />

              <Input
                id="tel"
                label="Téléphone"
                type="tel"
                register={register}
                errors={errors}
                disabled={isLoading}
              />

              <Input
                id="da"
                label="Numéro DA"
                maxlength={7}
                register={register}
                errors={errors}
                disabled={isLoading}
              />
            </div>
          </section>

          {/* Infos académiques */}
          <section>
            <h2 className="text-2xl font-thin">Informations Académiques</h2>

            <div className="mt-6 flex flex-col gap-4">
              <InputSelectTutor
                id="niveau"
                label="Niveau d'études"
                options={niveau}
                optionPlaceholder="choisir entre (primaire, secondaire, collégial, universitaire)"
                required
                register={register}
                errors={errors}
                disabled={isLoading}
              />

              <Input
                id="domaine"
                label="Domaine d'études"
                register={register}
                errors={errors}
                disabled={isLoading}
              />
            </div>
          </section>

          {/* Matières */}
          <section>
            <h2 className="text-2xl font-thin mb-4">
              Matières de Spécialisation{" "}
            </h2>
            <p className="text-sm font-thin text-gray-500">
              Sélectionnez une matière dans laquelle vous souhaitez être tuteur
              (mathématiques, français, sciences, etc.).
            </p>

            <div className="mt-6 flex flex-col gap-4 justify-center">
              <InputSelectTutor
                id="matiere"
                label="Matière"
                options={matieres}
                optionPlaceholder="Choisissez une matière"
                required
                register={register}
                errors={errors}
                disabled={isLoading}
              />
            </div>
          </section>

          {/* Heures dispos */}
          <section>
            <h2 className="text-2xl font-thin">Disponibilité</h2>
            <p className="text-sm font-thin text-gray-500 mb-6">
              Heures et jours de disponibilité pour le tutorat. Vous avez le
              droit à 2 jours par semaine maximum
            </p>

            <Input
              id="dispo"
              label="Date et heures"
              type="datetime-local"
              placeholder="(primaire, secondaire, collégial, universitaire)"
              register={register}
              errors={errors}
              disabled={isLoading}
            />
          </section>

          {/* Envoie PDF */}
          <section>
            <h2 className="text-2xl font-thin">
              Joindre un Curriculum Vitae (CV)
            </h2>
            <p className="text-sm font-thin text-gray-500 mb-6">
              Parlez-nous de vous et de ce qui vous motive à vouloir aider
              d&#39;autres étudiants.
            </p>

            {/* Input pour CV */}
            <label
              htmlFor="cvFile"
              className="
                            block
                            text-sm
                            font-medium
                            leading-6
                            text-gray-900 mb-2
                        "
            >
              Curriculum Vitae
            </label>
            <input
              name="cvFile"
              ref={inputCVFileRef}
              type="file"
              disabled={isLoading}
              required
              className="
                            file-input file-input-ghost w-full mt-2 file-input-bordered
                        "
            />

            {/* Input pour lettre de motivation */}
            <label
              htmlFor="cvFile"
              className="
                            block mt-4
                            text-sm
                            font-medium
                            leading-6
                            text-gray-900 mb-2
                        "
            >
              Lettre de motivation
            </label>
            <input
              name="lettreFile"
              ref={inputLettreFileRef}
              type="file"
              disabled={isLoading}
              required
              className="
                            file-input file-input-ghost w-full mt-2 file-input-bordered
                        "
            />
          </section>

          {/* Termes et conditions */}
          <section>
            <h2 className="text-2xl font-thin">Acceptation des Conditions</h2>

            <div className="mt-6 flex">
              <div className="flex items-center gap-2">
                <input
                  type="checkbox"
                  defaultChecked
                  className="checkbox checkbox-xs"
                />
                <label htmlFor="terms" className="text-sm text-gray-700 me-1">
                  J&#39;accepte les termes et conditions.
                </label>
              </div>

              <Dialog>
                <DialogTrigger className="underline">
                  Lire les conditions
                </DialogTrigger>
                <DialogContent className="max-h-[70vh] overflow-auto">
                  <DialogHeader>
                    <DialogTitle className="mb-6 text-2xl text-center">
                      Termes et Conditions d&#39;utilisation
                    </DialogTitle>
                    <DialogDescription className="">
                      <div className="sm:max-w-[50vw] flex flex-col gap-8">
                        <div className="">
                          <h2 className="text-lg font-semibold mb-2">
                            Bienvenue sur UTutor !
                          </h2>
                          <p className="font-thin">
                            Ces termes et conditions régissent votre utilisation
                            de notre site web. En utilisant notre site, vous
                            acceptez ces termes dans leur intégralité. Si vous
                            n&#39;êtes pas d&#39;accord avec ces termes,
                            veuillez ne pas utiliser notre site.
                          </p>
                        </div>

                        <div className="">
                          <h2 className="text-lg font-semibold mb-2">
                            Droits de Propriété Intellectuelle
                          </h2>
                          <p className="font-thin">
                            Le contenu de ce site, y compris mais sans s&#39;y
                            limiter, le texte, les graphiques, les images, le
                            logo, est la propriété de UTutor et est protégé par
                            les lois sur le droit d&#39;auteur et les marques de
                            commerce.
                          </p>
                        </div>

                        <div className="">
                          <h2 className="text-lg font-semibold mb-2">
                            Utilisation du Site
                          </h2>
                          <p className="font-thin">
                            Vous acceptez de n&#39;utiliser le site que dans un
                            but légal et conforme à ces termes et conditions.
                            Vous ne devez pas utiliser le site de manière à
                            causer des dommages, une interruption ou une
                            détérioration du site.
                          </p>
                        </div>

                        <div className="">
                          <h2 className="text-lg font-semibold mb-2">
                            Informations Personnelles
                          </h2>
                          <p className="font-thin">
                            Lorsque vous utilisez notre site, vous pouvez être
                            invité à fournir des informations personnelles. Vous
                            acceptez que ces informations soient collectées et
                            utilisées conformément à notre politique de
                            confidentialité.
                          </p>
                        </div>

                        <div className="">
                          <h2 className="text-lg font-semibold mb-2">
                            Liens Externes
                          </h2>
                          <p className="font-thin">
                            Notre site peut contenir des liens vers des sites
                            externes. Nous ne sommes pas responsables du contenu
                            de ces sites et n&#39;endossons pas leur contenu.
                          </p>
                        </div>

                        <div className="">
                          <h2 className="text-lg font-semibold mb-2">
                            Modifications des Termes et Conditions
                          </h2>
                          <p className="font-thin">
                            Nous nous réservons le droit de modifier ces termes
                            et conditions à tout moment. Les modifications
                            entrent en vigueur dès leur publication sur le site.
                          </p>
                        </div>

                        <div className="">
                          <h2 className="text-lg font-semibold mb-2">
                            Contact
                          </h2>
                          <p className="font-thin">
                            Si vous avez des questions ou des préoccupations
                            concernant ces termes et conditions, veuillez nous
                            contacter à{" "}
                            <span className=" text-blue-700">
                              info@ututor.ca
                            </span>
                            .
                          </p>
                        </div>
                      </div>
                    </DialogDescription>
                  </DialogHeader>
                </DialogContent>
              </Dialog>
            </div>
          </section>

          {/* Soumettre form */}
          <div>
            <Button disabled={isLoading} fullWidth type="submit">
              Soumettre
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ToApplyForm;
