'use client'

import { User } from '@prisma/client';
import React, { useEffect, useState } from 'react'
 
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import toast from 'react-hot-toast';

interface NouvellesCandidaturesFormProps {
    currentUser: User;
}

const NouvellesCandidatures: React.FC<NouvellesCandidaturesFormProps> = ({
    currentUser
}) => {
    const [loading, setLoading] = useState(false);
    const [nouvellesCandidatures, setNouvellesCandidatures] = useState<any>([]);
    
    const router = useRouter();

    useEffect(() => {
        // Déclarez une fonction asynchrone pour récupérer les devoirs
        setLoading(true);
        const fetchNouvellesCandidatures = async () => {
            try {
                // Appelez votre API pour récupérer tous les devoirs
                const response = await fetch('/api/candidatures/get/getNew');
                const data = await response.json();

                // Mettez à jour l'état avec les devoirs récupérés
                setNouvellesCandidatures(data);
            } catch (error) {
                console.error('Erreur lors de la récupération des devoirs', error);
            }
            finally{
                setLoading(false);
            }
        };

        // Appelez la fonction pour récupérer les devoirs au moment du rendu initial
        fetchNouvellesCandidatures();
    }, []);

    // Fonction pour formater la date
    const formatDate = (dateString: string) => {
        const options: Intl.DateTimeFormatOptions = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            hour12: false, // Use 24-hour format
        };
        const date = new Date(dateString);
        const formattedDateTime = date.toLocaleDateString('fr-CA', options);

        return formattedDateTime.replace(' ', ' | ');
    };
    
  return (
    <ul className='m-4 p-2 border h-[30vh] max:h-[30vh] overflow-auto rounded-lg'>
        <div className="grid lg:grid-cols-5 grid-cols-2 p-2 border-b font-thin">
            <span>Nom du candidat</span>
            <span className='hidden lg:block'>Courriel</span>
            <span className='hidden lg:block'>Numéro DA</span>
            <span className='hidden lg:block'>Date</span>
            <span>Statut</span>
        </div>
        {loading ? (
                <div className="flex items-center justify-center h-auto">
                    <span className="loading loading-dots loading-lg"></span>
                </div>
            ) : nouvellesCandidatures && nouvellesCandidatures.length > 0 ? (
                nouvellesCandidatures.map((candidature: any) => (
                    <Link key={candidature.id} 
                        href={{
                            pathname: `/admin/candidatures/${candidature.id}`,
                            query: { 
                            candidat: candidature.infosCandidature.name 
                            }
                        }} 
                        className='grid grid-cols-2 lg:grid-cols-5 p-2 border-b hover:bg-slate-100'>
                        <span className='truncate'>{candidature.user.name}</span>
                        <span className='hidden lg:block'>{candidature.user.email}</span>
                        <span className='hidden lg:block'>{candidature.infosCandidature.da}</span>
                        <span className='hidden lg:block'>{formatDate(candidature.createdAt)}</span>
                        <span className='font-thin'>{candidature.isTreated ? 'Traitée' : 'À traiter'}</span>
                    </Link>
                ))
            ) : (
                <div className="flex items-center justify-center h-auto">
                    <p>Aucune candidature pour le moment</p>
                </div>
            )}
    </ul>
  )
}

export default NouvellesCandidatures
