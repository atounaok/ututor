import { plugin } from 'postcss'
import React, { useRef } from 'react'

import {
    Carousel,
    CarouselContent,
    CarouselItem,
    CarouselNext,
    CarouselPrevious,
  } from "@/components/ui/carousel"
import Autoplay from 'embla-carousel-autoplay'

const CarouselApplyForm = () => {
    const plugin = useRef(
        Autoplay({ delay: 10000, stopOnInteraction: true })
      )
  return (
    <div className="lg:w-2/3 sm:w-1/2 hidden sm:flex flex-col justify-center items-center bg-[#141414] text-white h-full max-h-[calc(100vh-64px)] ">
    <Carousel className="w-[100%] max-h-[calc(100vh-64px)] overflow-hidden" plugins={[plugin.current]}>
        <CarouselContent>
            <CarouselItem className="flex flex-col justify-between h-[calc(100vh-64px)] gap-6 bg-cover bg-center" style={{ backgroundImage: "url('/images/unsplash/student-1.jpg')" }}>
                <div className=" h-full w-full bg-black bg-opacity-25 flex items-end">
                <div className=" mx-2 sm:ms-10 mb-32">
                    <h1 className="text-white text-4xl text-center sm:text-8xl font-semibold mb-4">Devenez tuteur</h1>
                    <p className="text-gray-300 text-lg sm:text-xl text-center">Devenez un Tuteur UTutor et guidez le chemin vers l&#39;excellence.</p>
                </div>
                </div>
            </CarouselItem>
            <CarouselItem className="flex flex-col justify-between h-[calc(100vh-64px)] gap-6 bg-cover bg-center" style={{ backgroundImage: "url('/images/unsplash/student-2.jpg')" }}>
                <div className=" h-full w-full bg-black bg-opacity-25 flex items-start">
                <div className="sm:ms-24 mt-32">
                    <h1 className="text-white text-4xl sm:text-8xl font-semibold mb-4">Aidez les autres</h1>
                    <p className="text-gray-300 text-lg sm:text-xl text-center max-w-[70%]">Faites une différence - En offrant votre aide, vous pouvez changer des vies et contribuer à la réussite des autres.</p>
                </div>
                </div>
            </CarouselItem>
            <CarouselItem className="flex flex-col justify-between h-[calc(100vh-64px)] gap-6 bg-cover bg-center" style={{ backgroundImage: "url('/images/unsplash/student-3.jpg')" }}>
                <div className=" h-full w-full bg-black bg-opacity-25 flex items-start justify-center">
                <div className="mt-32">
                    <h1 className="text-white text-4xl text-center sm:text-8xl font-semibold mb-4">Gagnez de l&#39;argent</h1>
                    <p className="text-gray-300 text-lg sm:text-xl text-center">Gagnez de l&#39;argent tout en partageant votre expertise avec d&#39;autres étudiants.</p>
                </div>
                </div>
            </CarouselItem>
        </CarouselContent>
        <CarouselPrevious className="fixed left-8 hidden"/>
        <CarouselNext className="fixed right-8 hidden"/>
    </Carousel>
</div>
  )
}

export default CarouselApplyForm
