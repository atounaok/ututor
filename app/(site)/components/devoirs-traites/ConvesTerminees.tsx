"use client";

import { User } from "@prisma/client";
import axios from "axios";
import { formatDate } from "date-fns";
import { Link } from "lucide-react";
import React, { useEffect, useState } from "react";
import toast from "react-hot-toast";

interface ConvesTermineesProps {
  currentUser: User;
}

const ConvesTerminees: React.FC<ConvesTermineesProps> = ({ currentUser }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [conversations, setConversations] = useState([]);

  // On cherche les conversations terminées
  useEffect(() => {
    setIsLoading(true);
    try {
      console.log("On cherche les devoirs traités");
      const conves = axios
        .get("/api/conversations/aExaminer")
        .then((response) => {
          setConversations(response.data);
        })
        .catch((error) => {
          console.error(error);
        });
    } catch (error) {
      toast.error("Une erreur est survenue, veuillez réessayer.");
    } finally {
      setIsLoading(false);
    }
  }, []);

  return (
    <div>
      {isLoading ? (
        <span className="loading loading-ball loading-lg text-purple-500"></span>
      ) : (
        <ul className="m-4 p-2 border h-[30vh] max:h-[30vh] overflow-auto rounded-lg">
          <div className="p-2 border-b font-thin">
            <span>Nom du candidat</span>
            {/* <span className="hidden lg:block">Courriel</span>
            <span className="hidden lg:block">Numéro DA</span>
            <span className="hidden lg:block">Date</span>
            <span>Statut</span> */}
          </div>
          {isLoading ? (
            <div className="flex items-center justify-center h-auto">
              <span className="loading loading-dots loading-lg"></span>
            </div>
          ) : conversations && conversations.length > 0 ? (
            conversations.map((convo: any) => (
              <Link
                key={convo.id}
                href={`/admin/conversations/${convo.id}`}
                className=" p-2 border-b hover:bg-slate-100"
              >
                <span className="truncate">convo name: {convo.id}</span>
                {/* <span className="hidden lg:block">
                  {candidature.user.email}
                </span>
                <span className="hidden lg:block">
                  {candidature.infosCandidature.da}
                </span>
                <span className="hidden lg:block">
                  {formatDate(candidature.createdAt)}
                </span>
                <span className="font-thin">
                  {candidature.isTreated ? "Traitée" : "À traiter"}
                </span> */}
              </Link>
            ))
          ) : (
            <div className="flex items-center justify-center h-auto">
              <p>Aucune candidature pour le moment</p>
            </div>
          )}
        </ul>
      )}
    </div>
  );
};

export default ConvesTerminees;
