"use client";

import Button from "@/components/AuthButton";
import Input from "@/components/inputs/Input";
import { useCallback, useEffect, useState } from "react";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import AuthSocialButton from "./AuthSocialButton";
import { BsGithub, BsGoogle } from "react-icons/bs";
import axios from "axios";
import toast from "react-hot-toast";
import { signIn, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";

import { MultiStepLoader as Loader } from "@/components/ui/multi-step-loader";
import { IconSquareRoundedX } from "@tabler/icons-react";

type Varient = "LOGIN" | "REGISTER";

const loadingStates = [
	{
		text: "On récupère vos informations",
	},
	{
		text: "On valide vos informations",
	},
	{
		text: "Nous envoyons vos informations au serveur",
	},
	{
		text: "Votre compte est créé!",
	},
	{
		text: "Nous préparons votre transfert",
	},
	{
		text: "Merci pour votre confiance! Vous allez être redirigé.",
	},
];

const AuthForm = () => {
	const session = useSession();
	const router = useRouter();
	const [inputError, setInputError] = useState<boolean>(false);
	const [varient, setVarient] = useState<Varient>("LOGIN");
	const [emailExists, setEmailExists] = useState(false);
	const [isLoading, setIsLoading] = useState(false);

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<FieldValues>({
		defaultValues: {
			name: "",
			email: "",
			password: "",
			cPassword: "",
		},
	});

	const checkEmailExists = async (email: string) => {
		try {
			const response = await axios.post("/api/check-email", { email });
			const { exists } = response.data; // Extrayez la propriété 'exists' de la réponse JSON
			setEmailExists(exists); // Mettez à jour l'état en fonction de la réponse
			if (exists) {
				toast.error("Cet e-mail est déjà utilisé!");
				setInputError(true);
			} else {
				setInputError(false);
			}
		} catch (error) {
			console.log("Erreur lors de la vérification de l'e-mail :", error);
		}
	};

	useEffect(() => {
		if (session?.status === "authenticated") {
			toast.success("Vous êtes connecté!");
			//window.location.reload();
			router.push("/");
		}
	}, [session?.status, router]);

	const toggleVarient = useCallback(() => {
		if (varient === "LOGIN") {
			setVarient("REGISTER");
		} else {
			setVarient("LOGIN");
		}
	}, [varient]);

	const validation = (data: FieldValues) => {
		if (varient === "REGISTER") {
			if (!data.name || !data.email || !data.password || !data.cPassword) {
				toast.error("Veuillez remplir tous les champs!");
				setIsLoading(false);
				return;
			}

			if (data.name.length < 3) {
				toast.error("Le nom doit contenir au moins 3 caractères!");
				setIsLoading(false);
				return;
			}

			if (!data.email.includes(".") || !data.email.includes("@")) {
				toast.error("Email invalide!");
				setIsLoading(false);
				return;
			}

			if (data.password.length < 8) {
				toast.error("Le mot de passe doit contenir au moins 8 caractères!");
				setIsLoading(false);
				return;
			}

			if (data.password !== data.cPassword) {
				toast.error("Les mots de passe ne correspondent pas!");
				setIsLoading(false);
				return;
			}
		}

		if (varient === "LOGIN") {
			if (!data.email) {
				return "L'email est requis!";
			}

			if (!data.password) {
				return "Le mot de passe est requis!";
			}
		}
	};

	const onSubmit: SubmitHandler<FieldValues> = (data) => {
		setIsLoading(true);

		if (varient === "REGISTER") {
			// Validation
			const validationError = validation(data);

			if (validationError) {
				toast.error(validationError);
				setIsLoading(false);
				return;
			}

			if (!validationError) {
				// Axios on register
				axios
					.post("/api/register", data)
					.then(() => {
						setTimeout(() => {
							signIn("credentials", data);
							setIsLoading(false);
						}, 6000);
					})
					.catch(() => toast.error("Erreur lors de l'inscription"));
				//   .finally(() => setIsLoading(false));
			}
		}

		if (varient === "LOGIN") {
			// Validation
			const validationError = validation(data);

			if (validationError) {
				toast.error(validationError);
				setIsLoading(false);
				return;
			} else {
				// NextAuth Sign in
				signIn("credentials", {
					...data,
					redirect: false,
				})
					.then((callback) => {
						if (callback?.error) {
							toast.error(
								"Information de connexions invalides: " + callback.error
							);
						}

						if (callback?.ok && !callback?.error) {
							// toast.success("Vous êtes connecté!");
							//window.location.reload();
						}
					})
					.finally(() => setIsLoading(false));
			}
		}
	};

	const socialAction = (action: string) => {
		setIsLoading(true);

		// NextAuth social Sign in
		signIn(action, { redirect: false })
			.then((callback) => {
				if (callback?.error) {
					toast.error("Information de connexions invalides");
				}

				if (callback?.ok && !callback?.error) {
					toast.success("Vous êtes connecté!");
				}
			})
			.finally(() => setIsLoading(false));
	};

	return (
		<div
			className="
            mt-8 w-[85vw]
            sm:mx-auto
            sm:w-full
            sm:max-w-md        
        "
		>
			{varient === "REGISTER" && (
				<Loader
					loadingStates={loadingStates}
					loading={isLoading}
					duration={1000}
					loop={false}
				/>
			)}

			<div
				className="
                glassmorphism
                px-4
                py-8
                shadow
                rounded-lg
                sm:px-10
            "
			>
				<form className="space-y-6" onSubmit={handleSubmit(onSubmit)}>
					{varient === "REGISTER" && (
						<Input
							id="name"
							label="Nom"
							register={register}
							errors={errors}
							disabled={isLoading}
						/>
					)}
					<Input
						id="email"
						label="Email"
						type="email"
						inputError={inputError}
						onBlur={(e: any) => {
							const email = e.target.value;
							if (email && varient === "REGISTER") {
								checkEmailExists(email);
							}
						}}
						register={register}
						errors={errors}
						disabled={isLoading}
					/>

					<Input
						id="password"
						label="Mot de passe"
						type="password"
						register={register}
						errors={errors}
						disabled={isLoading}
					/>

					{varient === "REGISTER" && (
						<Input
							id="cPassword"
							label="Confirmer le mot de passe"
							type="password"
							register={register}
							errors={errors}
							disabled={isLoading}
						/>
					)}
					<div>
						<Button disabled={isLoading} fullWidth type="submit">
							{varient === "LOGIN" ? "Se connecter" : "S'inscrire"}
						</Button>
					</div>
				</form>

				<div className="mt-6">
					<div className="relative">
						<div
							className="
                                absolute
                                inset-0
                                flex
                                items-center
                            "
						>
							<div className="w-full border-t border-gray-300"></div>
						</div>
						<div className="relative flex justify-center text-sm">
							<span
								className="
                                    bg-white 
                                    px-2 
                                    text-gray-500"
							>
								Ou continuer avec
							</span>
						</div>
					</div>

					<div
						className="
                            mt-6
                            flex
                            gap-2
                        "
					>
						<AuthSocialButton
							icon={BsGoogle}
							onClick={() => socialAction("google")}
						/>
						<AuthSocialButton
							icon={BsGithub}
							onClick={() => socialAction("github")}
						/>
					</div>
				</div>

				<div
					className="
                    flex
                    sm:flex-row
                    flex-col
                    items-center
                    gap-2
                    justify-center
                    text-sm
                    mt-6
                    px-2
                    text-gray-500
                "
				>
					<div>
						{varient === "LOGIN"
							? "Vous n'avez pas de compte?"
							: "Vous avez déjà un compte?"}
					</div>
					<div className="underline cursor-pointer" onClick={toggleVarient}>
						{varient === "LOGIN" ? "Créer un compte" : "Se connecter"}
					</div>
				</div>
			</div>
		</div>
	);
};

export default AuthForm;
