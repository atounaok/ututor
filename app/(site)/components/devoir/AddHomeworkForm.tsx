"use client";

import Button from "@/components/AuthButton";
import Input from "@/components/inputs/Input";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import axios from "axios";
import toast from "react-hot-toast";
import { signIn, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { Textarea } from "@material-tailwind/react";
import TextArea from "@/components/inputs/Textarea";
import { User } from "@prisma/client";
import InputSelectTutor from "@/components/inputs/InputSelectTutor";


import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import CheckIcon from '@mui/icons-material/Check';
import { green } from '@mui/material/colors';

interface AddHomeworkProps {
	currentUser: User;
}

import type { PutBlobResult } from '@vercel/blob';

const AddHomeworkForm: React.FC<AddHomeworkProps> = ({ currentUser }) => {

	const [isLoading, setIsLoading] = useState(false);
	const [success, setSuccess] = React.useState(false);

	const inputFileRef = useRef<HTMLInputElement>(null);
	const [blob, setBlob] = useState<PutBlobResult | null>(null);

	const timer = React.useRef<number>();

	const buttonSx = {
		...(success && {
			bgcolor: green[500],
			'&:hover': {
			bgcolor: green[700],
			},
		}),
	};

	const {
		register,
		handleSubmit,
		reset,
		formState: { errors },
	} = useForm<FieldValues>({
		defaultValues: {
			titre: "",
			description: "",
			matiere: "",
			fichier: "",
			dateLimite: "",
		},
	});
	const niveau = ["Primaire", "Secondaire", "Collegial", "Universitaire"];
    const matieres = ["Mathematiques", "Informatique", "Francais", "Anglais", "Philosophie", "Geographie"];

	const onSubmit: SubmitHandler<FieldValues> = async (data) => {
		setIsLoading(true);

		try {
			if (!currentUser) {
				toast.error("Impossible de récupérer l'ID de l'utilisateur.");
				return;
			}

			if (!inputFileRef.current?.files) {
				throw new Error("Pas de fichier sélectionné pour le devoir");
			  }
	
			  const file = inputFileRef.current.files[0];
	
			  const response = await fetch(
				`/api/file/upload?filename=${file.name}`,
				{
				  method: 'POST',
				  body: file,
				},
			  );
	
			  const newBlob = (await response.json()) as PutBlobResult;
	
			  setBlob(newBlob);

			const userId = currentUser.id;

			console.log(data);
			axios
				.post("/api/devoirs/add", { ...data, userId, file: newBlob.downloadUrl })
				.then((response) => {
					if (response.status === 200) {
						toast.success(
							"Merci, Votre devoir a été soumis avec succès."
						);
						reset();
					} else {
						toast.error("Une erreur s'est produite lors de l'ajout du devoir");
					}
				})
				.catch((error) => {
					// La requête a échoué, affichez un message d'erreur
					console.error(error);
					toast.error("Une erreur s'est produite lors de l'ajout du devoir");
				});
		} catch (error) {
			toast.error("Il y'a une erreur: " + error);
		} finally {
			setIsLoading(false);
		}
	};

	return (
		<div
			className="
            sm:mx-auto
            max-h-[70vh]
			overflow-auto
			scrollbar-hide  
        "
		>
			{/* Loading */}
			<div className={isLoading ? "fixed h-full w-full flex items-center justify-center backdrop-blur-sm z-50 p-10 rounded-md" : 'hidden'}>
				<div className=" p-16 rounded-lg drop-shadow-lg backdrop-blur-lg fixed bottom-[calc(50%+100px)]">
					<Box sx={{ m: 1, position: 'relative' }}>
						<Fab
						aria-label="save"
						color="primary"
						sx={{ backgroundColor: 'transparent', ...buttonSx }}
						>
						{success ? <CheckIcon /> : ''}
						</Fab>
						{isLoading && !success && (
						<CircularProgress
							size={68}
							sx={{
							color: green[500],
							position: 'absolute',
							top: -6,
							left: -6,
							zIndex: 1,
							}}
						/>
						)}
					</Box>
				</div>
			</div>

			<div
				className="
                bg-white
                px-4
                py-8
                sm:px-10
            "
			>
				<form className="flex flex-col gap-10" onSubmit={handleSubmit(onSubmit)}>
					<section className="flex flex-col gap-2">
						<div className="mb-2">
							<h3 className="font-thin text-3xl">Informations principales</h3>
							<p className="font-thin text-gray-400">Donner un titre à votre devoir et un début de description.</p>
						</div>

						<Input
							id="titre"
							label="Titre"
							register={register}
							errors={errors}
							disabled={isLoading}
						/>

						<InputSelectTutor
							id="matiere"
							optionPlaceholder="Choissiez une matière"
							label="Matière"
							register={register}
							errors={errors}
							disabled={isLoading}
							options={matieres}
						/>

						<InputSelectTutor 
                            id="niveau" 
                            label="Niveau d'études"
                            options={niveau}
                            optionPlaceholder="choisir entre (primaire, secondaire, collégial, universitaire)"
                            required
                            register={register}
                            errors={errors}
                            disabled={isLoading}/>
					</section>

					<section>
						<div className="mb-4">
							<h3 className="font-thin text-3xl mb-2">Décrivez votre devoir</h3>
							<p className="font-thin text-gray-400">Fournissez toutes les informations pertinentes pour que les tuteurs puissent comprendre clairement vos attentes.</p>
						</div>
						<TextArea
							id="description"
							label="Description"
							register={register}
							errors={errors}
							disabled={isLoading}
						/>
					</section>

					<section>
						<div className="mb-4">
							<h3 className="font-thin text-3xl mb-2">Date de remise</h3>
							<p className="font-thin text-gray-400">Quand est-ce que vous devez remettre ce devoir?</p>
						</div>

						<Input
							id="dateLimite"
							label="Date limite"
							type="date"
							register={register}
							errors={errors}
							disabled={isLoading}
						/>
					</section>

					<section>
						<div className="mb-4">
							<h3 className="font-thin text-3xl mb-2">Coller votre devoir en PDF</h3>
							<p className="font-thin text-gray-400">Joignez votre devoir au format PDF. Assurez-vous qu&#39;il est clairement rédigé et bien présenté. Cela facilitera la compréhension et l&#39;évaluation par les tuteurs.</p>
						</div>

						{/* Input pour lettre de motivation */}
						<label htmlFor="file" 
							className="
							block
							text-sm
							font-medium
							leading-6
							text-gray-900 mb-2
						"
						>
							Fichier du devoir
						</label>
						<input name="file" ref={inputFileRef} type="file" disabled={isLoading} required 
							className="
							form-input
							w-full
							border-0
							py-4
							text-gray-900
							shadow-sm
							rounded-md
							flex
							items-center
							justify-start
							text-start
							ring-1
							ring-inset
							ring-gray-300
							placeholder:text-gray-400
							focus:ring-2
							focus:ring-inset
							focus:ring-purple-600
							sm:text-sm
							sm:leading-6 mb-6
						"
						/>
					</section>



					<div>
						<Button disabled={isLoading} fullWidth type="submit">
							Créer
						</Button>
					</div>
				</form>
			</div>
		</div>
	);
};

export default AddHomeworkForm;
