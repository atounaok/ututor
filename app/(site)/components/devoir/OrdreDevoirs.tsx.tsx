import React, { useState } from "react";
import InputSelectTutor from "@/components/inputs/InputSelectTutor";
import { useForm, FieldValues } from "react-hook-form";

interface OrdreDevoirsProps {
	onChange: (order: { ordre: string }) => void; // Modifier le type de l'argument
}

const OrdreDevoirs: React.FC<OrdreDevoirsProps> = ({ onChange }) => {
	const [isLoading, setIsLoading] = useState(false);
	const [currentOrdre, setCurrentOrder] = useState(false);

	const {
		register,
		handleSubmit,
		formState: { errors },
		getValues,
	} = useForm<FieldValues>();

	const ordres = [
		"De A à Z",
		"De Z à A",
		"Date limite croissante",
		"Date limite décroissante",
		"Par niveau",
		"Par matière",
		"Par utilisateur",
	];

	const handleOrdersChange = () => {
		const { ordre } = getValues();
		if (currentOrdre != ordre) {
			setCurrentOrder(ordre);
			onChange({ ordre });
			console.log("Ordre mis à jour :", getValues());
		}
	};

	const onReset = () => {
		window.location.reload();
	};

	return (
		<div className="flex flex-col gap-4 text-white">
			<form
				onSubmit={handleSubmit(handleOrdersChange)}
				className="flex flex-col gap-4"
			>
				<div className="">
					<InputSelectTutor
						id="ordre"
						register={register}
						label=""
						isGlass
						errors={errors}
						optionPlaceholder="Trier par"
						onClickFun={handleSubmit(handleOrdersChange)}
						options={ordres}
					/>
					{
						//<div className="w-full flex justify-center mt-4 gap-2">
						//<button className="bg-slate-500 hover:bg-slate-300 rounded-md text-white px-8 py-2 hidden">
						//Appliquer
						//</button>
						//	</div>
					}
				</div>
			</form>
		</div>
	);
};

export default OrdreDevoirs;
