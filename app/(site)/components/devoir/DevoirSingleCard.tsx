import { Devoir } from "@prisma/client";
import React from "react";
import Image from "next/image";
import { Meteors } from "@/components/ui/meteors";
import MatièreBadges from "./MatièreBadges";
import StatusColor from "./StatusColor";

interface DevoirSingleCardProps {
  devoir: any;
}

const DevoirSingleCard: React.FC<DevoirSingleCardProps> = ({ devoir }) => {
  function formatDate(dbDate: string) {
    const options: any = { year: "numeric", month: "2-digit", day: "2-digit" };

    const formattedDate = new Date(dbDate).toLocaleDateString("fr-CA", options);
    return formattedDate;
  }
  return (
    <div className="w-full h-full relative max-w-xs" key={devoir.id}>
      <div className="absolute inset-0 h-full w-full bg-gradient-to-r from-violet-600 to-teal-900 transform scale-[0.80] bg-red-500 rounded-full blur-3xl hover:blur-none" />
      <div className="relative shadow-lg hover:hover:shadow-none bg-gray-900 border-none border-gray-800  px-3 py-6 h-full overflow-hidden rounded-2xl flex flex-col justify-between items-start">
        <div className="flex justify-between w-full items-top">
          <div className="flex items-center gap-2">
            <div className="h-8 w-8 rounded-full flex items-center justify-center mb-4">
              <Image
                src={
                  devoir?.user.image
                    ? devoir?.user.image
                    : `https://i.pravatar.cc/150?u=${devoir.user?.id}`
                }
                alt="user logo"
                className="rounded-full w-full h-full"
                width={20}
                height={20}
              />
            </div>
            <p className="text-xs text-white font-thin line-clamp-1">{devoir.user.name}</p>
          </div>

          <StatusColor statut={devoir.statut} />
        </div>

        <div className="w-full h-full flex flex-col justify-between">
          <h1 className="font-bold text-xl text-white mb-4 line-clamp-1">
            {devoir?.titre}
          </h1>

          <p className="font-normal text-base text-slate-500 mb-4 line-clamp-3">
            {devoir?.description}
          </p>

          <div className="flex justify-between w-full items-center">
            <MatièreBadges matiere={devoir.matiere} />
            {/* <button className="border px-4 py-1 rounded-lg  border-gray-500 text-gray-300">
              Explore
            </button> */}

            <p className="text-white font-thin text-sm">
              {formatDate(devoir.dateLimite)}
            </p>
          </div>
        </div>

        {/* Meaty part - Meteor effect */}
        <Meteors number={20} />
      </div>
    </div>
  );
};

export default DevoirSingleCard;
