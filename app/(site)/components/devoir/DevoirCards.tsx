"use client";

import { User } from "@prisma/client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import StatusColor from "./StatusColor";

import {
	Dialog,
	DialogContent,
	DialogDescription,
	DialogHeader,
	DialogTitle,
	DialogTrigger,
} from "@/components/ui/dialog";

import SearchInput from "./SearchInput";
import FiltresDevoirs from "./FiltresDevoirs";
import Link from "next/link";
import OrdreDevoirs from "./OrdreDevoirs.tsx";
import axios from "axios";
import toast from "react-hot-toast";
import DevoirSingleCard from "./DevoirSingleCard";
import { Spotlight } from "@/components/ui/Spotlight";

interface DevoirCardsFormProps {
	currentUser: User;
}

const DevoirCards: React.FC<DevoirCardsFormProps> = ({ currentUser }) => {
	const [devoirs, setDevoirs] = useState<any[]>([]);
	const [searchResults, setSearchResults] = useState<any[]>([]);
	const [isLoading, setIsLoading] = useState<boolean>(false);

	const rephraseStatut = (statut: string) => {
		if (statut === "Traite") return "Traité";
		if (statut === "Expire") return "Expiré";
		if (statut === "A_Traiter") return "À traiter";
		if (statut === "En_Revue") return "En revue";
		if (statut === "En_Traitement") return "En traitement";
		if (statut === "Finie") return "Traitement terminé";
	};

	const [filters, setFilters] = useState<{
		matiere: string;
		niveau: string;
		dateLimite: string;
	}>({
		matiere: "",
		niveau: "",
		dateLimite: "",
	});

	const [order, setOrder] = useState<{ ordre: string }>({ ordre: "" });

	useEffect(() => {
		async () => {
			if (searchResults) {
				setDevoirs(searchResults);
			} else {
				const response = axios.get("/api/devoirs/get/getAll");
				setDevoirs((await response).data);
			}
		};
	}, [searchResults]);

	// Définissez la fonction de mise à jour des résultats de recherche
	const updateSearchResults = (results: any[]) => {
		setSearchResults(results);
	};

	// Fonction pour récupérer les valeurs des filtres depuis le composant FiltresDevoirs
	const handleFiltersChange = (newFilters: {
		matiere: string;
		niveau: string;
		dateLimite: string;
	}) => {
		if (
			filters.dateLimite != newFilters.dateLimite ||
			filters.matiere != newFilters.matiere ||
			filters.niveau != newFilters.niveau
		) {
			setFilters(newFilters);
			console.log(filters != newFilters, newFilters);
			console.log(filters);
		}
	};

	// Fonction pour récupérer l'ordre depuis le composant OrdreDevoirs
	const handleOrderChange = (newOrder: { ordre: string }) => {
		setOrder(newOrder);
	};

	useEffect(() => {
		setIsLoading(true);
		try {
			// Code pour récupérer les devoirs filtrés en fonction des valeurs des filtres
			if (filters.matiere === "choix0" && filters.dateLimite === undefined) {
				async () => {
					axios
						.get("/api/devoirs/get/getAll")
						.then((response) => {
							setDevoirs(response.data);
						})
						.catch((error) => {
							toast.error(error.response.data);
							setIsLoading(false);
						});
				};
			}

			const fetchFilteredOrderedDevoirs = async () => {
				const response = await fetch(
					`/api/devoirs/get/getByFilters?matiere=${filters.matiere}&dateLimite=${filters.dateLimite}&ordre=${order.ordre}`
				);
				const data = await response.json();
				setDevoirs(data);
			};

			fetchFilteredOrderedDevoirs();
		} catch (error) {
			toast.error(
				"Une erreur est survenue lors de la récupération des devoirs filtrés"
			);
		} finally {
			setIsLoading(false);
		}
	}, [filters, order]);

	function formatDate(dbDate: string) {
		const options: any = { year: "numeric", month: "2-digit", day: "2-digit" };

		const formattedDate = new Date(dbDate).toLocaleDateString("fr-CA", options);
		return formattedDate;
	}

	const onReset = () => {
		window.location.reload();
	};

	const deleteDevoir = (devoirId: any) => {
		setIsLoading(true);

		let status = 200;
		try {
			axios
				.post("/api/devoirs/remove/removeById", { devoirId })
				.then((response) => {
					status = response.status;
					setIsLoading(false);
				})
				.catch((error) => {
					// La requête a échoué, affichez un message d'erreur
					console.error(error);
					setIsLoading(false);
				});
		} catch (error) {
			toast.error("il y a une erreur: " + error);
		} finally {
			if (status === 200) {
				onReset();
				toast.success("Votre devoir a été supprimé.");
			} else {
				toast.error(
					"Une erreur s'est produite lors de la suppression du devoir"
				);
			}
		}
	};

	return (
		<div className="flex flex-col lg:w-full h-full">
			<Spotlight
				className="-top-40 left-0 md:left-60 md:-top-20"
				fill="white"
			/>
			<div className="w-full min-h-[10vh] flex justify-center items-center bg-[#141414]">
				<SearchInput
					currentUser={currentUser}
					onUpdateSearchResults={updateSearchResults}
				/>
			</div>

			<div className="flex lg:flex-row sm:flex-col items-start justify-center w-full h-full bg-[#141414]">
				<div className=" sm:w-full lg:h-full sm:flex sm:justify-center lg:block lg:w-3/12  p-4 hidden">
					<div className="flex flex-wrap lg:flex-col sm:mx-1 lg:mx-0 gap-4 glass p-4 rounded-md border">
						<div className="sm:w-full flex justify-between items-center">
							<h3 className="text-md text-white sm:w-full font-bold">
								Filtres
							</h3>
							<button
								className="btn glassmorphism text-white hover:bg-transparent"
								onClick={() => onReset()}
							>
								Réinitialiser
							</button>
						</div>
						<FiltresDevoirs onChange={handleFiltersChange} />
						<OrdreDevoirs onChange={handleOrderChange} />
					</div>
				</div>

				<div className="md:flex-grow max-h-[100%] rounded-xl overflow-y-auto pb-24 lg:w-9/12 ">
					<div className={isLoading ? "" : "hidden"}>
						<span className="loading loading-ball text-purple-400 loading-lg border"></span>
					</div>
					<div
						className={
							!isLoading
								? "grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4 p-4"
								: "hidden"
						}
					>
						{/* Utilisez la liste des devoirs pour générer des composants de carte */}

						{devoirs ? (
							devoirs.map((devoir: any) => (
								<Dialog key={devoir.id}>
									<DialogTrigger className="">
										<DevoirSingleCard devoir={devoir} />
									</DialogTrigger>

									<DialogContent className="card md:max-w-[25vw] max-w-[95vw] rounded-2xl border-none">
										<DialogHeader>
											<DialogTitle className="text-center">
												{devoir.titre}
											</DialogTitle>
											<DialogDescription>
												<div className="flex justify-between mt-8">
													<div className="flex items-center gap-2">
														<h4 className="text-md">Matière: </h4>
														<p className="font-thin">{devoir.matiere}</p>
													</div>
													<div className="flex gap-2 items-center">
														<h4>Date limite : </h4>
														<p className="font-thin">
															{formatDate(devoir.dateLimite)}
														</p>
													</div>
												</div>
												<div className="mt-8">
													<h5 className="">Description:</h5>
													<p className="font-thin my-4">{devoir.description}</p>
												</div>
												<div className="flex items-center justify-between">
													<p className="text-white bg-purple-500 py-1 px-4 rounded-lg">
														{rephraseStatut(devoir.statut)}
													</p>

													<div className="my-2 flex w-1/2 items-center justify-end">
														<Image
															alt="image-user"
															width={35}
															height={20}
															src={
																devoir.user.image
																	? devoir.user.image
																	: `https://i.pravatar.cc/150?u=${devoir.user?.id}`
															}
															className="mx-2 rounded-full"
														/>
														<p className="my-auto">{devoir.user?.name}</p>
													</div>
												</div>
											</DialogDescription>
										</DialogHeader>
										<div className="w-full justify-around flex">
											{currentUser  && devoir.userId !== currentUser.id ? (
												<>
													
													<Link
														href={devoir.fichier}
														className="btn w-5/12 border-none"
														target="_blank"
													>
														Voir
													</Link>
													<Link
														href={{
															pathname: `/devoirs/${devoir.id}`,
															query: {
																devoirTitre: devoir.titre,
															},
														}}
														className={
															currentUser &&
															devoir.userId !== currentUser.id &&
															currentUser.isTutor &&
															(devoir.statut === "A_Traiter" ||
																devoir.statut === "En_Traitement")
																? "bg-[#141414] hover:bg-gray-800 btn text-white w-5/12 border-none"
																: "btn-disabled w-5/12 btn"
														}
													>
														Traiter
													</Link>
												</>
											) : (
												<>
													<Link
														href={devoir.fichier}
														className="btn w-5/12 border-none"
														target="_blank"
													>
														Voir
													</Link>
													<button
														onClick={() => deleteDevoir(devoir.id)}
														disabled={devoir.statut !== "A_Traiter"}
														className={
															"bg-error hover:bg-gray-800 btn text-white w-5/12 border-none"
														}
													>
														{!isLoading ? (
															<>Supprimer</>
														) : (
															<span className="loading loading-spinner text-purple-400 loading-lg border"></span>
														)}
													</button>
												</>
											)}
										</div>
									</DialogContent>
								</Dialog>
							))
						) : (
							// devoirs.map((devoir: any) => (
							//   <Dialog key={devoir.id}>
							//     <DialogTrigger
							//       className="
							// 		card border border-slate-200
							// 		p-4 min-h-[25vh] rounded-xl max-h-[30vh]
							// 		shadow-md hover:shadow-lg max-w-[90vw]
							// 		hover:cursor-pointer transition-all flex flex-col justify-between items-center"
							//     >
							//       <h4 className="text-xl mb-4 truncate hover:text-clip font-light w-full text-center py-2">
							//         {devoir.titre}
							//       </h4>

							//       <div className="w-full flex flex-col gap-1">
							//         <div className="flex items-center justify-between">
							//           <p>Matière: </p>
							//           <h4 className="text-end font-thin">{devoir.matiere}</h4>
							//         </div>
							//         <div className="flex items-center justify-between">
							//           <p>Date limite: </p>
							//           <h4 className="text-end font-thin">
							//             {formatDate(devoir.dateLimite)}
							//           </h4>
							//         </div>
							//         <div className="flex items-center justify-between">
							//           <p>Statut: </p>
							//           <h4 className="text-end font-thin">
							//             {rephraseStatut(devoir.statut)}
							//           </h4>
							//         </div>
							//         <div className="flex justify-between w-[100%] items-center">
							//           <p>Devoir de: </p>
							//           <div className="flex items-center gap-2">
							//             <Image
							//               alt="image-user"
							//               width={20}
							//               height={20}
							//               src={
							//                 devoir.user.image
							//                   ? devoir.user.image
							//                   : "/images/undraw/Male_avatar.jpg"
							//               }
							//               className="mx-2 rounded-full my-auto"
							//             />
							//             <h4 className="my-auto font-thin">
							//               {devoir.user?.name}
							//             </h4>
							//           </div>
							//         </div>
							//       </div>

							//       {/*<div className="flex justify-end w-full">
							// 				<div className="bg-green-400 p-1 text-white rounded-md">
							// 					terminé
							// 				</div>
							// 			</div> */}
							//     </DialogTrigger>

							//     <DialogContent className="md:max-w-[25vw] max-w-[95vw] rounded-md">
							//       <DialogHeader>
							//         <DialogTitle className="text-center">
							//           {devoir.titre}
							//         </DialogTitle>
							//         <DialogDescription>
							//           <div className="flex justify-between mt-8">
							//             <div className="flex items-center gap-2">
							//               <h4 className="text-md">Matière: </h4>
							//               <p className="font-thin">{devoir.matiere}</p>
							//             </div>
							//             <div className="flex gap-2 items-center">
							//               <h4>Date limite : </h4>
							//               <p className="font-thin">
							//                 {formatDate(devoir.dateLimite)}
							//               </p>
							//             </div>
							//           </div>
							//           <div className="mt-8">
							//             <h5 className="">Description:</h5>
							//             <p className="font-thin my-4">{devoir.description}</p>
							//           </div>
							//           <div className="my-2 flex w-full items-center justify-end">
							//             <Image
							//               alt="image-user"
							//               width={35}
							//               height={20}
							//               src={
							//                 devoir.user.image
							//                   ? devoir.user.image
							//                   : "/images/undraw/Male_avatar.jpg"
							//               }
							//               className="mx-2 rounded-full"
							//             />
							//             <p className="my-auto">{devoir.user?.name}</p>
							//           </div>
							//         </DialogDescription>
							//       </DialogHeader>
							//       <div className="w-full justify-around flex">
							//         <Link
							//           href={devoir.fichier}
							//           className="text-center rounded bg-secondary transition-all hover:shadow-lg py-2 w-5/12"
							//           target="_blank"
							//         >
							//           Voir
							//         </Link>
							//         <Link
							//           href={{
							//             pathname: `/devoirs/${devoir.id}`,
							//             query: {
							//               devoirTitre: devoir.titre,
							//             },
							//           }}
							//           className={
							//             currentUser &&
							//             devoir.userId !== currentUser.id &&
							//             currentUser.isTutor &&
							//             devoir.statut !== "Traite"
							//               ? "bg-black py-2 px-4  text-center rounded-md w-5/12 transition-all hover:shadow-lg shadow-black  text-white"
							//               : "disabled border hover:shadow-lg py-2 shadow-black transition-all border-slate-300 rounded-md px-4 w-5/12 text-center"
							//           }
							//         >
							//           Traiter
							//         </Link>
							//       </div>
							//     </DialogContent>
							//   </Dialog>
							// ))
							<div className="flex justify-center items-center w-full h-full">
								<h4 className="text-xl font-thin text-center">
									Aucun devoir pour le moment.
								</h4>
							</div>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

export default DevoirCards;
