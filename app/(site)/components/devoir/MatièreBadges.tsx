import React from 'react'

interface MatièreBadgesProps {
    matiere: any;
}

const MatièreBadges: React.FC<MatièreBadgesProps> = ({ matiere }) => {
    const Matieres =  [
        'MATHEMATIQUES',
        'INFORMATIQUE',
        'FRANCAIS',
        'ANGLAIS',
        'PHILOSOPHIE',
        'GEOGRAPHIE'
    ]

    if(matiere === "MATHEMATIQUES") return (<div className="badge badge-neutral bg-green-800 border-green-800">Math</div>)
    if(matiere === "INFORMATIQUE") return (<div className="badge badge-neutral bg-yellow-800 border-yellow-800">Info</div>)
    if(matiere === "FRANCAIS") return (<div className="badge badge-neutral bg-blue-800 border-blue-800">Français</div>)
    if(matiere === "ANGLAIS") return (<div className="badge badge-neutral bg-red-800 border-red-800">Anglais</div>)
    if(matiere === "PHILOSOPHIE") return (<div className="badge badge-neutral bg-purple-800 border-purple-800">Philo</div>)
    else return (<div className="badge badge-neutral">Geo</div>
  )
}

export default MatièreBadges
