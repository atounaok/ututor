"use client";

import { User } from "@prisma/client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import { useInfiniteQuery } from "@tanstack/react-query";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

import {
	Dialog,
	DialogContent,
	DialogDescription,
	DialogHeader,
	DialogTitle,
	DialogTrigger,
} from "@/components/ui/dialog";

import SearchInput from "./SearchInput";
import FiltresDevoirs from "./FiltresDevoirs";
import Link from "next/link";
import OrdreDevoirs from "./OrdreDevoirs.tsx";
import axios from "axios";
import { error } from "console";
import toast from "react-hot-toast";
import Loading from "@/components/Loading";
import DevoirSingleCard from "./DevoirSingleCard";
import { Spotlight } from "@/components/ui/Spotlight";
import { button } from "@material-tailwind/react";

interface MyDevoirCardsFormProps {
	currentUser: User;
}

const MyDevoirCards: React.FC<MyDevoirCardsFormProps> = ({ currentUser }) => {
	const [devoirs, setDevoirs] = useState<any[]>([]);
	const [searchResults, setSearchResults] = useState<any[]>([]);
	const [isLoading, setIsLoading] = useState<boolean>(false);

	const rephraseStatut = (statut: string) => {
		if (statut === "Traite") return "Traité";
		if (statut === "Expire") return "Expiré";
		if (statut === "A_Traiter") return "À traiter";
		if (statut === "En_Revue") return "En revue";
		if (statut === "En_Traitement") return "En traitement";
	};

	const [filters, setFilters] = useState<{
		matiere: string;
		niveau: string;
		dateLimite: string;
	}>({
		matiere: "",
		niveau: "",
		dateLimite: "",
	});

	const [order, setOrder] = useState<{ ordre: string }>({ ordre: "" });

	useEffect(() => {
		async () => {
			if (searchResults) {
				setDevoirs(searchResults);
			} else {
				const response = axios.get("/api/devoirs/get/getAll");
				setDevoirs((await response).data);
			}
		};
	}, [searchResults]);

	// Définissez la fonction de mise à jour des résultats de recherche
	const updateSearchResults = (results: any[]) => {
		setSearchResults(results);
	};

	// Fonction pour récupérer les valeurs des filtres depuis le composant FiltresDevoirs
	const handleFiltersChange = (newFilters: {
		matiere: string;
		niveau: string;
		dateLimite: string;
	}) => {
		if (
			filters.dateLimite != newFilters.dateLimite ||
			filters.matiere != newFilters.matiere ||
			filters.niveau != newFilters.niveau
		) {
			setFilters(newFilters);
			console.log(filters != newFilters, newFilters);
			console.log(filters);
		}
	};

	// Fonction pour récupérer l'ordre depuis le composant OrdreDevoirs
	const handleOrderChange = (newOrder: { ordre: string }) => {
		setOrder(newOrder);
	};

	const deleteDevoir = (devoirId: any) => {
		setIsLoading(true);

		let status = 200;
		try {
			axios
				.post("/api/devoirs/remove/removeById", { devoirId })
				.then((response) => {
					status = response.status;
					setIsLoading(false);
				})
				.catch((error) => {
					// La requête a échoué, affichez un message d'erreur
					console.error(error);
					setIsLoading(false);
				});
		} catch (error) {
			toast.error("il y a une erreur: " + error);
		} finally {
			if (status === 200) {
				onReset();
				toast.success("Votre devoir a été supprimé.");
			} else {
				toast.error(
					"Une erreur s'est produite lors de la suppression du devoir"
				);
			}
		}
	};

	useEffect(() => {
		setIsLoading(true);
		try {
			// Code pour récupérer les devoirs filtrés en fonction des valeurs des filtres.
			if (filters.matiere === "choix0" && filters.dateLimite === undefined) {
				async () => {
					axios
						.get("/api/devoirs/get/getAll")
						.then((response) => {
							setDevoirs(response.data);
						})
						.catch((error) => {
							toast.error(error.response.data);
							setIsLoading(false);
						});
				};
			}

			const fetchFilteredOrderedDevoirs = async () => {
				//Requête changé pour prendre les devoirs du user.
				const response = await fetch(
					`/api/devoirs/get/getByFilters?matiere=${
						filters.matiere
					}&dateLimite=${filters.dateLimite}&ordre=${order.ordre}&user=${true}`
				);
				const data = await response.json();
				setDevoirs(data);
			};

			fetchFilteredOrderedDevoirs();
		} catch (error) {
			toast.error(
				"Une erreur est survenue lors de la récupération des devoirs filtrés"
			);
		} finally {
			setIsLoading(false);
		}
	}, [filters, order]);

	function formatDate(dbDate: string) {
		const options: any = { year: "numeric", month: "2-digit", day: "2-digit" };

		const formattedDate = new Date(dbDate).toLocaleDateString("fr-CA", options);
		return formattedDate;
	}

	const onReset = () => {
		window.location.reload();
	};

	return (
		<div className="flex flex-col lg:w-full h-full">
			<Spotlight
				className="-top-40 left-0 md:left-60 md:-top-20"
				fill="white"
			/>
			<div className="w-full min-h-[10vh] flex justify-center items-center bg-[#141414]">
				<SearchInput
					currentUser={currentUser}
					onUpdateSearchResults={updateSearchResults}
				/>
			</div>

			<div className="flex lg:flex-row sm:flex-col items-start justify-center w-full h-full bg-[#141414]">
				<div className=" sm:w-full lg:h-full sm:flex sm:justify-center lg:block lg:w-3/12  p-4 hidden">
					<div className="flex flex-wrap lg:flex-col sm:mx-1 lg:mx-0 gap-4 glass p-4 border-gray-200 rounded-md border">
						<div className="sm:w-full flex justify-between items-center">
							<h3 className="text-md text-white sm:w-full font-bold">
								Filtres
							</h3>
							<button
								className="btn glassmorphism text-white hover:bg-transparent"
								onClick={() => onReset()}
							>
								Réinitialiser
							</button>
						</div>
						<FiltresDevoirs onChange={handleFiltersChange} />
						<OrdreDevoirs onChange={handleOrderChange} />
					</div>
				</div>

				<div className="md:flex-grow max-h-[100%] rounded-xl overflow-y-auto pb-24 lg:w-9/12 ">
					<div className={isLoading ? "" : "hidden"}>
						<span className="loading loading-ball text-purple-400 loading-lg border"></span>
					</div>
					<div
						className={
							!isLoading
								? "grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4 p-4"
								: "hidden"
						}
					>
						{/* Utilisez la liste des devoirs pour générer des composants de carte */}
						{devoirs ? (
							devoirs.map((devoir: any) => (
								<Dialog key={devoir.id}>
									<DialogTrigger className="">
										<DevoirSingleCard devoir={devoir} />
									</DialogTrigger>

									<DialogContent className="card md:max-w-[25vw] max-w-[95vw] rounded-2xl border-none">
										<DialogHeader>
											<DialogTitle className="text-center">
												{devoir.titre}
											</DialogTitle>
											<DialogDescription>
												<div className="flex justify-between mt-8">
													<div className="flex items-center gap-2">
														<h4 className="text-md">Matière: </h4>
														<p className="font-thin">{devoir.matiere}</p>
													</div>
													<div className="flex gap-2 items-center">
														<h4>Date limite : </h4>
														<p className="font-thin">
															{formatDate(devoir.dateLimite)}
														</p>
													</div>
												</div>
												<div className="mt-8">
													<h5 className="">Description:</h5>
													<p className="font-thin my-4">{devoir.description}</p>
												</div>
												<div className="my-2 flex w-full items-center justify-end">
													<Image
														alt="image-user"
														width={35}
														height={20}
														src={
															devoir.user.image
																? devoir.user.image
																: "/images/undraw/Male_avatar.jpg"
														}
														className="mx-2 rounded-full"
													/>
													<p className="my-auto">{devoir.user?.name}</p>
												</div>
											</DialogDescription>
										</DialogHeader>
										<div className="w-full justify-around flex">
											<Link
												href={devoir.fichier}
												className="btn w-5/12 border-none"
												target="_blank"
											>
												Voir
											</Link>

											<button
												onClick={() => deleteDevoir(devoir.id)}
												type="button"
												disabled={devoir.statut !== "A_Traiter"}
												className={
													"bg-error hover:bg-gray-800 btn text-white w-5/12 border-none"
												}
											>
												{!isLoading ? (
													<>Supprimer</>
												) : (
													<span className="loading loading-spinner text-purple-400 loading-lg border"></span>
												)}
											</button>
										</div>
									</DialogContent>
								</Dialog>
							))
						) : (
							<div className="flex justify-center items-center w-full h-full">
								<h4 className="text-xl font-thin text-center">
									Aucun devoir pour le moment.
								</h4>
							</div>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

export default MyDevoirCards;
