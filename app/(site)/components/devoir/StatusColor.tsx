import React from 'react'

interface StatusColorProps {
    statut: any;
}

const StatusColor: React.FC<StatusColorProps> = ({ statut }) => {

    if(statut === "Traite" || statut === "Finie") return (<div className="text-green-500">Traité</div>)
    if(statut === "Expire") return (<div className="text-red-500">Expiré</div>)
    if(statut === "A_Traiter") return (<div className="text-white">À traiter</div>)
    if(statut === "En_Traitement") return (<div className="text-orange-500">En traitement</div>)
    else return (<div className="text-purple-500">En revue</div>
  )
}

export default StatusColor
