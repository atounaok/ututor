'use client'

import { Conversation, User } from '@prisma/client';
import axios from 'axios';
import Link from 'next/link';
import React, { useEffect, useState, useRef as useRefReact } from 'react';

import Image from 'next/image';
import toast from 'react-hot-toast';
import { useRouter } from 'next/navigation';
import { useSession } from 'next-auth/react';
import {
	Dialog,
	DialogContent,
	DialogDescription,
	DialogHeader,
	DialogTitle,
	DialogTrigger,
} from "@/components/ui/dialog";
import { useForm, FieldValues, SubmitHandler } from 'react-hook-form';
import { PutBlobResult } from '@vercel/blob';
import MessageInput from '../../conversations/[conversationId]/components/MessageInput';
import { error } from 'console';


interface InfosCandidaturesFormProps {
    currentUser: User;
    devoirId: string;
}

const TraitementDevoir: React.FC<InfosCandidaturesFormProps> = ({
    currentUser,
    devoirId
}) => {
    const [Loading, setLoading] = useState(false);
    const [devoir, setDevoir] = useState<any>([]);
    const [devoirUser, setDevoirUser] = useState<User | null>(null);
    const [conversationId, setConversationId] = useState<string | null>(null);
    const inputFileRef = useRefReact<HTMLInputElement>(null);
	const [blob, setBlob] = useState<PutBlobResult | null>(null);

    const router = useRouter();

    const {
		register,
		handleSubmit,
		reset,
		formState: { errors },
	} = useForm<FieldValues>({
		defaultValues: {
            message: '',
		},
	});

    const rephraseStatut = (statut: string) => {
		if (statut === "Traite") return "Traité";
		if (statut === "Expire") return "Expiré";
		if (statut === "A_Traiter") return "À traiter";
		if (statut === "En_Revue") return "En revue";
		if (statut === "En_Traitement") return "En traitement";
	};


    function formatDate(dbDate: string) {
		const options: any = { year: "numeric", month: "2-digit", day: "2-digit" };
		//TODO: Changer le locale pour celui du navigateur
		const formattedDate = new Date(dbDate).toLocaleDateString("fr-CA", options);
		return formattedDate;
	}


    const onSubmitCorrection: SubmitHandler<FieldValues> = async (data) => {
		setLoading(true);

		try {
			if (!currentUser) {
				toast.error("Impossible de récupérer l'ID de l'utilisateur.");
				return;
			}

            if(!data.message) {
                data.message = `Bonjour ${devoirUser?.name}, j'ai fini la correction de votre devoir. J'espère que cela vous aidera à mieux comprendre le sujet. N'hésitez pas à me contacter pour plus d'explications.`;
            }

			if (!inputFileRef.current?.files) {
				throw new Error("Pas de fichier sélectionné pour le devoir");
			  }
	
              // Enregistrer le fichier dans le stockage
			  const file = inputFileRef.current.files[0];
	
			  const response = await fetch(
				`/api/file/upload?filename=${file.name}`,
				{
				  method: 'POST',
				  body: file,
				},
			  );
	
			  const newBlob = (await response.json()) as PutBlobResult;
	
			  setBlob(newBlob);

			const userId = currentUser.id;

            // Enregistrer la correction dans la base de données
			axios
				.put("/api/devoirsCorrige/add", { ...data, userId, file: newBlob.downloadUrl, devoirId: devoir.id })
				.then((response) => {
					if (response.status === 200) {
						toast.success(
							`Votre correction a été soumise.`
						);
					} else {
						toast.error("Une erreur s'est produite lors de la remise");
					}
				})
				.catch((error) => {
					// La requête a échoué, affichez un message d'erreur
					toast.error(error.response.data);
				});
            
            // Enregistrer un message pour informer l'élève de la correction
            // 1: On crée une nouvelle conversation

            axios.post(`/api/conversations`, {
                ...data, devoirId: devoir?.id, file: newBlob.downloadUrl
            })
            .then((response) => {
                const conversation = response.data;
                console.log(conversation.id);
                envoyerMessage(conversation.id);
                setConversationId(conversation.id);
            })
            .catch((error) => {
                toast.error(error.response.data);
            })

            // 2: On envoie un message pour informer l'élève de la correction
            const envoyerMessage = (id: string) => {
                axios.post(`/api/messages`, {
                    ...data, file: newBlob.downloadUrl, conversationId: id
                })
                .then((response) => {
                    router.push('/conversations/' + id);
                })
                .catch((error) => {
                    toast.error(error.response.data);
                });
            }
		} catch (error) {
			toast.error("Il y'a une erreur: " + error);
		} finally {
			setLoading(false);
		}
	};

    const handleCommencerTraitement = async () => {
        try {
            setLoading(true);
            // Appelez votre API pour commencer le traitement
            await axios.put('/api/devoirs/put/changerStatut', {
                devoirId: devoirId,
                tuteurId: currentUser.id,
                statut: "En_Traitement",
            })
            .then((response) => {
                setDevoir(response.data);
                toast.success('Traitement commencé avec succès');
            })
            .catch((error) => {
                toast.error(error.response.data);
            })

        } catch (error) {
            console.error('Erreur lors du commencement du traitement', error);
            // Affichez un message d'erreur
            toast.error('Erreur lors du commencement du traitement');
        } finally {
            setLoading(false);
        }
    }

    const handleAnnulerTraitement = async () => {
        try {
            setLoading(true);
            // Appelez votre API pour commencer le traitement
            const response = await axios.put('/api/devoirs/put/annulerTraitement', {
                devoirId: devoirId,
                tuteurId: currentUser.id,
                statut: "A_Traiter",
            });

            if(response.status === 200) {
                setDevoir(response.data);

                // Affichez un message de succès
                toast.success('Traitement annulé avec succès');
                window.location.reload();
            }
        } catch (error) {
            console.error('Erreur lors du commencement du traitement', error);
            // Affichez un message d'erreur
            toast.error('Erreur lors du commencement du traitement');
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        // Déclarez une fonction asynchrone pour récupérer le devoirs
        setLoading(true);
        const fetchDevoir = async () => {
            try {
                // Appelez votre API pour récupérer tous les devoirs
                axios.get('/api/devoirs/get/getById', {
                    params: {
                        devoirId: devoirId,
                    },
                })
                .then((response) => {
                    setDevoir(response.data);
                    setDevoirUser(response.data.user);
                })
                .catch((error) => {
                    toast.error(error.response.data);
                    router.push('/devoirs');
                })

            } catch (error) {
                console.error('Erreur lors de la récupération des devoirs', error);
            }
            finally{
                setLoading(false);
            }
        };

        // Appelez la fonction pour récupérer les devoirs au moment du rendu initial
        fetchDevoir();
    }, [devoirId, router]);


  return (
    <div className='py-4 min-h-[80vh] w-full flex flex-col justify-between'>
        <div className="h-2/6 flex flex-col gap-4 p-4 rounded-lg">
            <h2 className='text-2xl font-thin'>Informations du devoir</h2>
            <div className="flex flex-col gap-4">
                <div className="flex justify-between border-b py-1">
                    <p>Titre</p>
                    <p className='font-thin'>{devoir?.titre}</p>
                </div>
                <div className="flex justify-between border-b py-1">
                    <p>Matière</p>
                    <p className='font-thin'>{devoir?.matiere}</p>
                </div>
                <div className="flex justify-between border-b py-1">
                    <p>Date limite</p>
                    <p className='font-thin'>{formatDate(devoir?.dateLimite)}</p>
                </div>
                <div className="flex justify-between border-b py-1">
                    <p>Statut</p>
                    <p className='font-thin'>{rephraseStatut(devoir.statut)}</p>
                </div>
            </div>
        </div>
        <div className="flex flex-col justify-center gap-4 h-3/6 p-4">
        <h2 className='text-2xl font-thin'>Détails du devoir</h2>
            <div className="flex flex-col gap-4">
                <div className="flex justify-between border-b py-1">
                    <p>Nom de l&#39;élève</p>
                    <p className='font-thin'>{devoirUser?.name}</p>
                </div>
                <div className="flex flex-col gap-4 justify-between border-b py-1">
                    <p>Description</p>
                    <textarea className='font-thin' value={devoir?.description} disabled/>
                </div>
            </div>
            <div className="flex justify-center mt-4 gap-4">
                <Link href={devoir?.fichier ? devoir?.fichier: ''} target='_blank'
                className="
                    border btn-link
                    flex py-4 px-4
                    items-center 
                    justify-center 
                    gap-2 rounded-xl 
                    bg-cover shadow-sm hover:shadow-lg
                " 
                >
                    <Image
                        alt="Logo"
                        height="48"
                        width="48"
                        className="mx-auto w-auto"
                        src="/images/utils/cv.png"/>
                    <p>PDF du devoir</p>
                </Link>
            </div>
        </div>
        <div className="h-1/6">
            {
                devoir?.isTreated ? 
                    <p className='text-green-500 font-thin text-center'>Ce devoir a été traité!</p> : 
                    (
                    <div className="">
                        {
                            !devoir?.isTreated && devoir?.statut === 'A_Traiter' && devoir?.userId !== currentUser.id ? (
                                <div className="">
                                    <div className="flex gap-4 justify-center">
                                        <Link href={'/devoirs'} className="w-1/2 btn lg:w-[10vw] border-none">Annuler</Link>

                                        <Dialog>
                                            <DialogTrigger className='w-1/2 btn border-none bg-green-500 hover:bg-green-400 text-white lg:w-[10vw] text-center'>
                                                Traiter le devoir
                                            </DialogTrigger>

                                            <DialogContent className={Loading ? 'flex items-center justify-center bg-white' : 'hidden'}>
                                                <span className="loading loading-ring loading-lg text-purple-500"></span>
                                            </DialogContent>

                                            <DialogContent className={Loading ? 'hidden' : ''}>
                                                <div className="flex flex-col justify-center items-center">
                                                    <p className='text-center'>Voulez-vous vraiment traiter ce devoir?</p>
                                                    <p className='font-thin text-sm text-center my-4 max-w-[75%] text-wrap text-red-400'>*Le statut du devoir passera en traitement.</p>
                                                </div>
                                                <div className="flex gap-4 justify-center">
                                                    <button className="btn bg-green-500 border-none hover:bg-green-400 text-white" onClick={() => {handleCommencerTraitement()}}>Commencer le traitement</button>
                                                </div>
                                            </DialogContent>
                                        </Dialog>
                                    </div>
                                </div>
                            ) : (
                                <div className="flex gap-4 justify-center">
                                    <button className="btn bg-red-500 hover:bg-red-400 text-white w-1/2 lg:w-[10vw] border-none" onClick={() => {handleAnnulerTraitement()}}>Annuler le traitement</button>
                                    <Dialog key={devoir.id}>
                                        <DialogTrigger className={!devoir?.isTreated && devoir?.statut === 'En_Traitement' ? "btn w-1/2 lg:w-[10vw] border-none bg-green-500 hover:bg-green-400 text-white" : 'hidden'}>
                                            Envoyer la correction
                                        </DialogTrigger>

                                        <DialogContent className={Loading ? 'flex items-center justify-center bg-white' : 'hidden'}>
                                            <span className="loading loading-ball loading-lg text-purple-500"></span>
                                        </DialogContent>

                                        <DialogContent className={Loading ? 'hidden' : ''}>
                                            <div className="flex flex-col justify-center items-center">
                                                <p className='text-center text-2xl'>Voulez-vous vraiment envoyer la correction?</p>
                                                <p className='font-thin text-sm text-center my-4 max-w-[75%] text-wrap'>Une discussion va démarrer avec {devoirUser?.name} pour que vous lui expliquiez votre correction.</p>
                                            </div>
                                            <div className="flex gap-4 justify-center">
                                                <form onSubmit={handleSubmit(onSubmitCorrection)}>
                                                    <section>
                                                        <div className="mb-4">
                                                            <h3 className="font-thin text-xl mb-2">Coller votre correction en PDF</h3>
                                                            <p className="font-thin text-sm text-gray-400">Joignez votre correction au format PDF. Assurez-vous qu&#39;il est clairement rédigé et bien présenté. Cela facilitera la compréhension et l&#39;évaluation par les tuteurs.</p>
                                                        </div>

                                                        {/* Message de correction */}
                                                        <div className="mb-6">
                                                            <label htmlFor="message" 
                                                                className="
                                                                block text-center
                                                                text-sm lg:text-start
                                                                font-medium
                                                                leading-6
                                                                text-gray-900 mb-2">
                                                                Voici le message que {devoirUser?.name} verra <br/> (<span className='text-sm font-thin'>Vous pouvez le modifier maintenant</span>)
                                                            </label>

                                                            <MessageInput 
                                                                id="message"
                                                                register={register}
                                                                errors={errors}
                                                                isTextArea
                                                                disabled={true}
                                                                placeholder={`Bonjour ${devoirUser?.name}, j'ai fini la correction de votre devoir. J'espère que cela vous aidera à mieux comprendre le sujet. N'hésitez pas à me contacter pour plus d'explications.`}
                                                            />
                                                        </div>

                                                        {/* Input pour lettre de motivation */}
                                                        <label htmlFor="file" 
                                                            className="
                                                            block
                                                            text-sm
                                                            font-medium
                                                            leading-6
                                                            text-gray-900 mb-2
                                                        "
                                                        >
                                                            Fichier du devoir corrigé
                                                        </label>
                                                        <input id='file' name="file" ref={inputFileRef} type="file" disabled={Loading} required 
                                                            className="
                                                            file-input file-input-ghost w-full mt-2 file-input-bordered mb-4
                                                        "
                                                        />
                                                    </section>
                                                    <div className="flex justify-center">
                                                        <button type='submit' className="btn w-full bg-green-500 hover:bg-green-400 text-white border-none" >Envoyer la correction</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </DialogContent>
                                    </Dialog>
                                </div>
                            )
                        }
                    </div>
                )
            }
        </div>
    </div>
  )
}

export default TraitementDevoir
function useRef<T>(arg0: null) {
    throw new Error('Function not implemented.');
}

