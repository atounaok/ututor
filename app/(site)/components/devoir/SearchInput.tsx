"use client";

import { User } from "@prisma/client";
import React, { useEffect, useState } from "react";
import Image from "next/image";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import Link from "next/link";

interface SearchFormProps {
  currentUser: User;
  onUpdateSearchResults: any;
}

const SearchInput: React.FC<SearchFormProps> = ({
  currentUser,
  onUpdateSearchResults,
}) => {
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [isInputFocused, setIsInputFocused] = useState(false);
  const [isMouseOver, setIsMouseOver] = useState(true);

  // Utilisez useEffect pour surveiller les changements dans searchResults et appeler onUpdateSearchResults
  useEffect(() => {
    onUpdateSearchResults(searchResults);
  }, [searchResults, onUpdateSearchResults]);

  function formatDate(dbDate: string) {
    const options: any = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
    };
    //TODO: Changer le locale pour celui du navigateur
    const formattedDate = new Date(dbDate).toLocaleDateString("fr-CA", options);
    return formattedDate;
  }

  const handleSearchChange = async (event: any) => {
    const newSearchTerm = event.target.value;
    setSearchQuery(newSearchTerm);

    // Effectuer une requête à votre backend pour récupérer les résultats de recherche
    try {
      const response = await fetch(
        `/api/devoirs/get/getBySearch?search=${newSearchTerm}`
      );
      const data = await response.json();
      setSearchResults(data);
      console.log(newSearchTerm);
      console.log(searchResults);
    } catch (error) {
      console.error("Erreur lors de la recherche:", error);
    }
  };

  const handleInputFocus = () => {
    setIsInputFocused(true);
  };

  const handleInputBlur = () => {
    setIsInputFocused(false);
  };

  const handleMouseOver = () => {
    setIsMouseOver(true);
  };

  const handleMouseLeave = () => {
    setIsMouseOver(false);
  };

  return (
    <div className="md:w-[20%] w-[80%]">
      <label
        htmlFor="rechercher"
        className="input input-bordered flex items-center gap-2 glassmorphism text-white"
      >
        <input
          id="rechercher"
          type="search"
          placeholder="Rechercher"
          className="grow"
          value={searchQuery}
          onChange={handleSearchChange}
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
        />
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 16 16"
          fill="currentColor"
          className="w-4 h-4 opacity-70"
        >
          <path
            fillRule="evenodd"
            d="M9.965 11.026a5 5 0 1 1 1.06-1.06l2.755 2.754a.75.75 0 1 1-1.06 1.06l-2.755-2.754ZM10.5 7a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Z"
            clipRule="evenodd"
          />
        </svg>
      </label>
      {/* <button onClick={handleSearch}>Rechercher</button> */}
      <div
        onMouseOver={handleMouseOver}
        onMouseLeave={handleMouseLeave}
        className={
          (isMouseOver || isInputFocused) &&
          searchResults.length > 0 &&
          searchQuery.trim() != ""
            ? "min-h-[20vh] absolute md:max-w-[20%] w-[80%] rounded-md mt-[.2rem] text-white glassmorphism z-10 border max-h-[50vh] overflow-y-auto"
            : "hidden"
        }
      >
        <ul className="">
          {searchResults.map((devoir: any) => (
            <li key={devoir.id} className="relative z-50">
              <Dialog>
                <DialogTrigger className="w-full py-2 hover:glass text-start px-4 flex justify-between items-center">
                  <p className="truncate max-w-[50%]">{devoir.titre}</p>
                  <div className="flex items-center gap-1">
                    <h4 className="my-auto font-thin text-sm truncate">
                      {devoir.user?.name}
                    </h4>
                    <Image
                      alt="image-user"
                      width={15}
                      height={15}
                      src={
                        devoir.user.image
                          ? devoir.user.image
                          : `https://i.pravatar.cc/150?u=${devoir.user?.id}`
                      }
                      className="mx-2 rounded-full my-auto"
                    />
                  </div>
                </DialogTrigger>

                <DialogContent className="md:max-w-[25vw] max-w-[95vw] rounded-xl">
                  <DialogHeader>
                    <DialogTitle className="text-center">
                      {devoir.titre}
                    </DialogTitle>
                    <DialogDescription>
                      <div className="flex justify-between mt-8">
                        <div className="flex items-center gap-2">
                          <h4 className="text-md">Matière: </h4>
                          <p className="font-thin">{devoir.matiere}</p>
                        </div>
                        <div className="flex gap-2 items-center">
                          {/*TODO : Afficher le mois en lettres */}
                          <h4>Date limite : </h4>
                          <p className="font-thin">
                            {formatDate(devoir.dateLimite)}
                          </p>
                        </div>
                      </div>
                      <div className="mt-8">
                        <h5 className="">Description:</h5>
                        <p className="font-thin my-4">{devoir.description}</p>
                      </div>
                      <div className="my-2 flex w-full items-center justify-end">
                        <Image
                          alt="image-user"
                          width={35}
                          height={20}
                          src={
                            devoir.user.image
                              ? devoir.user.image
                              : `https://i.pravatar.cc/150?u=${devoir.user?.id}`
                          }
                          className="mx-2 rounded-full"
                        />
                        <p className="my-auto">{devoir.user?.name}</p>
                      </div>
                    </DialogDescription>
                  </DialogHeader>
                  <div className="w-full justify-around flex">
                    <Link
                      href={devoir.fichier}
                      className="btn w-5/12 border-none"
                      target="_blank"
                    >
                      Voir
                    </Link>
                    <Link
                      href={{
                        pathname: `/devoirs/${devoir.id}`,
                        query: {
                          devoirTitre: devoir.titre,
                        },
                      }}
                      className={
                        currentUser &&
                        devoir.userId !== currentUser.id &&
                        currentUser.isTutor &&
                        devoir.statut !== "Traite"
                          ? "bg-[#141414] hover:bg-gray-800 btn text-white w-5/12 border-none"
                          : "btn-disabled w-5/12 btn"
                      }
                    >
                      Traiter
                    </Link>
                  </div>
                </DialogContent>
              </Dialog>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default SearchInput;
