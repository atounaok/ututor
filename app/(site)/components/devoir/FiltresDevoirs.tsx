import React, { useState } from "react";
import InputSelectTutor from "@/components/inputs/InputSelectTutor";
import { useForm, FieldValues } from "react-hook-form";
import { User } from "@prisma/client";

interface FiltresDevoirsProps {
	onChange: (filters: {
		matiere: string;
		niveau: string;
		dateLimite: string;
		user: User;
	}) => void;
}

const FiltresDevoirs: React.FC<FiltresDevoirsProps> = ({ onChange }) => {
	const [isLoading, setIsLoading] = useState(false);

	const {
		register,
		handleSubmit,
		formState: { errors },
		getValues,
	} = useForm<FieldValues>();

	const niveau = ["Primaire", "Secondaire", "Collegial", "Universitaire"];
	
	const matieres = [
		"Mathematiques",
		"Informatique",
		"Francais",
		"Anglais",
		"Philosophie",
		"Geographie",
	];

	const handleFiltersChange = () => {
		const { matiere, niveau, dateLimite, user } = getValues();
		onChange({ matiere, niveau, dateLimite, user });
	};

	const onReset = () => {
		window.location.reload();
	};
	return (
		<div className="flex lg:flex-col sm:mx-1 lg:mx-0 gap-4 flex-wrap text-white">
			<form
				onSubmit={handleSubmit(handleFiltersChange)}
				className="flex lg:flex-col gap-4  w-full"
			>
				<div className="my-auto">
					<InputSelectTutor
						id="matiere"
						label="Matière"
						isGlass
						options={matieres}
						optionPlaceholder="Choisir une matière"
						required
						register={register}
						onClickFun={handleSubmit(handleFiltersChange)}
						errors={errors}
						disabled={isLoading}
					/>
				</div>
				<div className="my-auto">
					<h4 className="text-sm mb-2 sm:hidden">Date limite</h4>
					<input
						aria-label="date"
						type="date"
						id="dateLimite"
						onFocus={handleSubmit(handleFiltersChange)}
						{...register("dateLimite")}
						className="w-full border border-slate-300 rounded-md p-2 glassmorphism hover:bg-transparent"
					/>
				</div>
				{
					//<div className="w-full flex justify-center mt-4 gap-2">
					//<button
					//	hidden
					//	className="hover:border rounded-md px-8 py-2"
					//	type="reset"
					//	onClick={() => onReset()}
					//>
					//</div>	R
					//</button>
					//
					//<button className="bg-slate-500 hover:bg-slate-300 rounded-md text-white px-8 py-2">
					//Appliquer
					//</button>
					//</div>
				}
			</form>
		</div>
	);
};

export default FiltresDevoirs;
