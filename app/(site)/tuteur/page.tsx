import ToApplyForm from '@/app/(site)/components/candidatures/ToApplyForm'
import getCurrentUser from '@/app/actions/getCurrentUser'

const page = async () => {
  const currentUser = await getCurrentUser();

  return (
    <main className="min-h-[calc(100vh-64px)] flex items-center justify-center">
      <ToApplyForm currentUser={currentUser!}/>
    </main>
  )
}

export default page
