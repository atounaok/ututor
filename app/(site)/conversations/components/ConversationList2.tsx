"use client";

import Image from "next/image";
import { Tabs } from "@/components/ui/tabs";
import { FullConversationType } from "@/app/types";
import { User } from "@prisma/client";
import useConversation from "@/app/hooks/useConversation";
import { pusherClient } from "@/app/libs/pusher";
import { find } from "lodash";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useState, useMemo, useEffect } from "react";
import ConversationBox from "./ConversationBox";
import clsx from "clsx";

interface ConversationListProps {
  initialItems: FullConversationType[];
  users: User[];
  title?: string;
}

export const ConversationList2: React.FC<ConversationListProps> = ({
  initialItems,
  users,
}) => {
  const [items, setItems] = useState(initialItems);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const router = useRouter();
  const session = useSession();

  const { conversationId, isOpen } = useConversation();

  const pusherKey = useMemo(() => {
    return session.data?.user?.email;
  }, [session.data?.user?.email]);

  useEffect(() => {
    if (!pusherKey) {
      return;
    }

    pusherClient.subscribe(pusherKey);

    const updateHandler = (conversation: FullConversationType) => {
      setItems((current) =>
        current.map((currentConversation) => {
          if (currentConversation.id === conversation.id) {
            return {
              ...currentConversation,
              messages: conversation.messages,
            };
          }

          return currentConversation;
        })
      );
    };

    const newHandler = (conversation: FullConversationType) => {
      setItems((current) => {
        if (find(current, { id: conversation.id })) {
          return current;
        }

        return [conversation, ...current];
      });
    };

    const removeHandler = (conversation: FullConversationType) => {
      setItems((current) => {
        return [...current.filter((convo) => convo.id !== conversation.id)];
      });
    };

    pusherClient.bind("conversation:update", updateHandler);
    pusherClient.bind("conversation:new", newHandler);
    pusherClient.bind("conversation:remove", removeHandler);
  }, [pusherKey, router]);

  const tabs = [
    {
      title: "Conversations",
      value: "activeConversations",
      content: (
        <div className="w-full overflow-hidden relative h-full card border-t shadow-sm shadow-purple-300 rounded-2xl p-4 text-xl md:text-2xl font-bold text-white bg-white">
          <p className="mb-4 text-black">Conversations</p>
          <div className="overflow-y-auto">
            {items.map((item) =>
                item.isActive ? (
                <ConversationBox
                    key={item.id}
                    data={item}
                    selected={conversationId === item.id}
                />
                ) : (
                ""
                )
            )}
          </div>
        </div>
      ),
    },
    {
      title: "Historique",
      value: "nonActiveConversations",
      content: (
        <div className="w-full overflow-hidden relative h-full card border-t shadow-sm shadow-purple-300 rounded-2xl p-4 text-xl md:text-2xl font-bold text-white bg-white">
          <p className="mb-4 text-black">Historique</p>
          <div className="overflow-y-auto">
            {items.map((item) =>
                !item.isActive ? (
                <ConversationBox
                    key={item.id}
                    data={item}
                    selected={conversationId === item.id}
                />
                ) : (
                ""
                )
            )}
          </div>
        </div>
      ),
    },
  ];

  return (
    <aside
      className={clsx(
        `
    fixed h-[calc(100vh-64px)] top-[64px]
    inset-y-0 
    pb-20 overflow-hidden
    lg:pb-0
    lg:left-20 
    lg:w-80 
    lg:block
    border-r flex flex-col justify-between
    border-gray-200
  `,
        isOpen ? "hidden" : "block w-full left-0"
      )}
    >
      <Tabs tabs={tabs} tabClassName="overflow-y-auto" />
    </aside>
  );
};
