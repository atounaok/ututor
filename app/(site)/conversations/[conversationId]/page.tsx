import  getConversationById  from '@/app/actions/getConversationById';
import React, { useEffect } from 'react'
import EmptyState from '../../components/messagerie/EmptyState';
import getMessages from '@/app/actions/getMessages';
import Header from './components/Header';
import Body from './components/Body';
import Form from './components/Form';
import getCurrentUser from '@/app/actions/getCurrentUser';


import { useState } from 'react';
import { emit } from 'process';
import toast from 'react-hot-toast';

interface IParams {
    conversationId: string;
}

const conversationId = async ({ params }: {params: IParams}) => {
    const currentUser = await getCurrentUser();
    const conversation = await getConversationById(params?.conversationId);
    const messages = await getMessages(params?.conversationId);

    if (!conversation) {
        return (
            <div className="lg:pl-80 h-[calc(100vh-64px)] top-[64px]">
                <div className="h-full flex flex-col">
                    <EmptyState/>
                </div>
            </div>
        )
    }


    return (
        <div className="lg:pl-80 h-[calc(100vh-64px)] top-[64px]">
            <div className="h-full flex flex-col ">
                <Header conversation={conversation}/>
                <Body initialMessages={messages!} currentUserId={currentUser?.id!} conversation={conversation!}/>
            </div>
        </div>
    )
}

export default conversationId

