"use client";

import axios from "axios";

import { use, useEffect, useRef, useState } from "react";

import { pusherClient } from "@/app/libs/pusher";
import useConversation from "@/app/hooks/useConversation";
import MessageBox from "./MessageBox";
import { FullMessageType } from "@/app/types";

import { Conversation } from "@prisma/client";
import { HiPaperAirplane, HiPhoto } from "react-icons/hi2";
import MessageInput from "./MessageInput";
import { useForm, FieldValues, SubmitHandler } from "react-hook-form";
import { CldUploadButton } from "next-cloudinary";
import toast from "react-hot-toast";
import { find } from "lodash";
import SatisfiedButton from "./SatisfiedButton";
import Link from "next/link";
import Image from "next/image";

interface BodyProps {
  initialMessages: FullMessageType[];
  currentUserId: string;
  conversation: Conversation;
}

const Body: React.FC<BodyProps> = ({
  initialMessages = [],
  currentUserId,
  conversation,
}) => {
  const bottomRef = useRef<HTMLDivElement>(null);
  const [messages, setMessages] = useState(initialMessages);
  const [isLoading, setIsLoading] = useState(false);
  const [currentMessage, setCurrentMessage] = useState("");
  const [tutorId, setTutorId] = useState<string | null>(null);

  const { conversationId } = useConversation();

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      message: "",
    },
  });

  useEffect(() => {
    bottomRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  // Envoi d'un message
  const sendMessage: SubmitHandler<FieldValues> = async (data) => {
    setIsLoading(true);
    try {
      setValue("message", "", { shouldValidate: true });
      const response = await axios.post(`/api/messages`, {
        ...data,
        conversationId,
      });

      const newMessage = response.data;
      console.log(newMessage);
      setMessages([...messages, newMessage]);

      setIsLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  // Upload une image
  const handleUpload = (result: any) => {
    axios.post(`/api/messages`, {
      conversationId,
      image: result?.info?.secure_url,
    });
  };

  useEffect(() => {
    axios.post(`/api/conversations/${conversationId}/seen`);
  }, [conversationId]);

  useEffect(() => {
    axios
      .post(`/api/conversations/getById/`, {
        conversationId: conversationId,
      })
      .then((response) => {
        axios
          .get(
            `/api/devoirTuteurRelation/get/getById?devoirId=${response.data.devoirId}`
          )
          .then((response) => {
            setTutorId(response.data.tutorId);
            console.log(response.data.tutorId);
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  }, [conversationId]);

  //Code pusher
  useEffect(() => {
    pusherClient.subscribe(conversationId!);
    bottomRef?.current?.scrollIntoView();

    const messageHandler = (message: FullMessageType) => {
      axios.post(`/api/conversations/${conversationId}/seen`);

      setMessages((current) => {
        if (find(current, { id: message?.id })) {
          return current;
        }

        return [...current, message];
      });

      bottomRef?.current?.scrollIntoView();
    };

    const updateMessageHandler = (newMessage: FullMessageType) => {
      setMessages((current) =>
        current.map((currentMessage) => {
          if (currentMessage?.id === newMessage?.id) {
            return newMessage;
          }

          return currentMessage;
        })
      );
    };

    pusherClient.bind("messages:new", messageHandler);
    pusherClient.bind("message:update", updateMessageHandler);

    return () => {
      pusherClient.unsubscribe(conversationId!);
      pusherClient.unbind("messages:new", messageHandler);
      pusherClient.unbind("message:update", updateMessageHandler);
    };
  }, [conversationId]);

  return (
    <>
      <div
        className={
          !isLoading
            ? "flex-1 justify-between flex-col flex overflow-hidden"
            : "flex-1 justify-between flex-col flex overflow-hidden"
        }
        ref={bottomRef}
      >
        <div className="overflow-y-auto">
          {messages.map((message, i) => (
            <MessageBox
              isLast={i === messages.length - 1}
              key={message?.id}
              data={message}
            />
          ))}
          <div ref={bottomRef} />
        </div>

        <div className=" flex flex-col gap-4">
          <div className="flex justify-between px-4 bg-transparent">
            <Link
              href={conversation.devoirCorrige}
              className="bg-purple-200 hover:bg-purple-500 rounded-md p-1 flex items-center justify-center"
            >
              <Image
                src={"/images/utils/cv.png"}
                width={32}
                height={32}
                alt={"Lien de la correction"}
              />
            </Link>

            {conversation.tutorId !== currentUserId && conversation.isActive ? (
              <SatisfiedButton conversationId={conversationId!} />
            ) : (
              ""
            )}
          </div>

          {/* Form de message */}
          {!conversation.isActive ? (
            <div className="flex justify-center items-center h-16">
              <p className="text-sm text-gray-400">
                Cette conversation est terminée!
              </p>
            </div>
          ) : (
            <div
              className="
                  relative
                  bottom-0
                  py-4
                  px-4
                  bg-white
                  border-t
                  flex
                  items-center
                  gap-2
                  lg:gap-4
                  w-full
              "
            >
              <CldUploadButton
                options={{ maxFiles: 1 }}
                onUpload={handleUpload}
                uploadPreset="wyi8jpnd"
              >
                <HiPhoto size={32} className="text-purple-500" />
              </CldUploadButton>

              <form
                onSubmit={handleSubmit(sendMessage)}
                className="flex items-center gap-2 lg:gap-4 w-full"
              >
                <MessageInput
                  id="message"
                  register={register}
                  errors={errors}
                  disabled={isLoading}
                  required
                  placeholder="Ecrire un message"
                />
                <button
                  disabled={isLoading}
                  type="submit"
                  className="
                      rounded-full
                      p-2
                      bg-purple-500 
                      hover:bg-purple-600
                      cursor-pointer
                      transition
                  "
                >
                  <HiPaperAirplane size={18} className="text-white" />
                </button>
              </form>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Body;
