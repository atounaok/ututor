'use client'

import { CldUploadButton } from 'next-cloudinary'
import useConversation from "@/app/hooks/useConversation"
import axios from "axios";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import { HiPaperAirplane, HiPhoto } from "react-icons/hi2";
import MessageInput from "./MessageInput";

const Form = () => {
    const { conversationId } = useConversation();


    const { 
        register, 
        handleSubmit,
        setValue, 
        formState: { errors }
     } = useForm<FieldValues>({
        defaultValues: {
            message: ''
        }
    });

    // Upload une image
    const handleUpload = (result: any) => {
        axios.post(`/api/messages`, {
            conversationId,
            image: result?.info?.secure_url
        })
    }

    // Envoi d'un message
    const onSubmit: SubmitHandler<FieldValues> = async (data) => {
        console.log('ConversatioId: ' + conversationId);
        setValue('message', '', { shouldValidate: true });
        axios.post(`/api/messages`, {
            ...data,
            conversationId
        })

    }
  return (
    <div className="
        py-4
        px-4
        bg-white
        border-t
        flex
        items-center
        gap-2
        lg:gap-4
        w-full
    ">
        <CldUploadButton
            options={{maxFiles: 1}}
            onUpload={handleUpload}
            uploadPreset='wyi8jpnd'
        >
            <HiPhoto size={32} className="text-purple-500" />
        </CldUploadButton>
        <form onSubmit={handleSubmit(onSubmit)} className="flex items-center gap-2 lg:gap-4 w-full">
            <MessageInput 
                id="message"
                register={register}
                errors={errors}
                required
                placeholder="Ecrire un message"
            />
            <button type="submit" className="
                rounded-full
                p-2
                bg-purple-500 
                hover:bg-purple-600
                cursor-pointer
                transition
            ">
                <HiPaperAirplane size={18} className="text-white"/>
            </button>
        </form>
    </div>
  )
}

export default Form
