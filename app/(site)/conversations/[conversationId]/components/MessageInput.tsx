'use client'

import clsx from "clsx";
import { FieldErrors, FieldValues, UseFormRegister } from "react-hook-form";

interface MessageInputProps {
    placeholder?: string;
    id: string;
    type?: string;
    required?: boolean;
    isTextArea?: boolean;
    register: UseFormRegister<FieldValues>;
    errors: FieldErrors;
    defaultValue?: string;
    disabled?: boolean;
}

const MessageInput: React.FC<MessageInputProps> = ({
    placeholder,
    id,
    type,
    required,
    isTextArea,
    register,
    errors,
    defaultValue,
    disabled
}) => {
  return (
    <div className="relative w-full">
      {
        isTextArea ? (
          <textarea
            id={id}
            autoComplete={id}
            { ...register(id, { required }) }
            placeholder={ placeholder }
            defaultValue={defaultValue} // Utilisez defaultValue ici pour les zones de texte
            className={clsx(`
              text-black
              font-light
              py-2
              px-4
              bg-neutral-100
              w-full h-32
              rounded-2xl
              focus:outline-none
            `)}
          />
        ) : (
          <input 
          id={id}
          type={type}
          autoComplete={id}
          { ...register(id, { required }) }
          defaultValue={defaultValue}
          disabled={disabled}
          placeholder="Ecrire un message"
          className={clsx(`
            text-black
            font-light
            py-2
            px-4
            bg-neutral-100
            w-full
            rounded-full
            focus:outline-none
          `)}
          />
        )
      }
    </div>
  )
}

export default MessageInput
