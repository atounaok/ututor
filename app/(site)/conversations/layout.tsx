import getConversations from "@/app/actions/getConversation";
import getUsers from "@/app/actions/getUsers";
import Sidebar from "../components/messagerie/Sidebar";
import { ConversationList2 } from "./components/ConversationList2";

export default async function ConversationsLayout({
  children
}: {
  children: React.ReactNode,
}) {
  const conversations = await getConversations();
  const users = await getUsers();

  return (
    <Sidebar>
      <div className="h-full">
        <ConversationList2 
          users={users} 
          title="Messages" 
          initialItems={conversations}
        />
        {children}
      </div>
    </Sidebar>
  );
}