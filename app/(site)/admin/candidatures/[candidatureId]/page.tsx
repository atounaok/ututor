import getCurrentUser from '@/app/actions/getCurrentUser';
import Link from 'next/link';
import Image from 'next/image';
import React, { useState } from 'react'
import axios from 'axios';
import toast from 'react-hot-toast';
import { useParams } from 'next/navigation';
import TraitementCandidature from '@/app/(site)/components/candidatures/TraitementCandidature';

const page = async ({ params, searchParams }: {
  params: {
    candidatureId: string
  },
  searchParams: {
    candidat: string
  }

}) => {
  const currentUser = await getCurrentUser();
  console.log(searchParams.candidat);
  

  return (
    <div className='container min-h-[90vh]'>
      <div className="flex items-center justify-between mb-2">
        <h2 className='text-4xl font-thin'>Candidature de {searchParams.candidat}</h2>
        <Link href="/admin/candidatures" className="btn md:max-w-[20%] max-w-[35%]">
            <Image src={'/images/utils/undo.png'} alt='logout' width={20} height={20}/>
            Précédent
        </Link>
      </div>
      <div className="flex flex-col gap-6 w-full min-h-[100%] justify-around items-center border-t">
        <TraitementCandidature currentUser={currentUser!} candidatureId={params.candidatureId}/>
      </div>
  </div>
  )
}

export default page
