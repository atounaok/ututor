import Link from 'next/link'
import Image from 'next/image'
import React from 'react'
import getCurrentUser from '@/app/actions/getCurrentUser';
import NouvellesCandidatures from '../../components/candidatures/NouvellesCandidatures';
import AnciennesCandidatures from '../../components/candidatures/AnciennesCandidatures';

const page = async () => {
  const currentUser = await getCurrentUser();

  return (
    <div className='container'>
      <div className="flex items-center justify-between mb-2">
        <h2 className='text-4xl font-semibold'>Toutes les candidatures</h2>
        <Link href="/admin" className="btn flex items-center justify-center">
            <Image src={'/images/utils/undo.png'} alt='logout' width={20} height={20}/>
            Précédent
        </Link>
      </div>
      <div className="flex flex-col gap-6 w-full justify-around items-center border-t">
        <div className="w-full flex flex-col gap-4 mt-2">
          <h2 className='text-2xl font-thin'>Nouvelles candidatures</h2>
          <NouvellesCandidatures currentUser={currentUser!}/>
        </div>
        <div className="w-full flex flex-col gap-4">
          <h2 className='text-2xl font-thin'>Anciennes candidatures</h2>
          <AnciennesCandidatures currentUser={currentUser!}/>
        </div>
      </div>
    </div>
  )
}

export default page
