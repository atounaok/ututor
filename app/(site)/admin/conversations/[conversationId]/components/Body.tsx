"use client";

import axios from "axios";

import { use, useEffect, useRef, useState } from "react";

import { pusherClient } from "@/app/libs/pusher";
import useConversation from "@/app/hooks/useConversation";
import MessageBox from "./MessageBox";
import { FullMessageType } from "@/app/types";

import { Conversation } from "@prisma/client";
import { HiPaperAirplane, HiPhoto } from "react-icons/hi2";
import MessageInput from "./MessageInput";
import { useForm, FieldValues, SubmitHandler } from "react-hook-form";
import { CldUploadButton } from "next-cloudinary";
import toast from "react-hot-toast";
import { find } from "lodash";
import Accept from "./Accept";
import Refuse from "./Refuse";
import Link from "next/link";
import Image from "next/image";

interface BodyProps {
  initialMessages: FullMessageType[];
  currentUserId: string;
  conversation: Conversation;
}

const Body: React.FC<BodyProps> = ({
  initialMessages = [],
  currentUserId,
  conversation,
}) => {
  const bottomRef = useRef<HTMLDivElement>(null);
  const [messages, setMessages] = useState(initialMessages);
  const [isLoading, setIsLoading] = useState(false);
  const [tutorId, setTutorId] = useState<string | null>(null);

  const { conversationId } = useConversation();

  useEffect(() => {
    axios
      .post(`/api/conversations/getById/`, {
        conversationId: conversationId,
      })
      .then((response) => {
        axios
          .get(
            `/api/devoirTuteurRelation/get/getById?devoirId=${response.data.devoirId}`
          )
          .then((response) => {
            setTutorId(response.data.tutorId);
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  }, [conversationId]);

  return (
    <>
      <div
        className={
          !isLoading
            ? "flex-1 justify-between flex-col flex overflow-hidden"
            : "flex-1 justify-between flex-col flex overflow-hidden"
        }
        ref={bottomRef}
      >
        <div className="overflow-y-auto">
          {messages.map((message, i) => (
            <MessageBox
              isLast={i === messages.length - 1}
              key={message?.id}
              data={message}
            />
          ))}
          <div ref={bottomRef} />
        </div>

        <div className=" flex flex-col gap-4">
          <div className="flex justify-between px-4 relative bottom-12 bg-transparent">
            <Link
              href={conversation.devoirCorrige}
              className="bg-purple-200 hover:bg-purple-500 hover:shadow-xl rounded-md p-1 flex items-center justify-center"
            >
              <Image
                src={"/images/utils/cv.png"}
                width={32}
                height={32}
                alt={"Lien de la correction"}
              />
            </Link>

            {
              conversation.statut === "A_Examiner_Admin" ? (
                <div className="flex gap-4">
                  <Accept conversationId={conversation.id} />
                  <Refuse conversationId={conversation.id} />
                </div>
              ) : (
                <p className="text-center text-green-500">Conversation traitée avec succès!</p>
              )
            }
          </div>
        </div>
      </div>
    </>
  );
};

export default Body;
