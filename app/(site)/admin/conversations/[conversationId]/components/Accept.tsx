"use client";

import Image from "next/image";
import {
  Drawer,
  DrawerClose,
  DrawerContent,
  DrawerDescription,
  DrawerFooter,
  DrawerHeader,
  DrawerTitle,
  DrawerTrigger,
} from "@/components/ui/drawer";
import { usePathname } from "next/navigation";
import { User } from "@prisma/client";
import AddHomeworkForm from "@/app/(site)/components/devoir/AddHomeworkForm";
import clsx from "clsx";
import toast from "react-hot-toast";
import axios from "axios";
import { useRouter } from "next/navigation";
import { useState } from "react";

interface SatisfiedButtonProps {
  conversationId: string;
}

const Accept: React.FC<SatisfiedButtonProps> = ({
  conversationId,
}) => {
  const [isLoading, setIsLoading] = useState(false);

  const router = useRouter();

  // Lorsque satisfait on termine la conversation
  const handleSatisfied = async () => {
    setIsLoading(true);

    try {
      await axios
        .put(`/api/conversations/accept`, {
          conversationId: conversationId,
        })
        .then((response) => {
          toast.success("Récompense envoyée!");
          router.push("/admin/conversations");
          window.location.reload()
        })
        .catch((error) => {
          toast.error(error.response.data);
        });
    } catch (error) {
      toast.error("Une erreur est survenue, veuillez réessayer.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
    {
      isLoading ? (
        <span className="loading loading-ball loading-lg text-purple-500"></span>
      ) : (
        <Drawer>
          <DrawerTrigger
            className={clsx(`
                btn btn-bordered border flex items-center transition-all
                py-2 px-4 gap-2
                rounded-md shadow-md
                glassmorphism
                text-purple-500 
                hover:bg-purple-500
                hover:text-white
            `)}
          >
            <p className="hidden md:block">Autoriser</p>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-5 h-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6.633 10.25c.806 0 1.533-.446 2.031-1.08a9.041 9.041 0 0 1 2.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 0 0 .322-1.672V2.75a.75.75 0 0 1 .75-.75 2.25 2.25 0 0 1 2.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282m0 0h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 0 1-2.649 7.521c-.388.482-.987.729-1.605.729H13.48c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 0 0-1.423-.23H5.904m10.598-9.75H14.25M5.904 18.5c.083.205.173.405.27.602.197.4-.078.898-.523.898h-.908c-.889 0-1.713-.518-1.972-1.368a12 12 0 0 1-.521-3.507c0-1.553.295-3.036.831-4.398C3.387 9.953 4.167 9.5 5 9.5h1.053c.472 0 .745.556.5.96a8.958 8.958 0 0 0-1.302 4.665c0 1.194.232 2.333.654 3.375Z"
              />
            </svg>
          </DrawerTrigger>
    
          <DrawerContent className="">
            <DrawerHeader className="">
              <DrawerTitle className="md:text-5xl text-2xl text-center mb-2">
                Récompenser le tuteur
              </DrawerTitle>
              <DrawerDescription className="text-center md:text-xl text-sm">
                Êtes-vous certain de vouloir octroyer une récompense? <br />{" "}
                <span className="text-xs md:text-sm text-red-500">
                  Le tuteur sera rémunéré et la conversation sera terminée!
                </span>
              </DrawerDescription>
            </DrawerHeader>
            <div className="w-full rounded-md"></div>
            <DrawerFooter className="border-t flex flex-row justify-center w-full items-center">
              <DrawerClose className="btn border hover:bg-gray-100 text-center py-2 px-12 rounded-md  sm:max-w-[35%]">
                Annuler
              </DrawerClose>
              <button
                className="btn bg-white hover:bg-purple-500 hover:text-white text-purple-500 text-center py-2 px-6 rounded-md  sm:max-w-[35%]"
                onClick={() => handleSatisfied()}
              >
                Autoriser
              </button>
            </DrawerFooter>
          </DrawerContent>
        </Drawer>

      )
    }
    </>
  );
};

export default Accept;
