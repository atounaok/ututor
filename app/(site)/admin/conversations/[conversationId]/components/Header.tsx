'use client';

import { HiChevronLeft } from 'react-icons/hi'
import { HiEllipsisHorizontal } from 'react-icons/hi2';
import { useMemo, useState } from "react";
import Link from "next/link";
import { Conversation, User } from "@prisma/client";
import Image from "next/image";

import useOtherUser from "@/app/hooks/useOtherUser";
import useActiveList from "@/app/hooks/useActiveList";

import Avatar from '@/app/(site)/components/messagerie/Avatar';
import ProfileDrawer from "./ProfileDrawer";
import AvatarGroup from '@/components/AvatarGroup';

interface HeaderProps {
  conversation: Conversation & {
    users: User[],
  }
}

const Header: React.FC<HeaderProps> = ({ conversation }) => {
  const otherUser = useOtherUser(conversation);
  const [drawerOpen, setDrawerOpen] = useState(false);

  const { members } = useActiveList();
  const isActive = members.indexOf(otherUser?.email!) !== -1;
  
  const statusText = useMemo(() => {
    return isActive ? 'En ligne' : 'Hors ligne'
  }, [isActive]);

  return (
  <>
    <ProfileDrawer 
      data={conversation} 
      isOpen={drawerOpen} 
      onClose={() => setDrawerOpen(false)}
    />
    <div 
      className="
        bg-white 
        w-full 
        flex 
        border-b-[1px] 
        sm:px-4 
        py-3 
        px-4 
        lg:px-6 
        justify-between 
        items-center 
        shadow-sm
      "
    >
      <div className="flex gap-3 items-center">
        <Link
          href="/admin/conversations" 
          className="
            lg:hidden 
            block 
            text-purple-500 
            hover:text-purple-600 
            transition 
            cursor-pointer
          "
        >
          <HiChevronLeft size={32} />
        </Link>
        <AvatarGroup users={conversation.users} />
        <div className="flex flex-col line-clamp-1">
          <div>{conversation.users[0].name} & {conversation.users[1].name}</div>
        </div>
      </div>

      {/* <HiEllipsisHorizontal
        size={32}
        onClick={() => setDrawerOpen(true)}
        className="
          text-purple-500
          cursor-pointer
          hover:text-purple-600
          transition
        "
      /> */}
    </div>
    </>
  );
}
 
export default Header;