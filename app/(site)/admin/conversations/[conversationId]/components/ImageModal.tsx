'use client';

import Modal from "@/components/modals/Modal";
import Image from "next/image";

interface ImageModalProps {
    src: string;
    onClose: () => void;
    isOpen?: boolean;
}

const ImageModal: React.FC<ImageModalProps> = ({
    src,
    onClose,
    isOpen
}) => {
    if(!src) return null;

  return (
    <Modal isOpen={isOpen} onClose={onClose} >
        <div className="w-80 h-80">
            <Image alt="Image" className="object-cover" fill src={src}/>
        </div>
    </Modal>
  )
}

export default ImageModal
