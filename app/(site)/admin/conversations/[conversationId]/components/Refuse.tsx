"use client";

import Image from "next/image";
import {
  Drawer,
  DrawerClose,
  DrawerContent,
  DrawerDescription,
  DrawerFooter,
  DrawerHeader,
  DrawerTitle,
  DrawerTrigger,
} from "@/components/ui/drawer";
import { usePathname } from "next/navigation";
import { User } from "@prisma/client";
import AddHomeworkForm from "@/app/(site)/components/devoir/AddHomeworkForm";
import clsx from "clsx";
import toast from "react-hot-toast";
import axios from "axios";
import { useRouter } from "next/navigation";
import { useState } from "react";
import Loading from "@/components/Loading";

interface SatisfiedButtonProps {
  conversationId: string;
}

const Refuse: React.FC<SatisfiedButtonProps> = ({ conversationId }) => {
  const [isLoading, setIsLoading] = useState(false);

  const router = useRouter();

  // Lorsque refusé on termine la conversation
  const handleDenial = async () => {
    setIsLoading(true);

    try {
      await axios
        .put(`/api/conversations/refuse`, {
          conversationId: conversationId,
        })
        .then((response) => {
          toast.success("Récompense refusée!");
          router.push("/admin/conversations");
          window.location.reload()
        })
        .catch((error) => {
          toast.error(error.response.data);
        });
    } catch (error) {
      toast.error("Une erreur est survenue, veuillez réessayer.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      {isLoading ? (
        <span className="loading loading-ball loading-lg text-purple-500"></span>
      ) : (
        <Drawer>
          <DrawerTrigger
            className={clsx(`
                btn btn-bordered border flex items-center transition-all
                py-2 px-4 gap-2
                rounded-md shadow-md
                glassmorphism
                text-red-500 
                hover:bg-red-500
                hover:text-white
            `)}
          >
            <p className="hidden md:block">Refuser</p>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M7.498 15.25H4.372c-1.026 0-1.945-.694-2.054-1.715a12.137 12.137 0 0 1-.068-1.285c0-2.848.992-5.464 2.649-7.521C5.287 4.247 5.886 4 6.504 4h4.016a4.5 4.5 0 0 1 1.423.23l3.114 1.04a4.5 4.5 0 0 0 1.423.23h1.294M7.498 15.25c.618 0 .991.724.725 1.282A7.471 7.471 0 0 0 7.5 19.75 2.25 2.25 0 0 0 9.75 22a.75.75 0 0 0 .75-.75v-.633c0-.573.11-1.14.322-1.672.304-.76.93-1.33 1.653-1.715a9.04 9.04 0 0 0 2.86-2.4c.498-.634 1.226-1.08 2.032-1.08h.384m-10.253 1.5H9.7m8.075-9.75c.01.05.027.1.05.148.593 1.2.925 2.55.925 3.977 0 1.487-.36 2.89-.999 4.125m.023-8.25c-.076-.365.183-.75.575-.75h.908c.889 0 1.713.518 1.972 1.368.339 1.11.521 2.287.521 3.507 0 1.553-.295 3.036-.831 4.398-.306.774-1.086 1.227-1.918 1.227h-1.053c-.472 0-.745-.556-.5-.96a8.95 8.95 0 0 0 .303-.54"
              />
            </svg>
          </DrawerTrigger>

          <DrawerContent className="">
            <DrawerHeader className="">
              <DrawerTitle className="md:text-5xl text-2xl text-center mb-2">
                Refuser la récompense
              </DrawerTitle>
              <DrawerDescription className="text-center md:text-xl text-sm">
                Êtes-vous certain de vouloir refuser de lui octroyer une
                récompense? <br />{" "}
                <span className="text-xs md:text-sm text-red-500">
                  La conversation sera terminée et le tuteur ne recevra pas de
                  récompense!
                </span>
              </DrawerDescription>
            </DrawerHeader>
            <div className="w-full rounded-md"></div>
            <DrawerFooter className="border-t flex flex-row justify-center w-full items-center">
              <DrawerClose className="btn border hover:bg-gray-100 text-center py-2 px-12 rounded-md  sm:max-w-[35%]">
                Annuler
              </DrawerClose>
              <button
                className="btn bg-white hover:bg-red-500 hover:text-white text-red-500 text-center py-2 px-6 rounded-md  sm:max-w-[35%]"
                onClick={() => handleDenial()}
              >
                Refuser
              </button>
            </DrawerFooter>
          </DrawerContent>
        </Drawer>
      )}
    </>
  );
};

export default Refuse;
