import Link from "next/link";
import Image from "next/image";
import React from "react";
import { Spotlight } from "@/components/ui/Spotlight";

const page = () => {
  return (
    <div className="bg-[#141414] h-[calc(100vh-64px)]">
        <Spotlight
          className="-top-40 left-0 md:left-60 md:-top-20"
          fill="white"
        />
      <div className="container flex flex-col">
        <h2 className="sm:text-5xl text-3xl font-semibold text-center my-10 py-8 text-white">
          Que voulez-vous faire?
        </h2>

        <div className="flex w-full justify-around items-center flex-col md:flex-row gap-6">
          <Link
            href="/admin/candidatures"
            className="
                 
                flex 
                flex-col 
                items-center 
                justify-center 
                gap-2 rounded-xl 
                bg-cover shadow-lg
              text-white bg-center
                md:h-[60vh] md:w-[30%] sm:w-[60%] w-[80%]"
            style={{ backgroundImage: `url(/images/unsplash/tutor-2.jpg)` }}
          >
            <div className="z-10 md:bg-black md:opacity-35 hover:opacity-60 h-full w-full rounded-xl relative flex items-center justify-center">
              <div className="text-2xl font-thin py-6">
                Voir les candidatures
              </div>
            </div>
          </Link>
          
          <Link
            href="/admin/conversations"
            className="
                 
                flex 
                flex-col 
                items-center 
                justify-center 
                gap-2 rounded-xl 
                bg-cover shadow-lg
              text-white bg-center
              md:h-[60vh] md:w-[30%] sm:w-[60%] w-[80%]"
            style={{ backgroundImage: `url(/images/unsplash/tutor-1.jpg)` }}
          >
            <div className="z-10 md:bg-black md:opacity-35 hover:opacity-60 h-full w-full rounded-xl relative flex items-center justify-center">
              <div className="text-2xl font-thin py-6">
                Voir les devoirs traités
              </div>
            </div>
          </Link>

          <Link
            href="/admin/"
            className="
                 
                flex 
                flex-col 
                items-center 
                justify-center 
                gap-2 rounded-xl 
                bg-cover shadow-lg
              text-white bg-center
              md:h-[60vh] md:w-[30%] sm:w-[60%] w-[80%]"
            style={{ backgroundImage: `url(/images/unsplash/tutor-3.jpg)` }}
          >
            <div className="z-10 md:bg-black md:opacity-35 hover:opacity-60 h-full w-full rounded-xl relative flex items-center justify-center">
              <div className="text-2xl font-thin  py-6">...</div>
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default page;
