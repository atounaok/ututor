import getCurrentUser from '@/app/actions/getCurrentUser';
import React, { useEffect, useState } from 'react';
import ConvesTerminees from '../../components/devoirs-traites/ConvesTerminees';

const page = async () => {
  const currentUser =  await getCurrentUser();
  return(
    <div className="">
        <ConvesTerminees currentUser={currentUser!} />
    </div>
  )
};

export default page;
