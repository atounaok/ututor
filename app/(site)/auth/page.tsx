"use client"

import AuthForm from '../components/auth/AuthForm';
import Image from 'next/image';

const page = () => {

  return (
    <div className='
      flex
      h-[calc(100vh-64px)]
      justify-center
      lg:py-12
      sm:px-6
      lg:px-8
    ' >

    <div className="flex justify-center items-center w-[75%] gap-4">
      <Image className='lg:block hidden lg:w-2/3' src={'/images/undraw/multitasking.svg'} width={500} height={50} alt={'image'}/>

      <div className="lg:w-1/3 h-full px-2 w-full flex flex-col justify-center items-center">
        <div className="sm:mx-auto sm:w-full sm:max-w-md">
          <Image
            alt="Logo"
            height="48"
            width="48"
            className="mx-auto w-auto"
            src="/images/logo.png"/>

          <h2 className="mt-6 text-center text-2xl sm:text-3xl font-bold tracking-tight text-gray-900">
            Commencez dès à présent
          </h2>
        </div>
        <AuthForm/> 
      </div>
    </div>
    </div>
  )
}

export default page
