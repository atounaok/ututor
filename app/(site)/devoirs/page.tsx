import React from "react";
import DevoirCards from "../components/devoir/DevoirCards";
import getCurrentUser from "@/app/actions/getCurrentUser";

const page = async () => {
	const currentUser = await getCurrentUser();

	return (
		<main className="h-[calc(100vh-64px)] overflow-hidden">
			<DevoirCards currentUser={currentUser!} />
		</main>
	);
};

export default page;
