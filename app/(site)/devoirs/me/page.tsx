import React from "react";
import DevoirCards from "../../components/devoir/DevoirCards";
import getCurrentUser from "@/app/actions/getCurrentUser";
import { Spotlight } from "@/components/ui/Spotlight";
import MyDevoirCards from "../../components/devoir/MyDevoirCards";

const page = async () => {
	const currentUser = await getCurrentUser();

	return (
		<main className="h-[calc(100vh-64px)] overflow-hidden">
			<MyDevoirCards currentUser={currentUser!} />
		</main>
	);
};

export default page;
