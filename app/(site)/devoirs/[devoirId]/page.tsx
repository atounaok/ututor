import getCurrentUser from "@/app/actions/getCurrentUser";
import Link from "next/link";
import Image from "next/image";
import React, { useState } from "react";
import axios from "axios";
import toast from "react-hot-toast";
import { useParams } from "next/navigation";
import TraitementCandidature from "@/app/(site)/components/candidatures/TraitementCandidature";
import { Devoir } from "@prisma/client";
import TraitementDevoir from "../../components/devoir/TraitementDevoir";

const page = async ({
  params,
  searchParams,
}: {
  params: {
    devoirId: string;
  };
  searchParams: {
    devoirTitre: string;
  };
}) => {
  const currentUser = await getCurrentUser();

  return (
    <div className="container min-h-[90vh]">
      <div className="flex items-center justify-between mb-2">
        <h2 className="text-2xl lg:text-4xl font-thin truncate">
          {searchParams.devoirTitre}
        </h2>
        <Link
          href="/devoirs"
          className="btn flex xl:max-w-[20%] border-none md:max-w-[20%] max-w-[35%]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-5 h-5"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M9 15 3 9m0 0 6-6M3 9h12a6 6 0 0 1 0 12h-3"
            />
          </svg>

          <p>devoirs</p>
        </Link>
      </div>
      <div className="flex flex-col gap-6 w-full min-h-[100%] justify-around items-center border-t">
        <TraitementDevoir
          currentUser={currentUser!}
          devoirId={params.devoirId}
        />
      </div>
    </div>
  );
};

export default page;
