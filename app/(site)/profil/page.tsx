import getCurrentUser from "@/app/actions/getCurrentUser";
import React from "react";
import Image from "next/image";
import UpdateProfile from "../components/profil/UpdateProfile";
import { Spotlight } from "@/components/ui/Spotlight";

const page = async () => {
  const currentUser = await getCurrentUser();
  return (
    <main className="min-h-[calc(100vh-64px)] flex justify-center w-full items-center py-2 overflow-auto bg-[#141414]">
      <Spotlight
        className="-top-40 left-0 md:left-60 md:-top-20"
        fill="white"
      />
      <UpdateProfile currentUser={currentUser!} />
    </main>
  );
};

export default page;
