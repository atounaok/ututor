"use client";

import Image from "next/image";
import {
  Drawer,
  DrawerClose,
  DrawerContent,
  DrawerDescription,
  DrawerFooter,
  DrawerHeader,
  DrawerTitle,
  DrawerTrigger,
} from "@/components/ui/drawer";
import { usePathname } from "next/navigation";
import { User } from "@prisma/client";
import AddHomeworkForm from "@/app/(site)/components/devoir/AddHomeworkForm";
import clsx from "clsx";
import { Button } from "./ui/moving-border";

interface AddHomeWorkTriggerProps {
  currentUser: User;
}

const AddHomeWorkTrigger: React.FC<AddHomeWorkTriggerProps> = ({
  currentUser,
}) => {
  const pathname = usePathname();
  const isDrawerLeft = pathname?.startsWith("/devoirs");

  const isDrawerHidden =
    pathname?.startsWith("/tuteur") ||
    pathname?.startsWith("/admin") ||
    pathname?.startsWith("/devoirs/") ||
    pathname === "/candidature/[id]" ||
    pathname?.startsWith("/messagerie") ||
    pathname?.startsWith("/profil") ||
    pathname?.startsWith("/conversation");

  return (
    <div>
      {isDrawerHidden ? (
        ""
      ) : (
        <Drawer>
          <DrawerTrigger
            className={clsx(
              `
                  hover:scale-110 border-none text-white
                  fixed bottom-2 
                  
                `,
              !currentUser ? "hidden" : "",
              isDrawerLeft ? "left-4" : "right-4"
            )}
          >
            <span className="btn border-none shadow-[0_4px_14px_0_rgb(0,118,255,39%)] hover:shadow-[0_6px_20px_rgba(0,118,255,23%)] hover:bg-purple-500/75 px-8 py-2 bg-purple-500 rounded-md text-white font-light transition duration-200 ease-linear">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6"
              >
                
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M12 4.5v15m7.5-7.5h-15"
                />
              </svg>
              <p className="hidden md:block">Ajouter un devoir</p>
            </span>
          </DrawerTrigger>

          <DrawerContent className="max-h-[calc(100vh-64px)] relative z-50">
            <DrawerHeader className="">
              <DrawerTitle className="lg:text-5xl text-3xl text-center mb-2">
                Ajouter un devoir
              </DrawerTitle>
              <DrawerDescription className="text-center lg:text-xl text-md">
                Soyez organisé et efficace avec l&#39;ajout de devoirs.
              </DrawerDescription>
            </DrawerHeader>
            <div className="w-full rounded-md">
              <AddHomeworkForm currentUser={currentUser!} />
            </div>
            <DrawerFooter className="border-t flex justify-center w-full items-center">
              <DrawerClose className="btn bg-red-500 hover:bg-red-400 text-white btn-ghost w-full sm:max-w-[30%]">
                Annuler
              </DrawerClose>
            </DrawerFooter>
          </DrawerContent>
        </Drawer>
      )}
    </div>
  );
};

export default AddHomeWorkTrigger;
