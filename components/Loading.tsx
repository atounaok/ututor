import React from 'react'

interface LoadingProps {
    isLoading?: Boolean;
}

const Loading: React.FC<LoadingProps> = ({
    isLoading
}) => {
  return (
    <div className='flex items-center justify-center w-screen h-[calc(100vh-64px)]'>
        {
            isLoading ? <span className="loading loading-ball text-purple-400 loading-lg"></span> : ('')
        }
    </div>
  )
}

export default Loading