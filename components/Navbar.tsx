"use client";

import { User } from "@prisma/client";
import { signOut } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import React, { useState, useEffect } from "react";

import MobileNav from "./MobileNav";
import ProfileNav from "./ProfileNav";
import ThemeChanger from "./ThemeChanger";
import UIProfileNav from "./UIProfileNav";

interface NavbarProps {
  currentUser: User;
}

const Navbar: React.FC<NavbarProps> = ({ currentUser }) => {
  const [show, setShow] = useState(false);
  const [showMenu, setShowMenu] = useState(false);

  return (
    <nav className="navbar max-h-[49px]">
      <div className="flex-1">
        <Link href={"/"} className="btn btn-ghost text-lg">
          <Image
            alt="Logo"
            height="48"
            width="48"
            className=""
            src="/images/logo.png"
          />
          <h2 className="">UTutor.</h2>
        </Link>
      </div>

      {/* Navbar desktop */}
      <div className="flex-none">
        <MobileNav currentUser={currentUser} />

        {/* <ThemeChanger /> */}
        <ul className="md:menu md:menu-horizontal md:items-center md:px-1 hidden gap-2">
          <li>
            <Link className="" href="/">
              <Image
                src={"/images/utils/home.png"}
                alt="logout"
                width={20}
                height={20}
              />
              Accueil
            </Link>
          </li>
          {currentUser ? (
            <li>
              <Link className="" href="/conversations">
                <Image
                  src={"/images/utils/chat.png"}
                  alt="logout"
                  width={20}
                  height={20}
                />
                Messagerie
              </Link>
            </li>
          ) : (
            ""
          )}
          <li>
            <Link className="" href="/devoirs">
              <Image
                src={"/images/utils/book.png"}
                alt="logout"
                width={20}
                height={20}
              />
              Devoirs
            </Link>
          </li>
          {currentUser && currentUser.isAdmin ? (
            <li>
              <Link className="" href="/admin">
                <Image
                  src={"/images/utils/setting.png"}
                  alt="logout"
                  width={20}
                  height={20}
                />
                Panel admin
              </Link>
            </li>
          ) : (
            ""
          )}
          {currentUser && currentUser.hasApplied === false ? (
            <li>
              <Link className="" href="/tuteur">
                <Image
                  src={"/images/utils/training.png"}
                  alt="logout"
                  width={20}
                  height={20}
                />
                Devenir tuteur
              </Link>
            </li>
          ) : (
            ""
          )}
          {!currentUser ? (
            ""
          ) : (
            //Profile dropdown
            <div className="">
              <div>
                <UIProfileNav currentUser={currentUser} />
              </div>
            </div>
          )}
          <li className="">
            {currentUser ? (
              ""
            ) : (
              <Link className="" href="/auth">
                <Image
                  src={"/images/utils/web.png"}
                  alt="logout"
                  width={20}
                  height={20}
                />
                Se connecter
              </Link>
            )}
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
