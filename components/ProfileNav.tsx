import { Menu, Transition } from '@headlessui/react'
import { Fragment } from 'react'
import Image from "next/image";
import Link from "next/link";
import { signOut } from "next-auth/react";
import { User } from "@prisma/client";
import clsx from "clsx";

interface ProfileNavProps {
    currentUser: User;
}

const ProfileNav: React.FC<ProfileNavProps> = ({ currentUser }) => {
  return (
    <div className="md:flex items-center hidden">
        <Menu as="div" className="relative inline-block text-left">
            <div className="h-full flex items-center">
                <Menu.Button className="focus:outline-2 relative rounded-full focus-visible:ring-4 focus-visible:ring-purple/75 hover:opacity-75 focus:outline-none">
                    <Image src={currentUser?.image? currentUser?.image : "/images/undraw/Male_avatar.jpg"} className='fill rounded-full' alt='logout' width={32} height={32}/>
                </Menu.Button>
            </div>
            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <Menu.Items className="absolute right-0 mt-3 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black/5 focus:outline-none">
                    <div className="px-1 py-1">

                        {/* Profil */}
                        <Menu.Item>
                            {({ active }) => (
                                <Link href={'/profil'}
                                className={clsx(`${
                                    active ? 'bg-violet-500 text-white' : 'text-gray-900'
                                } group flex w-full items-center rounded-md px-2 py-2 text-sm`, !currentUser ? 'hidden' : '') }
                                >
                                    <Image src={currentUser?.image? currentUser?.image : `https://i.pravatar.cc/150?u=${currentUser?.id}`} className='rounded-full me-3' alt='logout' width={25} height={25}/>
                                    Profil
                                </Link>
                            )}
                        </Menu.Item>
                    </div>
                    <div className="px-1 py-1">
                        {/* Logout */}
                        <Menu.Item>
                            {({ active }) => (
                                <button onClick={() => signOut()}
                                className={clsx(`${
                                    active ? 'bg-violet-500 text-white' : 'text-gray-900'
                                } group flex w-full items-center rounded-md px-2 py-2 text-sm`, !currentUser ? 'hidden' : '') }
                                >
                                    {active ? (
                                        <LogoutActiveIcon
                                        className="mr-2 h-5 w-5"
                                        aria-hidden="true"
                                        />
                                    ) : (
                                        <LogoutInactiveIcon
                                        className="mr-2 h-5 w-5"
                                        aria-hidden="true"
                                        />
                                    )}
                                    Se déconnecter
                                </button>
                            )}
                        </Menu.Item>
                    </div>
                </Menu.Items>
            </Transition>
        </Menu>
    </div>
    )
    }

function LogoutInactiveIcon(props: any) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 me-3">
            <path d="M8.25 9V5.25A2.25 2.25 0 0 1 10.5 3h6a2.25 2.25 0 0 1 2.25 2.25v13.5A2.25 2.25 0 0 1 16.5 21h-6a2.25 2.25 0 0 1-2.25-2.25V15m-3 0-3-3m0 0 3-3m-3 3H15" />
        </svg>      
    )
    }

    function LogoutActiveIcon(props: any) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 me-3">
            <path d="M8.25 9V5.25A2.25 2.25 0 0 1 10.5 3h6a2.25 2.25 0 0 1 2.25 2.25v13.5A2.25 2.25 0 0 1 16.5 21h-6a2.25 2.25 0 0 1-2.25-2.25V15m-3 0-3-3m0 0 3-3m-3 3H15" />
        </svg>    
  )
}

export default ProfileNav
