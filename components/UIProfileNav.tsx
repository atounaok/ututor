import { User } from "@prisma/client";
import React from "react";
import Image from "next/image";
import Link from "next/link";
import clsx from "clsx";
import { signOut } from "next-auth/react";

interface UIProfileNavProps {
  currentUser: User;
}

const UIProfileNav: React.FC<UIProfileNavProps> = ({ currentUser }) => {
  return (
    <div className="dropdown dropdown-end">
      <div
        tabIndex={0}
        role="button"
        className="btn btn-ghost btn-circle avatar"
      >
        <div className="">
          <Image
            src={
              currentUser?.image
                ? currentUser?.image
                : `https://i.pravatar.cc/150?u=${currentUser?.id}`
            }
            className="fill rounded-full"
            alt="logout"
            width={32}
            height={32}
          />
        </div>
      </div>
      <ul
        tabIndex={0}
        className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52"
      >
        <li>
          <Link
            href={"/profil"}
            className={clsx(
              ``,
              !currentUser ? "hidden" : ""
            )}
          >
            <Image
              src={
                currentUser?.image
                  ? currentUser?.image
                  : `https://i.pravatar.cc/150?u=${currentUser?.id}`
              }
              className="rounded-full"
              alt="logout"
              width={25}
              height={25}
            />
            Profil
          </Link>
        </li>
        <li>
            <button
              onClick={() => signOut()}
              className={clsx(
                ``,
                !currentUser ? "hidden" : "text-red-500"
              )}
            >
              <LogoutActiveIcon className="" aria-hidden="true" />
              Se déconnecter
            </button>
        </li>
      </ul>
    </div>
  );
};

    function LogoutActiveIcon(props: any) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 25 25" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
            <path d="M8.25 9V5.25A2.25 2.25 0 0 1 10.5 3h6a2.25 2.25 0 0 1 2.25 2.25v13.5A2.25 2.25 0 0 1 16.5 21h-6a2.25 2.25 0 0 1-2.25-2.25V15m-3 0-3-3m0 0 3-3m-3 3H15" />
        </svg>    
  )
}

export default UIProfileNav;