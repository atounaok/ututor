"use client"

import clsx from "clsx"
import { FieldErrors, FieldValues, UseFormRegister } from "react-hook-form"

interface InputProps {
    label: string;
    id: string;
    type?: string;
    required?: boolean;
    register: UseFormRegister<FieldValues>;
    errors: FieldErrors;
    inputError?: boolean;
    disabled?: boolean;
    onBlur?: (e:any) => void;
}
const Input : React.FC<InputProps> = ({
    label,
    id,
    type,
    required,
    register,
    errors,
    inputError,
    disabled,
    onBlur
}) => {
  return (
    <div>
      <label 
        htmlFor={id}
        className="
            block
            text-sm
            font-medium
            leading-6
            text-gray-900
        "
        >
            {label}
      </label>
      <div className="mt-2">
        <input 
            id={id}
            autoComplete={id}
            disabled={disabled}
            type={type}
            {...register(id, { required })}
            onBlur={onBlur} 
            className={clsx(`
               input input-bordered w-full focus:outline-purple-500
            `, errors[id] && "focus:ring-rose-500", inputError && "border-rose-500",
            disabled && "opacity-50 cursor-default")}/>
      </div>
    </div>
  )
}

export default Input
