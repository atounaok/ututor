"use client";

import clsx from "clsx";
import { FieldErrors, FieldValues, UseFormRegister } from "react-hook-form";

interface InputProps {
	label: string;
	id: string;
	options: string[];
	optionPlaceholder: string;
	required?: boolean;
	register: UseFormRegister<FieldValues>;
	onChangeFun?: () => void;
	onClickFun?: () => void;
	errors: FieldErrors;
	disabled?: boolean;
	isGlass?: boolean;
}
const InputSelectTutor: React.FC<InputProps> = ({
	label,
	id,
	options,
	optionPlaceholder,
	required,
	onClickFun,
	onChangeFun,
	register,
	errors,
	disabled,
	isGlass,
}) => {
	return (
		<div>
			<label
				htmlFor={id}
				className="
            lg:block
            text-sm
            font-medium
            leading-6
            text-gray-500
			sm:hidden
            
        "
			>
				{label}
			</label>
			<div className="lg:mt-2">
				<select
					id={id}
					disabled={disabled}
					defaultValue="choix0"
					{...register(id, { required })}
					//onChange={onChangeFun}
					onClick={onClickFun}
					className={clsx(
						`
					select select-bordered w-full
				`,
						isGlass ? "glassmorphism hover:bg-transparent" : "",
						errors[id] && "focus:ring-rose-500",
						disabled && "opacity-50 cursor-default"
					)}
				>
					<option value="choix0" disabled>
						{optionPlaceholder}
					</option>

					{options.map((option, index) => (
						<option
							key={index}
							className="text-black"
							value={option.toUpperCase()}
						>
							{option}
						</option>
					))}
				</select>
			</div>
		</div>
	);
};

export default InputSelectTutor;
