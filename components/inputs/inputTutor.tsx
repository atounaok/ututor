"use client"

import clsx from "clsx"
import { FieldErrors, FieldValues, UseFormRegister } from "react-hook-form"

interface InputProps {
    label: string;
    id: string;
    type?: string;
    maxlength?: number;
    placeholder?: string;
    required?: boolean;
    register: UseFormRegister<FieldValues>;
    errors: FieldErrors;
    disabled?: boolean;
    value?: string | number;
}
const Input : React.FC<InputProps> = ({
    label,
    id,
    type,
    maxlength,
    placeholder,
    required,
    register,
    errors,
    disabled,
    value,
}) => {
  return (
    <div>
      <label 
        htmlFor={id}
        className="
            block
            text-sm
            font-medium
            leading-6
            text-gray-900
        "
        >
            {label}
      </label>
      <div className="mt-2">
        <input 
            id={id}
            autoComplete={id}
            disabled={disabled}
            type={type}
            maxLength={maxlength}
            placeholder={placeholder}
            defaultValue={value ? value : ""}
            {...register(id, { required })}
            className={clsx(`
            input input-bordered w-full focus:outline-purple-500
         `, errors[id] && "focus:ring-rose-500",
         disabled && "opacity-50 cursor-default")}/>
      </div>
    </div>
  )
}

export default Input
