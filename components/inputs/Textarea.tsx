"use client";

import clsx from "clsx";
import { FieldErrors, FieldValues, UseFormRegister } from "react-hook-form";

interface InputProps {
	label: string;
	id: string;
	required?: boolean;
	register: UseFormRegister<FieldValues>;
	errors: FieldErrors;
	disabled?: boolean;
}
const TextArea: React.FC<InputProps> = ({
	label,
	id,
	required,
	register,
	errors,
	disabled,
}) => {
	return (
		<div>
			<label
				htmlFor={id}
				className="
            block
            text-sm
            font-medium
            leading-6
            text-gray-900
        "
			>
				{label}
			</label>
			<div className="mt-2">
				<textarea
					id={id}
					autoComplete={id}
					disabled={disabled}
					{...register(id, { required })}
					className={clsx(
						`
                	textarea textarea-bordered
					w-full
            	`,
						errors[id] && "focus:ring-rose-500",
						disabled && "opacity-50 cursor-default"
					)}
				></textarea>{" "}
			</div>
		</div>
	);
};

export default TextArea;
