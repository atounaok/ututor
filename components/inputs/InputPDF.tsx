"use client"

import clsx from "clsx"
import { RefObject } from "react";
import { FieldErrors, FieldValues, UseFormRegister } from "react-hook-form"
import React, { forwardRef } from 'react';

interface InputProps {
  id: string;
  label: string;
  required?: boolean;
  register: UseFormRegister<FieldValues>;
  errors: FieldErrors;
  disabled?: boolean;
  ref?: RefObject<HTMLInputElement>;
}
const InputCV : React.ForwardRefRenderFunction<HTMLInputElement, InputProps> = ({
  id,
  label,
  required,
  register,
  errors,
  disabled,
}, ref) => {
  return (
    <div>
      <div className="mt-2">
        <label 
          htmlFor={id}
          className="
              block
              text-sm
              font-medium
              leading-6
              text-gray-900 mb-2
          "
          >
            {label}
        </label>

        <input 
            id={id}
            
            type="file"
            accept=".pdf"
            disabled={disabled}
            {...register(id, { required })}
            ref={ref}
            className=""/>

      </div>
    </div>
  )
}

export default InputCV
