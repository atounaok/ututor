// next.config.js
module.exports = {
    images: {
      domains: [
        "res.cloudinary.com",
        "lh3.googleusercontent.com",
        "avatars.githubusercontent.com",
        'i.pravatar.cc',
      ],
      remotePatterns: [
        {
          protocol: 'https',
          hostname: 'lh3.googleusercontent.com',
          port: '',
          pathname: '/**',
        },
      ],
    }
  };