import { withAuth } from "next-auth/middleware";

 export default withAuth({
     pages: {
         signIn: "/auth"
     }
 });

 export const config = {
     matcher: [
        "/tuteur/:path*","/messagerie/:path*","/profil", "/conversations/:path*", "/admin/:path*" 
     ]
 }