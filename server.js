const http = require('http');
const { Server } = require('socket.io');
const cors = require('cors');

const httpServer = http.createServer();

const io = new Server(httpServer, {
    cors: {
        origin: '*', // À remplacer pas mon vrai site plus tard
        methods: ['GET', 'POST', 'PUT', 'DELETE'],
        allowedHeaders: ['my-custom-header'],
        credentials: true,
    },
    path: 'pages/api/socket/io',
});

io.on('connection', (socket) => {
    console.log('a socket.io user connected: ', socket.id);

    // Losrque je joint une conversation
    socket.on('join_conversation', (conversationId) => {
        socket.join(conversationId);
        console.log(`a user with id-${socket.id} joined conversation: `, conversationId);
    });

    // Lorsque j'envoie un message
    socket.on('send_message', (message) => {
        console.log('message: ', message);
        io.to(message.conversationId).emit('receive_message', message);
    });

    // Lorsque je quitte une conversation
    socket.on('leave_conversation', (conversationId) => {
        console.log(`a user with id-${socket.id} left conversation: `, conversationId);
    });

    const PORT = 3000;
    httpServer.listen(PORT, () => {
        console.log(`server is running on port ${PORT}`);
    })
})

